# The editor

Since commit 2d3c2bcf, it is now quite doable to edit the content of the game. Indeed, it is now possible to add new games (refered in this guide as "adventures") by prodiving a json file describing it. Some examples can be found in the [/public/adventures](public/adventures) directory.

# Adventures json syntax

The adventures files follow a simple JSON format, with optional and mandatory values described below. Please note that this format is most likely to change in the near future.

## Levels

There are 5 available level sets, which are compiled level sets from the editor. Only the first one is mandatory. They all are referred as a different name in the editor

| JSON name | mandatory | editor name |
| --- | :---: | --- |
| `levels` | Yes | adv |
| `levels_dim1` | No | deep |
| `levels_dim2` | No | hiko |
| `levels_dim3` | No | ayame |
| `levels_dim4` | No | HK |

## Links

**TODO**

## Skins

This section is optional. It is an array that list all skins available by pressing the `D` key. Each skin have an id ranging from 1 to 12. Any value above 12 is interpreted as 12. Any value below 1 will display all the skins in a blinky way, which is probably not what you want. Here is the list of all available skins.  
An empty array has no effect.

| #id | name | image |
| --- | --- | --- |
| 1 | Igor's base hat | ![](doc/assets/skins/1.svg) |
| 2 | Chourou | ![](doc/assets/skins/2.svg) |
| 3 | Peruvian | ![](doc/assets/skins/3.svg) |
| 4 | Pioupiouz | ![](doc/assets/skins/4.svg) |
| 5 | Mario | ![](doc/assets/skins/5.svg) |
| 6 | Tuberculoz | ![](doc/assets/skins/6.svg) |
| 7 | Flower | ![](doc/assets/skins/7.svg) |
| 8 | Igor's donkey | ![](doc/assets/skins/8.svg) |
| 9 | Igor's crown | ![](doc/assets/skins/9.svg) |
| 10 | Sandy's base hat | ![](doc/assets/skins/10.svg) |
| 11 | Sandy's donkey | ![](doc/assets/skins/11.svg) |
| 12 | Sandy's crown | ![](doc/assets/skins/12.svg) |

### Examples

| Description | json |
| --- | --- |
| All the available skins in the base game | `"skins": [1, 2, 3, 4, 5, 6]` |
| All the available skins in the base game in reverse order | `"skins": [6, 5, 4, 3, 2, 1]` |
| All the base skins but the first one is the flower one | `"skins": [7, 2, 3, 4, 5, 6]` |
| Only the base skin and the pioupiouz one | `"skins": [1, 4]` |
| Only the base skin | `"skins": [1]` |
| All Sandy's skins | `"skins": [10, 11, 12]` |
| All the skins | `"skins": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]` |
| All available skins in the base game, but there is a base skin between each other one | `"skins": [1, 2, 1, 3, 1, 4, 1, 5, 1, 6]` |
