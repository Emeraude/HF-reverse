# Portal color matrixes

Here are a list of common Color Matrix to change the color of the portals in a pretty way:

| Color | Matrix | Render |
| --- | --- | --- |
| Green | <code>[1, 0, 0, 0, 0, <br>0, 0.5, 0.5, 0, 0, <br>0.5, 0.5, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/green.png) |
| Emerald green | <code>[1, 0, 0, 0, 0, <br>0, 0.7, 0.3, 0, 0, <br>0.5, 0.5, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/emerald_green.png) |
| Teal | <code>[1, 0, 0, 0, 0, <br>0, 0.5, 0.5, 0, 0, <br>0, 0.5, 0.5, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/teal.png) |
| Dark Teal | <code>[1, 0, 0, 0, 0, <br>0.5, 0.5, 0, 0, 0, <br>0.5, 0.5, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/dark_teal.png) |
| Blue (game default) | <code>[1, 0, 0, 0, 0, <br>0, 1, 0, 0, 0, <br>0, 0, 1, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/blue.png) |
| Deep blue | <code>[1, 0, 0, 0, 0, <br>0.5, 0.5, 0, 0, 0, <br>0, 0, 1, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/deep_blue.png) |
| Yellow | <code>[0, 0, 1, 0, 0, <br>0, 0.4, 0.6, 0, 0, <br>1, 0, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/yellow.png) |
| Gold | <code>[0, 0, 1, 0, 0, <br>0, 0.6, 0.4, 0, 0, <br>1, 0, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/gold.png) |
| Orange | <code>[0, 0, 1, 0, 0, <br>0, 1, 0, 0, 0, <br>1, 0, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/orange.png) |
| Red | <code>[0, 0, 1, 0, 0, <br>0.8, 0.2, 0, 0, 0, <br>1, 0, 0, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/red.png) |
| Cotton pink | <code>[1, 0, 1, 0, 0, <br>0, 1, 0, 0, 0, <br>0, 0, 1, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/cotton_pink.png) |
| Pink | <code>[0, 0, 1, 0, 0, <br>0.8, 0.2, 0, 0, 0, <br>0, 0.2, 0.8, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/pink.png) |
| Purple | <code>[0, 0, 1, 0, 0, <br>0, 1, 0, 0, 0, <br>0.2, 0, 0.8, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/purple.png) |
| Light grey | <code>[0.4, 0.4, 0.4, 0, 0, <br>0.4, 0.4, 0.4, 0, 0, <br>0.4, 0.4, 0.4, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/light_grey.png) |
| Grey | <code>[0.25, 0.25, 0.25, 0, 0, <br>0.25, 0.25, 0.25, 0, 0, <br>0.25, 0.25, 0.25, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/grey.png) |
| Black | <code>[0.1, 0.1, 0.1, 0, 0, <br>0.1, 0.1, 0.1, 0, 0, <br>0.1, 0.1, 0.1, 0, 0, <br>0, 0, 0, 1, 0]</code> | ![](/doc/assets/portals/black.png) |
