all: game loader

asm: game-asm loader-asm

decompile: game-decompile loader-decompile

##
## Game rules
##
game:
	make -C disasm/game all

game-asm:
	make -C disasm/game asm

game-decompile:
	make -C disasm/game decompile

##
## Loader rules
##
loader:
	make -C disasm/loader all

loader-asm:
	make -C disasm/loader asm

loader-decompile:
	make -C disasm/loader decompile

.PHONY:	all asm decompile game game-asm game-decompile loader loader-asm loader-decompile
