#!/usr/bin/env node

const fs = require('fs'),
      xml2js = require('xml2js');
const parser = new xml2js.Parser();

let quests = [];
let langs = {
  quests: {
    desc: {
      fr: {},
      en: {},
      es: {}
    },
    name: {
      fr: {},
      en: {},
      es: {}
    }
  },
  families: {
    fr: {},
    en: {},
    es: {}
  },
  items: {
    fr: {},
    en: {},
    es: {}
  }
};

function loadQuests() {
  fs.readFile(__dirname + '/quests.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (let i in result.quests.quest) {
        let quest = result.quests.quest[i];
        let saveQ = {
          id: parseInt(quest.$.id),
          name: {
            fr: langs.quests.name.fr[quest.$.id],
            en: langs.quests.name.en[quest.$.id],
            es: langs.quests.name.es[quest.$.id]
          },
          desc: {
            fr: langs.quests.desc.fr[quest.$.id],
            en: langs.quests.desc.en[quest.$.id],
            es: langs.quests.desc.es[quest.$.id]
          },
          items: [],
          give: {
            families: []
          },
          remove: {
            families: []
          }
        };
        for (let j in quest.require) {
          let item = quest.require[j];
          saveQ.items.push({
            id: parseInt(item.$.item),
            quantity: parseInt(item.$.qty),
            name: {
              fr: langs.items.fr[item.$.item],
              en: langs.items.en[item.$.item],
              es: langs.items.es[item.$.item]
            }
          });
        }
        if (quest.give) {
          for (let k in quest.give) {
            let give = quest.give[k].$;
            if (give.tokens) {
              saveQ.give.tokens = parseInt(give.tokens);
            } else if (give.bank) {
              saveQ.give.bank = parseInt(give.bank);
            } else if (give.option) {
              saveQ.give.option = give.option;
            } else if (give.mode) {
              saveQ.give.mode = give.mode;
            } else if (give.log) {
              saveQ.give.log = !!give.log;
            } else if (give.family) {
              let family = {
                id: parseInt(give.family)
              };
              if (langs.families.fr[give.family])
                family.name = {
                  fr: langs.families.fr[give.family],
                  en: langs.families.fr[give.family],
                  es: langs.families.fr[give.family]
                };
              saveQ.give.families.push(family);
            }
          }
        }
        if (quest.remove) {
          for (let k in quest.remove) {
            let remove = quest.remove[k].$;
            saveQ.remove.families.push({
              id: parseInt(remove.family),
              name: {
                fr: langs.families.fr[remove.family],
                en: langs.families.fr[remove.family],
                es: langs.families.fr[remove.family]
              }
            });
          }
        }
        if (!saveQ.remove.families[0]) {
          delete saveQ.remove.families;
          delete saveQ.remove;
        }
        if (!saveQ.give.families[0]) {
          delete saveQ.give.families;
        }
        if (JSON.stringify(saveQ.give) == "{}") {
          delete saveQ.give;
        }
        quests.push(saveQ);
      }
      fs.writeFile("./quests.json", JSON.stringify({quests: quests}, null, 2), (err) => {
        if (err) throw err;
      });
    });
  });
}

function loadSpanish() {
  fs.readFile(__dirname + '/lang-es.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (i in result.lang.items[0].item) {
        let item = result.lang.items[0].item[i].$;
        langs.items.es[item.id] = item.name;
      }
      for (i in result.lang.families[0].family) {
        let family = result.lang.families[0].family[i].$;
        langs.families.es[family.id] = family.name;
      }
      for (i in result.lang.quests[0].quest) {
        let quest = result.lang.quests[0].quest[i];
        langs.quests.name.es[quest.$.id] = quest.$.title;
        langs.quests.desc.es[quest.$.id] = quest._.replace(/\\t/g, '').replace(/\\n/g, ' ');
      }
      loadQuests();
    });
  });
}

function loadEnglish() {
  fs.readFile(__dirname + '/lang-en.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (i in result.lang.items[0].item) {
        let item = result.lang.items[0].item[i].$;
        langs.items.en[item.id] = item.name;
      }
      for (i in result.lang.families[0].family) {
        let family = result.lang.families[0].family[i].$;
        langs.families.en[family.id] = family.name;
      }
      for (i in result.lang.quests[0].quest) {
        let quest = result.lang.quests[0].quest[i];
        langs.quests.name.en[quest.$.id] = quest.$.title;
        langs.quests.desc.en[quest.$.id] = quest._.replace(/\\t/g, '').replace(/\\n/g, ' ');
      }
      loadSpanish();
    });
  });
}

fs.readFile(__dirname + '/lang-fr.xml', function(err, data) {
  if (err)
    console.error(err);
  parser.parseString(data, function (err, result) {
    for (i in result.lang.items[0].item) {
      let item = result.lang.items[0].item[i].$;
      langs.items.fr[item.id] = item.name;
    }
    for (i in result.lang.families[0].family) {
      let family = result.lang.families[0].family[i].$;
      langs.families.fr[family.id] = family.name;
    }
    for (i in result.lang.quests[0].quest) {
      let quest = result.lang.quests[0].quest[i];
      langs.quests.name.fr[quest.$.id] = quest.$.title;
      langs.quests.desc.fr[quest.$.id] = quest._.replace(/\\t/g, '').replace(/\\n/g, ' ');
    }
    loadEnglish();
  });
});

