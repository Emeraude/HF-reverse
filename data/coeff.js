#!/usr/bin/env node

const fs = require('fs'),
      xml2js = require('xml2js');
const parser = new xml2js.Parser();

let items = [];
let langs = {
  families: {
    fr: {},
    en: {},
    es: {}
  },
  items: {
    fr: {},
    en: {},
    es: {}
  }
};

function loadPointItems() {
  fs.readFile(__dirname + '/items.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (let i in result.items.family) {
        let family = result.items.family[i];
        for (let j in family.item) {
          let item = family.item[j].$;
          items.push({
            id: parseInt(item.id),
            rarity: parseInt(item.rarity),
            value: item.value == "--" ? item.value : parseInt(item.value),
            name: {
              fr: langs.items.fr[item.id],
              en: langs.items.en[item.id],
              es: langs.items.es[item.id]
            }
          });
        }
      }
      items = items.sort( (a, b) => { return a.id - b.id});
      fs.writeFile("./coeff.json", JSON.stringify({items: items}, null, 2), (err) => {
        if (err) throw err;
      });
    });
  });
}


function loadEffectItems() {
  fs.readFile(__dirname + '/families.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (let i in result.items.family) {
        let family = result.items.family[i];
        for (let j in family.item) {
          let item = family.item[j].$;
          items.push({
            id: parseInt(item.id),
            rarity: parseInt(item.rarity),
            unlock: item.unlock ? parseInt(item.unlock) : 10,
            name: {
              fr: langs.items.fr[item.id],
              en: langs.items.en[item.id],
              es: langs.items.es[item.id]
            }
          });
        }
      }
      loadPointItems();
    });
  });
}

function loadSpanish() {
  fs.readFile(__dirname + '/lang-es.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (i in result.lang.items[0].item) {
        let item = result.lang.items[0].item[i].$;
        langs.items.es[item.id] = item.name;
      }
      for (i in result.lang.families[0].family) {
        let family = result.lang.families[0].family[i].$;
        langs.families.es[family.id] = family.name;
      }
      loadEffectItems();
    });
  });
}

function loadEnglish() {
  fs.readFile(__dirname + '/lang-en.xml', function(err, data) {
    if (err)
      console.error(err);
    parser.parseString(data, function (err, result) {
      for (i in result.lang.items[0].item) {
        let item = result.lang.items[0].item[i].$;
        langs.items.en[item.id] = item.name;
      }
      for (i in result.lang.families[0].family) {
        let family = result.lang.families[0].family[i].$;
        langs.families.en[family.id] = family.name;
      }
      loadSpanish();
    });
  });
}

fs.readFile(__dirname + '/lang-fr.xml', function(err, data) {
  if (err)
    console.error(err);
  parser.parseString(data, function (err, result) {
    for (i in result.lang.items[0].item) {
      let item = result.lang.items[0].item[i].$;
      langs.items.fr[item.id] = item.name;
    }
    for (i in result.lang.families[0].family) {
      let family = result.lang.families[0].family[i].$;
      langs.families.fr[family.id] = family.name;
    }
    loadEnglish();
  });
});

