# Hammerfest local

This repository is trying to make a working local copy of the game Hammerfest.  
Usage:

```sh
mv config.json.default config.json
$EDITOR config.json
npm start
```

## Requirements

In order to edit the project you will need the **flasm** assembler/disassembler (required to edit the assembly code of the game) and the **flare** decompiler, to get the ActionScript code of the game, in a non recompilable way (useful to better understand the code).


## Tools

I had to write some tools in order to help me to understand how the game works. They are located in the `tools` directory.
