#!/usr/bin/env bash

# item_cli.sh
#
# This script display one or several items on the terminal, depending on their id. The size of the output image (in "pixels", or character) can be set with the environment variable SIZE.
#
# This script requires ImageMagick and img2xterm to be installed. Note that if you are building img2xterm from source, you might need to apply this patch to build it: https://github.com/rossy/img2xterm/pull/14

[ $# -lt 1 ] && echo "Usage: $0 <item_id>..." && exit 1

cd $(dirname $0)
size=${SIZE-48}

for i in "$@"; do
  if [ ! -f ../data/images/items/"$i".svg ]; then
    echo "No such item \"$i\"" >&2
  else
    # The option to read from stdin is broken in img2xterm, so we have to use this hideous workaround
    convert -background none -density 1000 -resize $size'x'$size ../data/images/items/$i.svg png32:- | img2xterm /proc/self/fd/0
  fi
done
