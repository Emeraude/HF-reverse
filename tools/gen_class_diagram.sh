#!/usr/bin/env bash

# gen_class_diagram.sh
#
# This script generates a very basic class diagram of the game source code, using the .asm.flr source files.
# It does not work on the loader since there is no inheritance in the loader source code
#
# This script requires the `dot` command

cd $(dirname $0)"/../disasm/game/"

echo "digraph \"Game class diagram\" {" > graph.dot
find . -name '*.asm.flr' | xargs grep extends | sed -E -e 's/^.*:\s{2}*//' -e 's/extends/->/' -e 's/(^.*)( -> )(.*)(;$)/    "\3"\2"\1"\4/' >> graph.dot
echo "}" >> graph.dot

dot -Tpng graph.dot -o graph.png -Nshape=box -Grankdir=LR
