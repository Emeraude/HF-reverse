#!/usr/bin/env sh

# Loop.sh
#
# This script retrieve the assembly code that generate ugly for loops
# Some of them are not retrieved yet, because the used regexp in not exhaustive
# This script can be used with `loop.js`, which edit the assembly code retrieved and update the `game.asm` file
#
# See issue #1 at https://gitlab.com/Emeraude/HF-reverse/issues/1
#
# https://stackoverflow.com/a/7167115

file=${1-"../disasm/game.asm"}

grep -Pzo '(?s)\s*push\s0.\s*setRegister\sr:\d+.\s*pop.\s*branch label\d+.\s*label\d+:.\s*push r:\d+.\s*increment\s*.\s*setRegister r:\d+.\s*pop.\s*label\d+:\n(\s*push\N*\n(\s*get(Member|Variable)\n)?)+\s*(greater|less)Than.(\s*not\n)+\s*branchIfTrue\slabel\d+.' $file
