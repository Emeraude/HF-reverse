#!/usr/bin/env bash

# asm2flr.sh
#
# This script turns a .asm file into a .flr one, by assembling it using flasm, then decompiling it using flare.
# This script requires the file `empty.swf`, which is used as a temporary container for the temporary generated .swf file.
# It also turns #include into include(), avoiding including external code in output flr.
#
# See issue #3 at https://gitlab.com/Emeraude/HF-reverse/issues/3

[ $# -lt 1 ] && echo "Usage: $0 <file.asm>..." && exit 1

for i in "$@"; do
  if [ -f "$i" ]; then
      file=$(basename -- "$i")

      if [ $(head -c 5 -- "$i") != "movie" ]; then
          echo "movie '/tmp/$file.swf'" > /tmp/"$file".tmp
          echo "  frame 0" >> /tmp/"$file".tmp
          echo "" >> /tmp/"$file".tmp
          swf="$(dirname $0)/empty.swf"
      else
        swf="$(head -n 1 -- $i | grep "movie '.*'" -o | cut -d"'" -f2)"
      fi

      cp "$swf" /tmp/"$file".swf

      sed -E "s/(\s+)#include '(.*)'/\1push '\2', 1, include\n\1callFunction\n\1pop/g" -- "$i" >> /tmp/"$file".tmp

      if [ $(head -c 5 -- "$i") != "movie" ]; then
          echo "" >> /tmp/"$file".tmp
          echo "  end" >> /tmp/"$file".tmp
          echo "end" >> /tmp/"$file".tmp
      else
        sed -i "1s/movie '.*'/movie '\/tmp\/$file.swf'/" /tmp/"$file".tmp
      fi

      flasm -a /tmp/"$file".tmp
      flare /tmp/"$file".swf

      if [ $(head -c 5 -- "$i") != "movie" ]; then
          tail -n +5 /tmp/"$file".flr | head -n -2 | sed -E 's/^    //g' > $(dirname -- "$i")/"$file".flr
      else
        mv /tmp/"$file".flr $(dirname -- "$i")
      fi

      rm -f /tmp/"$file".???
  else
    echo "Invalid file \"$i\"" >&2
  fi
done

