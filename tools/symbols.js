#!/usr/bin/env node

// symbols.js
//
// This script is an interface for a database that stores old symbols and their new name.
// Its main goal is to have a database that can easily turns all the previous symbols with their new versions, turning the original source code into a readable one.
//
// This db is used by the mv_symbol.sh script. It will probablby will used in more scripts in the future
//
// Return values:
//   0 Everything went right
//   1 System error
//   2 Usage error
//   10 Add error
//   11 Get error
//   12 Update warning
//   13 Remove warning

const fs = require("fs")
const dbFile = __dirname + "/.symbols-db.json"
const db = require(dbFile)

const commands = {
  add: function(name, value) {
    if (name === undefined || value === undefined) {
      usage()
      return 2
    }
    else if (db[name] !== undefined) {
      console.error(`Symbol "${name}" already exists. Use command "update" tu edit it.`)
      return 10
    } else {
      db[name] = value
      return 0
    }
  },
  get: function(name) {
    if (name === undefined) {
      usage()
      return 2
    }
    else if (db[name] !== undefined) {
      console.log(db[name])
      return 0
    } else {
      console.error(`Cannot retrieve symbol "${name}".`)
      return 11
    }
  },
  update: function(name, value) {
    if (name === undefined || value === undefined) {
      usage()
      return 2
    }
    else if (db[name] === undefined) {
      console.error(`Symbol "${name}" was not found. Use command "add" tu add it.`)
      return 12
    } else {
      db[name] = value
      return 0
    }
  },
  remove: function(name) {
    if (name === undefined) {
      usage()
      return 2
    }
    else if (db[name] !== undefined) {
      delete db[name]
      return 0
    } else {
      console.error(`Symbol "${name}" was not found. Unable to delete it`)
      return 13
    }
  },
  usage: function() {
    console.log(`Usage: ${process.argv[1]} add <NAME> <VALUE> | get <NAME> | update <NAME> <VALUE> | remove <NAME> | usage`)
    return 0
  }
}

const usage = commands.usage

if (commands[process.argv[2]]) {
  const retValue = commands[process.argv[2]].apply(null, process.argv.slice(3))
  fs.writeFile(dbFile, JSON.stringify(db, null, 2), (err) => {
    if (err)
      throw err
    else
      process.exit(retValue)
  })
} else {
  usage()
  process.exit(2)
}
