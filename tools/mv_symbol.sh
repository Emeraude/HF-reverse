#!/usr/bin/env bash

# mv_symbol.sh
#
# This script renames a symbol from the asm files in the /disasm directory. It logs it in the symbols database (using symbols.sh script) and recompile the swf files. It also edits the class diagram, using the gen_class_diagram.sh script

[ $# -ne 2 ] && echo "Usage: $0 <old> <new>" && exit 1

cd $(dirname $0)

san1=$(echo -n "$1" | sed -e 's/[\/&*\[]/\\&/g')
san2=$(echo -n "$2" | sed -e 's/[]\/$*.^[]/\\&/g')

find ../disasm -name '*.asm' -type f | xargs grep -Fl -- "$1" | xargs sed -e "s/$san1/$san2/g" -i \
    && make -C .. \
    && ./gen_class_diagram.sh \
    && ./symbols.js add "$1" "$2"
