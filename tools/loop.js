#!/usr/bin/env node

// Loop.js
//
// This script edit the assembly code retrieved by `loop.sh` and update the `game.asm` file
//
// See issue #1 at https://gitlab.com/Emeraude/HF-reverse/issues/1

const exec = require('child_process').exec
const fs = require('fs')

let file = process.argv[2] || "../disasm/game.asm"

fs.readFile(file, {encoding: 'utf-8'}, function(err, data) {
  exec("./loop.sh " + file, function(err, stdout, stderr) {
    let blobs = stdout.split("\n\0\n")

    for (let i in blobs) {
      let blob = blobs[i]
      let register = 0
      let lines = blob.split('\n')

      if (lines[0] == '')
        lines.shift(1)
      if (lines[lines.length - 1] == '\0')
        lines.splice(lines.length - 1, 1)
      blob = lines.join('\n')

      let newBlob = blob.replace('push 0', 'push -1')
      lines = newBlob.split('\n')

      // Get register
      for (let j in lines) {
        if (lines[j].match('setRegister r:')) {
          register = lines[j].split("setRegister r:")[1]
          break
        }
      }

      // Drop branch
      for (let j in lines) {
        if (lines[j].match('branch ')) {
          lines.splice(j, 1)
          break
        }
      }

      // Drop second label
      let nb = 0
      for (let j in lines) {
        if (lines[j].match(/label\d+:/)) {
          if (nb > 0)
            lines.splice(j, 1)
          else
            ++nb
        }
      }

      // Drop second pop
      nb = 0
      for (let j in lines) {
        if (lines[j].match('pop')) {
          if (nb > 0)
            lines.splice(j, 1)
          else
            ++nb
        }
      }

      // Edit third push
      nb = 0
      for (let j in lines) {
        if (lines[j].match('push')) {
          if (nb == 2)
            lines[j] = lines[j].replace('r:' + register + ', ', '')
          else
            ++nb
        }
      }

      console.log(blob)
      console.log('-------------------')
      console.log(lines.join('\n'))
      console.log('=====================')

      data = data.replace(blob, lines.join('\n'))
    }
    fs.writeFile(file, data, function(err) {
      if (err)
        console.warn(err)
      else
        console.warn('done')
    });
  });
});
