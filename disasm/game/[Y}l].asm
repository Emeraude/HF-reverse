    push '[Y}l]'
    function2 (r:2='}J', r:3='}JA', r:4='f', r:5='opt') (r:1='this')
      push r:this, '-iB=', r:'}J'
      setMember
      push r:this, '+(WHk', r:'}JA'
      setMember
      push '$volume', 1, r:this, 'getInt'
      callMethod
      push 1, 'isNaN'
      callFunction
      not
      branchIfTrue label3555
      push 'missing parameters', 1, '{aq}5'
      getVariable
      push ')oAPu'
      callMethod
      pop
     label3555:
      push r:this, 'options', 0, '19{O'
      new
      setMember
      push r:this, '+8gXK', 0, 'Array'
      new
      setMember
      push r:'opt', 'length'
      getMember
      push 0
      greaterThan
      not
      branchIfTrue label3557
      push r:this, '+8gXK', ',', 1, r:'opt', 'split'
      callMethod
      setMember
      push -1
      setRegister r:6
      pop
     label3556:
      push r:6
      increment
      setRegister r:6
      push r:this, '+8gXK'
      getMember
      push 'length'
      getMember
      lessThan
      not
      branchIfTrue label3557
      push TRUE, r:this, '+8gXK'
      getMember
      push r:6
      getMember
      push 2, r:this, 'options'
      getMember
      push 'set'
      callMethod
      pop
      branch label3556
     label3557:
      push r:this, 'families', ',', 1, r:'f', 'split'
      callMethod
      setMember
      push r:this, 'familiesScore', 0, 'Array'
      new
      setMember
      push r:this, 'familiesSpecial', 0, 'Array'
      new
      setMember
      push -1
      setRegister r:7
      pop
     label3558:
      push r:7
      increment
      setRegister r:7
      push r:this, 'families'
      getMember
      push 'length'
      getMember
      lessThan
      not
      branchIfTrue label3561
      push 10, r:this, 'families'
      getMember
      push r:7
      getMember
      push 2, 'parseInt'
      callFunction
      setRegister r:8
      pop
      push r:8, 1000
      lessThan
      not
      not
      branchIfTrue label3559
      push r:8, 1, r:this, 'familiesScore'
      getMember
      push 'push'
      callMethod
      pop
      branch label3560
     label3559:
      push r:8, 1, r:this, 'familiesSpecial'
      getMember
      push 'push'
      callMethod
      pop
     label3560:
      branch label3558
     label3561:
      push r:this, 'volume', '$volume', 1, r:this, 'getInt'
      callMethod
      push 0.5
      multiply
      push 100
      divide
      setMember
      push r:this, 'sound', '$sound', 1, r:this, 'getInt'
      callMethod
      push r:this, 'volume'
      getMember
      multiply
      setMember
      push r:this, 'music', '$music', 1, r:this, 'getInt'
      callMethod
      push r:this, 'volume'
      getMember
      multiply
      push 0.65
      multiply
      setMember
      push r:this, 'details', '$detail', 1, r:this, 'getBool'
      callMethod
      setMember
      push r:this, 'shaky', '$shake', 1, r:this, 'getBool'
      callMethod
      setMember
      push r:this, 'details'
      getMember
      not
      not
      branchIfTrue label3562
      push 0, r:this, '{[+xB'
      callMethod
      pop
     label3562:
    end // of function 

    setRegister r:0
    setVariable
    push r:0, 'prototype'
    getMember
    setRegister r:1
    pop
    push r:1, 'getString'
    function2 (r:2='7') (r:1='this')
      push r:this, '-iB='
      getMember
      push r:'7'
      getMember
      return
    end // of function 

    setMember
    push r:1, 'getInt'
    function2 (r:2='7') (r:1='this')
      push 10, r:this, '-iB='
      getMember
      push r:'7'
      getMember
      push 2, 'parseInt'
      callFunction
      return
    end // of function 

    setMember
    push r:1, 'getIntArray'
    function2 (r:2='7') (r:1='this')
      push 0, 'Array'
      new
      setRegister r:3
      pop
      push r:this, '-iB='
      getMember
      push r:'7'
      getMember
      push 'length'
      getMember
      push 0
      greaterThan
      not
      branchIfTrue labelGetIntArrayEndLoop
      push ',', 1, r:this, '-iB='
      getMember
      push r:'7'
      getMember
      push 'split'
      callMethod
      setRegister r:4
      pop
      push -1
      setRegister r:5
      pop
     labelGetIntArrayLoop:
      push r:5
      increment
      setRegister r:5
      push r:4, 'length'
      getMember
      lessThan
      not
      branchIfTrue labelGetIntArrayEndLoop
      push 10, r:4, r:5
      getMember
      push 2, 'parseInt'
      callFunction
      push 1, r:3, 'push'
      callMethod
      pop
      branch labelGetIntArrayLoop
     labelGetIntArrayEndLoop:
      push r:3
      return
    end // of function 

    setMember
    push r:1, 'getBool'
    function2 (r:2='7') (r:1='this')
      push r:this, '-iB='
      getMember
      push r:'7'
      getMember
      push '0'
      equals
      not
      dup
      not
      branchIfTrue label3563
      pop
      push r:this, '-iB='
      getMember
      push r:'7'
      getMember
      push NULL
      equals
      not
     label3563:
      return
    end // of function 

    setMember
    push r:1, '{[+xB'
    function2 () (r:1='this')
      push r:this, 'details', FALSE
      setMember
      push '_root'
      getVariable
      push '_quality', 'medium'
      setMember
      push '5e{K'
      getVariable
      push '2HuF}', '5e{K'
      getVariable
      push '2HuF}'
      getMember
      push 0.5
      multiply
      push 1, 'Math'
      getVariable
      push 'ceil'
      callMethod
      setMember
    end // of function 

    setMember
    push r:1, 'hasFamily'
    function2 (r:2='id') (r:1='this')
      push -1
      setRegister r:3
      pop
     label3564:
      push r:3
      increment
      setRegister r:3
      push r:this, 'familiesSpecial'
      getMember
      push 'length'
      getMember
      lessThan
      not
      branchIfTrue label3566
      push r:this, 'familiesSpecial'
      getMember
      push r:3
      getMember
      push r:'id'
      equals
      not
      branchIfTrue label3565
      push TRUE
      return
     label3565:
      branch label3564
     label3566:
      push -1
      setRegister r:3
      pop
     label3567:
      push r:3
      increment
      setRegister r:3
      push r:this, 'familiesScore'
      getMember
      push 'length'
      getMember
      lessThan
      not
      branchIfTrue label3569
      push r:this, 'familiesScore'
      getMember
      push r:3
      getMember
      push r:'id'
      equals
      not
      branchIfTrue label3568
      push TRUE
      return
     label3568:
      branch label3567
     label3569:
      push FALSE
      return
    end // of function 

    setMember
    push r:1, 'hasOption'
    function2 (r:2='oid') (r:1='this')
      push r:'oid', 1, r:this, 'options'
      getMember
      push 'get'
      callMethod
      push TRUE
      equals
      return
    end // of function 

    setMember
    push r:1, 'toString'
    function2 () (r:1='this')
      push ''
      setRegister r:2
      pop
      push r:2, 'fam=', ', ', 1, r:this, 'families'
      getMember
      push 'join'
      callMethod
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2, 'opt=', '\n  ', 1, r:this, '+8gXK'
      getMember
      push 'join'
      callMethod
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2, 'mus=', r:this, 'music'
      getMember
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2, 'snd=', r:this, 'sound'
      getMember
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2, 'detail=', r:this, 'details'
      getMember
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2, 'shaky =', r:this, 'shaky'
      getMember
      add
      push '\n'
      add
      add
      setRegister r:2
      pop
      push r:2
      return
    end // of function 

    setMember
    push r:1, 'hasMusic'
    function2 () (r:1='this')
      push r:this, '+(WHk'
      getMember
      push 'songs'
      getMember
      push 0
      getMember
      push NULL
      equals
      not
      return
    end // of function 

    setMember
