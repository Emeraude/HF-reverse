movie '../../public/loader.swf' compressed // flash 8, total frames: 5, frame rate: 40 fps, 420x520 px

  frame 0
    push 'enXml'
    push '<lang id="en" debug="0"><statics><t id="1" v="Level"/><t id="2" v="Learning"/><t id="3" v="next level"/><t id="4" v="QUICK!"/><t id="5" v="Instructions"/><t id="6" v="Click here to continue..."/><t id="7" v="Move"/><t id="8" v="Place a bomb"/><t id="9" v="Pause"/><t id="10" v="SPACE"/><t id="11" v="PERFECT!"/><t id="12" v="DANGER, BOSS!"/><t id="13" v="New land"/><t id="14" v="Land:"/><t id="15" v="Hammerfest requires version 8 or higher of the Flash player. Click the following link to install it now:"/><t id="16" v="Hammerfest Forest"/><t id="17" v="Trainee\'s Caverns"/><t id="18" v="MINUTE"/><t id="19" v="MINUTES"/><t id="20" v="TIMEATTACK!"/><t id="21" v="Get ready..."/><t id="22" v="SCORE!"/><t id="23" v="OWN GOAL!"/><t id="24" v="Start!"/><t id="25" v="End of match"/><t id="26" v="VICTORY: IGOR!"/><t id="27" v="VICTORY: ¡SANDY!"/><t id="28" v="DRAW!"/><t id="29" v="Move"/><t id="30" v="Kick"/><t id="31" v="Vertical kick"/><t id="32" v="Kick the ball into your opponent\'s goal. Everything\'s allowed! This mode is for 2 players at the same computer."/><t id="33" v="Press P key for help"/><t id="34" v="Beaming up is at present impossible"/><t id="35" v="Bonus life:"/><t id="36" v="Tuberace"/><t id="37" v="Your time:"/><t id="38" v="Average time for level:"/><t id="39" v="Time:"/><t id="40" v="Required key:"/><t id="41" v="Igor has used:"/><t id="42" v="Bomba"/><t id="43" v="est eliminé !"/><t id="44" v="Cible interdite !"/><t id="45" v="Cible éliminée."/><t id="50" v="Travelling to Hammerfest..."/><t id="51" v="yd"/><t id="52" v="You\'ve just arrived!"/><t id="53" v="Click here to start..."/><t id="54" v="Starting game..."/><t id="55" v="Saving game..."/><t id="56" v="Please, wait a moment..."/><t id="57" v="You\'ll have a game less."/><t id="58" v="This mode is free and unlimited!"/><t id="59" v="----[ MODE DEV LOCAL ]----"/><t id="60" v="Error sending data, try attempt: "/><t id="61" v="Loading music..."/><t id="62" v="Everything\'s ready!"/><t id="63" v="Reading music tracks..."/><t id="64" v="Error downloading data."/><t id="65" v="Please install the latest Flash version"/><t id="66" v="Couldn\'t find file server."/><t id="67" v="Download error"/><t id="68" v="Error at the beginning of the download"/><t id="69" v="Music download error!"/><t id="70" v="Game download error! Check the Technical Support to Clear your browser\'s cache memory."/><t id="71" v="Saving game..."/><t id="72" v="Impossible to finish saving the game."/><t id="73" v="Game successfully saved :)"/><t id="74" v="Nearly..."/><t id="100" v="Jump *into the well* to start the adventure!"/><t id="101" v="A secret passageway!"/><t id="102" v="At the very bottom of the well, a frightening laugh resounds..."/><t id="103" v="You have discovered a *secret passageway*!"/><t id="104" v="You have just freed the latest fruits!"/><t id="105" v="You hear a clicking sound coming from a pack of idombinos*..."/><t id="106" v="The Bombinos have been destroyed! Igor can continue his adventure."/><t id="107" v="A strange noise is coming from the lower levels..."/><t id="108" v="You have just entered the idaddybombs Pears\' Lair*..."/><t id="109" v="Igor has beaten the Baddybombs Pears!"/><t id="110" v="It seems that temperature has suddenly dropped... Igor has arrived at the *Sorbex* Dive!"/><t id="111" v="The terrible Sortex lemons have been destroyed! Igor can continue his exploration..."/><t id="112" v="Igor has just arrived at a Tagada-Strawberry-Ball match. You\'ll need to beat the team of *Tagada-Strawberries* !"/><t id="113" v="The *Strawberry* team has been beaten 4-0 by Igor the champion!"/><t id="114" v="Igor has entered the cavern of the mischievous *Kiwi Sappers*!"/><t id="115" v="You have beaten the *Kiwi Sappers*!"/><t id="116" v="The *Destiny Test*"/><t id="117" v="You have entered a secret and forgotten place... The *Test Temple*!"/><t id="118" v="The *Dexterity* Test"/><t id="119" v="You have *failed* to pass this Test"/><t id="120" v="You have *won* this horrible Test!"/><t id="121" v="The *Courage* Test"/><t id="122" v="You have opened a *shortcut*!"/><t id="123" v="The whistling sound of bodies moving through the air... no doubt! You are in the territory of the *Leapers*!"/><t id="124" v="Silence comes again whilst the last Leaper falls down..."/><t id="125" v="BETA-TEST: cette porte mène au niveau 51."/><t id="126" v="*Darkness* invades the caverns while you go down..."/><t id="127" v="The ground is shaking... Prepare yourself to face the *Armaggedon-Pineapple*!"/><t id="128" v="Calm returns whilst the latest Armaggedon-Pineapple falls down..."/><t id="129" v="Choose the court for this match. You can *press the P key* to obtain help at any moment."/><t id="130" v="This *special court* is dedicated to VolleyFest!"/><t id="131" v="You have found the *Ankhel Jewel*! Rick would be proud of you..."/><t id="132" v="The *Ankhel Jewel* has been sealed for eternity. All that effort for nothing!"/><t id="133" v="Protect the *red button*!"/><t id="134" v="Pick up the maximum number of crystals you can!"/><t id="135" v="The door has just opened!"/><t id="136" v="A suspicious mouldy smell is coming from the lower levels..."/><t id="137" v="Something horrible seems to have germinated from the Bobble Cane... You are in the Fallen Bobbles Den!"/><t id="138" v="The Fallen Bobbles have been beaten... Considering the size of the Cane, it\'s possible that there are more sticks all over the caverns."/><t id="139" v="*SPFiredrake* : Welcome to the Carrotian Council, Igor. We\'ve been waiting for you."/><t id="140" v="*Joekickass* : We are reflections of yourself. We each come from a parallel dimension where we have succeeded in defeating Tuberculoz."/><t id="141" v="*Lucied* : A superior entity sent us here to help you in your quest and show you the way."/><t id="142" v="*DigDog* : Who\'s next? Is it my turn?"/><t id="143" v="*goronhead* : Here is the Gordon Key. It will allow you to open the entrance to our cavern."/><t id="144" v="*arr8* : We have hidden, at the bottom of this cavern, the great Bobble Cane -an object of legend and of great value."/><t id="145" v="*goronhead* : But we did that a very long time ago... before The Great Update."/><t id="146" v="*Everyone* : Oooooooooh... The Great Update!"/><t id="147" v="*SPFiredrake* : SILENCE!"/><t id="148" v="*SPFiredrake* : As First Pantheonian, I entrust to you the important task of retrieving the Bobble Cane. Now, go."/><t id="149" v="*SPFiredrake* : The Bobble Cane has been **corrupted**!"/><t id="150" v="*Everyone* : ooooooooh...."/><t id="151" v="*DigDog* : Corrupted! Your turn..."/><t id="152" v="*goronhead* : I bet you comrades that this corruption will be the source of huge changes!"/><t id="153" v="*Lucied* : You must return to explore the caverns, Igor. Without a doubt, other *secret places* just like this one exist..."/><t id="154" v="*SPFiredrake* : Yes, it\'s true! You must be attentive, Igor. Search every corner of this world..."/><t id="155" v="*SPFiredrake* : We had never mentioned it... but numerous secrets still hide in The Caverns of Hammerfest!!"/><t id="156" v="*arr8* : ... Like the one where you blow of a bomb in the Well?"/><t id="157" v="*Everyone* : ooooooooooooooooh...."/><t id="200" v="Use the *arrow keys* to move Igor from right to left."/><t id="201" v="Press the *Up arrow key* to jump up this wall!"/><t id="202" v="Throw yourself into the void to go to the next level."/><t id="203" v="Use the *space bar* on the keyboard to place a bomb next to Igor. These bombs *freeze the enemy*, but they don\'t have *any effect on you*."/><t id="204" v="A defeated enemy gives you an *item*. Don\'t be long in recovering it because it won\'t last eternally!"/><t id="205" v="Sometimes you must freeze the enemy *a few times* by pushing it into the void. Place another bomb beside the frozen monster to push it more."/><t id="206" v="Don\'t forget to collect the item before leaving the level."/><t id="207" v="You can *push the bombs*! Press the Space bar key again beside a bomb."/><t id="208" v="Put a bomb here and *press the Space bar key again*!"/><t id="209" v="Good! Do the same for the next one!"/><t id="210" v="This technique is very important! Learn well how to use it."/><t id="211" v="There are different types of bombs. Pass through the *energy field* to change the type of bomb."/><t id="212" v="*Green bombs* explode when they come in contact with an enemy, like *mines*!"/><t id="213" v="Place a *green mine* here!"/><t id="214" v="Pay attention: if you kick a green mine, it explodes *when it touches the ground*!"/><t id="215" v="You can change the type of bomb again when you pass through other energy fields."/><t id="216" v="You\'ll see that *Igor\'s scarf changes its color* depending on the bomb type"/><t id="217" v="idlue bombs* knock out an enemy and they make it *fall from a platform*."/><t id="218" v="Pay attention because blue bombs explode more quickly."/><t id="219" v="Some items have surprising effects that will help you on your adventure!"/><t id="220" v="Some of them will have positive effects, others won\'t. It\'s up to you to discover them!"/><t id="221" v="Accumulate the letters from the word *CRISTAL* that will appear throughout your adventure."/><t id="222" v="All the enemies will be destroyed and you\'ll be able to go directly to the next level!"/><t id="223" v="Use *red* bombs to propel yourself onto the highest platforms. *Place a red bomb here and don\'t move*!"/><t id="224" v="Oh well, what are you doing here?"/><t id="225" v="No! Don\'t go that way!!"/><t id="226" v="Well that\'s enough..."/><t id="227" v="You can jump higher *dropping bombs during the jump*. If you do this at the precise moment, this will give you a little push."/><t id="228" v="Well done!"/><t id="229" v="You can *jump farther* thanks to this techique. Drop a bomb during the jump."/><t id="230" v="Drop the bomb right at the moment *when you start to go down* during the jump."/><t id="231" v="Good!"/><t id="232" v="Let\'s see if you reach the exit at this level!"/><t id="233" v="The Learning mode has finished! Let\'s start in earnest..."/><t id="234" v="*Secret*: there\'s a secret door in the *first level* of this Learning level. Place *a bomb just on the right, in the first level*!"/><t id="235" v="You can jump through the ceiling *to go up a platform*."/><t id="236" v="To jump over this hole, press the *up arrow key* and then *leave the left arrow key* pressed."/><t id="237" v="Nearly! *Go back to the right* and try again."/><t id="238" v="There\'s an *enemy* here!"/><t id="239" v="You can only leave a level if all the enemies have been defeated."/><t id="240" v="Don\'t worry if you die. Unlike the other game modes in Hammerfest, in the Learning mode you have *unlimited lives*!"/><t id="241" v="Good! Go down again *on the right*..."/><t id="242" v="If you can\'t do it, you can continue through the passage on the left. *Jump at the right moment when the red bomb explodes*!"/><t id="243" v="No, not here :) In the *first level of this Learning*! For the moment you can simply continue."/><t id="244" v="This door is a Learning shortcut. To continue, follow the arrow!"/><t id="245" v="Don\'t go any farther! It\'s the end of the world!"/><t id="246" v="This bucket won\'t fall."/><t id="247" v="You won\'t be able to go further!"/><t id="248" v="*Red bombs* can also help an ennemy to escape from a blocked place."/><t id="400" v="Here you have a sample of what you\'ll find in the Caverns of Hammerfest... *Jump into the void to continue!*"/><t id="401" v="This is it! You\'re ready to start the great adventure!"/><t id="402" v="If you haven\'t done it yet, you only have to *create an account* and guide Igor to the very far end of the caverns..."/><t id="403" v="Hammerfest: more than *100 levels* to explore, more than *150 items* to collect and *15 different types of enemies*."/><t id="404" v="At the bottom of the game screen you see the *number of lives left*, the *number of the level* and *your score*."/><t id="405" v="Once you have created an account, you\'ll be able to win extra lives during the game or thanks to the *quests*"/><t id="406" v="A quest consists of collecting a certain number of items spread all over the game."/><t id="407" v="Each time you play a game your score is registered and you enter into the *general ranking*!"/><t id="408" v="Go on, don\'t be shy and join the other players!"/><t id="500" v="Utilisez la *barre espace* pour poser une bombe près du fruit. Ces bombes *gèlent les ennemis*, mais n\'ont *aucun effet sur vous*."/><t id="501" v="Faites attention aux *pièges* !!"/><t id="502" v="Bravo ! Vous avez réussi à terminer cette série de niveaux ! Hammerfest vous attend, avec ses *centaines de niveaux uniques* et ses *milliers de surprises* !"/><t id="503" v="Essayez donc d\'attraper ce *joyau*... Attention, les *scies ne peuvent pas être détruites* !"/><t id="300" v="Tip:"/><t id="301" v="You can drop a bomb while you are falling to take advantage of a slight extra impulse."/><t id="302" v="Press the M key to stop the game music."/><t id="303" v="A frozen monster can kill others if it strikes them at high speed!"/><t id="304" v="If you kill several monsters with one single bomb, you will receive better rewards."/><t id="305" v="If you reach 100.000 points, you will gain an additional life."/><t id="306" v="Some letters of the word CRYSTAL are more rare than others (\' L \' or \' S \' for example)"/><t id="307" v="In every level you will receive a point item, an effect item and some letters for the word CRYSTAL."/><t id="308" v="The giant crystal at the end of some levels does not fall incidentally..."/><t id="309" v="Forming the word CRYSTAL is useful, but it\'s possible to make it even better..."/><t id="310" v="After dropping a bomb, press SPACE again to kick it!"/><t id="311" v="Press CONTROL + SHIFT + K to end the game (your progress will be saved)."/><t id="312" v="Press the F key to show the speed of the game (shown in frames per second, the ideal value is 30 or higher)."/><t id="313" v="Press C to show the map of the caverns!"/></statics><families><family id="0" name="Basic items"/><family id="1" name="First aid kit"/><family id="2" name="Massive destruction"/><family id="3" name="Mushrooms"/><family id="4" name="Deck of cards"/><family id="5" name="Zodiac signs"/><family id="6" name="Zodiac potions"/><family id="7" name="Heart 1"/><family id="8" name="Heart 2"/><family id="9" name="Heart 3"/><family id="10" name="Pirate treasure"/><family id="11" name="Motion-Twin Gadgets"/><family id="12" name="Norwegian experimental armament"/><family id="13" name="Ancient light"/><family id="14" name="Igor-Newton"/><family id="15" name="Snowflake 1"/><family id="16" name="Snowflake 2"/><family id="17" name="Snowflake 3"/><family id="18" name="Car Kit"/><family id="19" name="Mario Party"/><family id="1000" name="Little treats"/><family id="1001" name="Classic food"/><family id="1002" name="Bobble Cane"/><family id="1003" name="Precious stones"/><family id="1004" name="Junk food"/><family id="1005" name="Exotic!"/><family id="1006" name="Aperitives"/><family id="1007" name="Kapital Risk"/><family id="1008" name="Great Predator Trophies"/><family id="1009" name="Good-looking Harry\'s delights"/><family id="1010" name="Chocolates"/><family id="1011" name="Cheese"/><family id="1012" name="Canned food"/><family id="1013" name="Vegetables"/><family id="1014" name="MotionTwin Delights"/><family id="1015" name="MotionTwin survival kit"/><family id="1016" name="Time for tea"/><family id="1017" name="The cake Boy"/><family id="1018" name="Laws of Robotics"/><family id="1019" name="Fighter\'s trophies"/><family id="1020" name="Ice-cream Keys"/><family id="1021" name="Wine Key"/><family id="1022" name="Paperwork"/><family id="1023" name="Lost Keys"/><family id="1024" name="Glad sticks"/><family id="1025" name="Funny drinks"/><family id="1026" name="Daddy\'s administratrive material"/><family id="1027" name="Mythical artifacts"/><family id="1028" name="Sandy kit"/><family id="1029" name="Ninjutsu awards"/><family id="1030" name="Ninjutsu artefact"/></families><items><item id="0" name="Cristal Alphabet"/><item id="1" name="Golden Shield"/><item id="2" name="Silver Shield"/><item id="3" name="Beach Ball"/><item id="4" name="Magic Lamp"/><item id="5" name="Black Magic Lam"/><item id="6" name="Inner Peace"/><item id="7" name="Running Shoes"/><item id="8" name="Gold Star"/><item id="9" name="Bad eye"/><item id="10" name="Telephone"/><item id="11" name="Red Umbrella"/><item id="12" name="Blue Umbrella"/><item id="13" name="Biker Helmet"/><item id="14" name="Blue Hallucinogenic Mushroom"/><item id="15" name="Red Delight Mushroom"/><item id="16" name="Green Beer Mushroom"/><item id="17" name="Golded Mushroom"/><item id="18" name="Tropical Dandelion"/><item id="19" name="Explosive Sunflower"/><item id="20" name="Treasure Chest"/><item id="21" name="Jukebox Igor"/><item id="22" name="Heavy Weight Boots"/><item id="23" name="Orb of Ice"/><item id="24" name="Iceberg Shower"/><item id="25" name="Snow Flame"/><item id="26" name="Light Bulb"/><item id="27" name="Special Lilypad"/><item id="28" name="Silver Trophy"/><item id="29" name="Ring of Fire"/><item id="30" name="Blue Shades"/><item id="31" name="Red Shades"/><item id="32" name="Ace of Spades"/><item id="33" name="Ace of Clubs"/><item id="34" name="Ace of Diamonds"/><item id="35" name="Ace of Hearts"/><item id="36" name="Golden Igor"/><item id="37" name="Necklace of Frost"/><item id="38" name="Dino Totem Pole"/><item id="39" name="Granite Easter Island"/><item id="40" name="Sagittarius"/><item id="41" name="Capricorn"/><item id="42" name="Leo"/><item id="43" name="Taurus"/><item id="44" name="Libra"/><item id="45" name="Aries"/><item id="46" name="Scorpio"/><item id="47" name="Cancer"/><item id="48" name="Aquarius"/><item id="49" name="Gemini"/><item id="50" name="Pisces"/><item id="51" name="Virgo"/><item id="52" name="Potion of Sagitarius"/><item id="53" name="Potion of Capricorn"/><item id="54" name="Potion of Leo"/><item id="55" name="Potion of Taurus"/><item id="56" name="Potion of Libra"/><item id="57" name="Potion of Aries"/><item id="58" name="Potion of Scorpio"/><item id="59" name="Potion of Cancer"/><item id="60" name="Potion of Aquarius"/><item id="61" name="Potion of Gemini"/><item id="62" name="Potion of Pisces"/><item id="63" name="Potion of Virgo"/><item id="64" name="Honey Rainbow"/><item id="65" name="Rubber Ducky"/><item id="66" name="Magical Cactus"/><item id="67" name="Ring of William Tell"/><item id="68" name="Candle of Light"/><item id="69" name="Turtle Islands"/><item id="70" name="Four-Leaf Clover"/><item id="71" name="Dragon\'s Breath"/><item id="72" name="Magician\'s Hat"/><item id="73" name="Leaf"/><item id="74" name="Spirit of Orange"/><item id="75" name="Spirit of Rain"/><item id="76" name="Spirit of Trees"/><item id="77" name="Blue Crested Fish"/><item id="78" name="Flame Fish"/><item id="79" name="Fish Emperor"/><item id="80" name="Snail Speed"/><item id="81" name="Murloc\'s Pearl"/><item id="82" name="Eye of Providence"/><item id="83" name="Final Judgment"/><item id="84" name="Scorpion Talisman"/><item id="85" name="Hammer Time 3000"/><item id="86" name="Ghost Candy"/><item id="87" name="King Larvae"/><item id="88" name="Hare Nochi Guuuuu!"/><item id="89" name="Dragon Egg"/><item id="90" name="Slippery Bananas"/><item id="91" name="Luffy\'s Hat"/><item id="92" name="Chopper\'s Hat"/><item id="93" name="Mail Box"/><item id="94" name="Antok Ring"/><item id="95" name="Tuber\'s Bag"/><item id="96" name="Flameberg Pearl"/><item id="97" name="Green-Moss Pearl "/><item id="98" name="Super-Purple Pearl "/><item id="99" name="Chourou\'s Hair"/><item id="100" name="Pocket-Guu"/><item id="101" name="Surprise Package"/><item id="102" name="Igor\'s Carrot"/><item id="103" name="Wonderful Life!"/><item id="104" name="Heart of an Adventurer"/><item id="105" name="Final Destiny"/><item id="106" name="Book of Mushrooms"/><item id="107" name="Book of Stars"/><item id="108" name="Green Umbrella"/><item id="109" name="Snowflake"/><item id="110" name="Glacier"/><item id="111" name="Gon Ga Jora"/><item id="112" name="Carnivorous PiouPiou"/><item id="113" name="The Robe of Tuber"/><item id="114" name="Mario Mode!"/><item id="115" name="The Helmet of Volleyfest"/><item id="116" name="The Ankhel Jewel"/><item id="117" name="Gordon\'s Key"/><item id="1000" name="Hammerfest Crystal"/><item id="1001" name="Hamburger"/><item id="1002" name="Candy Cane"/><item id="1003" name="Seed Candy"/><item id="1004" name="Raindrop Candy"/><item id="1005" name="Cherry Candy"/><item id="1006" name="Blue Raspberry Lollipop"/><item id="1007" name="Green Apple Lollipop"/><item id="1008" name="Difaïned Diamond"/><item id="1009" name="Eye of the Tiger"/><item id="1010" name="12kg Jade"/><item id="1011" name="Moon Glade"/><item id="1012" name="Surprise Cake"/><item id="1013" name="Muffin"/><item id="1014" name="Cake"/><item id="1015" name="Wedding Cake"/><item id="1016" name="Piece of Cake"/><item id="1017" name="Magical Stones"/><item id="1018" name="Strawberry Ice Cream"/><item id="1019" name="Ice Cream Cup"/><item id="1020" name="Noodles"/><item id="1021" name="Frozen Chicken"/><item id="1022" name="Bizarre Liquid"/><item id="1023" name="Taffy"/><item id="1024" name="Strange Liquid"/><item id="1025" name="Raw Egg"/><item id="1026" name="Potato"/><item id="1027" name="Red Licorice"/><item id="1028" name="Plain Egg"/><item id="1029" name="Sausage on a Stick"/><item id="1030" name="Cherry on a Stick"/><item id="1031" name="Cheese on a Stick"/><item id="1032" name="Green Olive on a Stick"/><item id="1033" name="Black Olive on a Stick"/><item id="1034" name="Eyeball on a Stick"/><item id="1035" name="Jello on a Stick"/><item id="1036" name="Gouda on a Stick"/><item id="1037" name="Fish on a Stick"/><item id="1038" name="Stuffed Olive on a Stick"/><item id="1039" name="Mr. Radish"/><item id="1040" name="Fish Sushi"/><item id="1041" name="Lollipop"/><item id="1042" name="Chocolate Mousse"/><item id="1043" name="Plastic Sorbet"/><item id="1044" name="Manda"/><item id="1045" name="Cashew"/><item id="1046" name="Gump"/><item id="1047" name="Sapphire Mushroom"/><item id="1048" name="Ruby Mushroom"/><item id="1049" name="Emerald Mushroom"/><item id="1050" name="Sliver Mushroom"/><item id="1051" name="Secret Coin"/><item id="1052" name="Silver Coin"/><item id="1053" name="Gold Coin"/><item id="1054" name="Big Unemployment"/><item id="1055" name="Soda"/><item id="1056" name="French Fries"/><item id="1057" name="Slice of Pizza"/><item id="1058" name="Hazelnut Chocolate"/><item id="1059" name="Chocolate Eclair"/><item id="1060" name="Red Plant"/><item id="1061" name="Parrot Head"/><item id="1062" name="Alien Blob"/><item id="1063" name="Tagada-Strawberry"/><item id="1064" name="Car-En-Sac"/><item id="1065" name="Dragibus"/><item id="1066" name="Crocodile"/><item id="1067" name="Half Cocobats"/><item id="1068" name="Happy Cola"/><item id="1069" name="Peanut"/><item id="1070" name="Small Ghost"/><item id="1071" name="Hard Cookie"/><item id="1072" name="Christmas Log"/><item id="1073" name="Hot Pepper"/><item id="1074" name="Bread"/><item id="1075" name="Bouquet"/><item id="1076" name="Daruma Watermelon"/><item id="1077" name="Grain"/><item id="1078" name="Croissant"/><item id="1079" name="Beans"/><item id="1080" name="Chocolate Bunny"/><item id="1081" name="Beer"/><item id="1082" name="Jelly with Corn"/><item id="1083" name="Chocolate with Almond"/><item id="1084" name="Dark Chocolate"/><item id="1085" name="Belgium Chocolate"/><item id="1086" name="White Chocolate with Coffee Bean"/><item id="1087" name="Vodka Chocolate"/><item id="1088" name="Snail Chocolate"/><item id="1089" name="Marble Chocolate"/><item id="1090" name="Cerisot Marinated in Beer"/><item id="1091" name="Raw Ham"/><item id="1092" name="Sliced Sausage"/><item id="1093" name="Red Remains"/><item id="1094" name="Maple Syrup Roll"/><item id="1095" name="Blood Sausage"/><item id="1096" name="Kangaroo Liver"/><item id="1097" name="Sausage of St.Morgelet"/><item id="1098" name="Head Cheese"/><item id="1099" name="Squished Sausage"/><item id="1100" name="Lemon Canned-Fish"/><item id="1101" name="Canned Sliced Sausages"/><item id="1102" name="White Beans"/><item id="1103" name="Canned Lychees"/><item id="1104" name="Zion\'s Cheese"/><item id="1105" name="Pâté"/><item id="1106" name="Mozzarella Cheese"/><item id="1107" name="Emmental Cheese"/><item id="1108" name="Edam Cheese"/><item id="1109" name="Blue Cheese"/><item id="1110" name="Swiss Cheese"/><item id="1111" name="Cheesecake"/><item id="1112" name="Lettuce"/><item id="1113" name="Leek"/><item id="1114" name="Eggplant"/><item id="1115" name="Walnut"/><item id="1116" name="Hazelnut"/><item id="1117" name="Cucumber"/><item id="1118" name="Tomato"/><item id="1119" name="Radish"/><item id="1120" name="Green Beans"/><item id="1121" name="Onion"/><item id="1122" name="Garlic"/><item id="1123" name="Shallots"/><item id="1124" name="River Salt"/><item id="1125" name="Green Pepper"/><item id="1126" name="Yellow Pepper"/><item id="1127" name="Red Pepper"/><item id="1128" name="Digested Broccoli"/><item id="1129" name="Bitten Radish"/><item id="1130" name="Snow Peas"/><item id="1131" name="White Beans"/><item id="1132" name="Blended Pears"/><item id="1133" name="Artichoke"/><item id="1134" name="Cauliflower"/><item id="1135" name="Purple Cabbage"/><item id="1136" name="Corn"/><item id="1137" name="Dutch Cake"/><item id="1138" name="Chocolate Fudge Cake"/><item id="1139" name="Sparkling Soda"/><item id="1140" name="Baked Cracker"/><item id="1141" name="Golden Pastry"/><item id="1142" name="Project Finisher"/><item id="1143" name="Almond Laxative"/><item id="1144" name="Ghost Toast"/><item id="1145" name="Flat Bread"/><item id="1146" name="Cracked Nut"/><item id="1147" name="Crunchy Almond"/><item id="1148" name="Chestnut"/><item id="1149" name="Ramen Noodles"/><item id="1150" name="Steamed Brioche"/><item id="1151" name="Slice of Chocolate Hazelnut Bread"/><item id="1152" name="Slice of Raspberry Hazelnut Bread"/><item id="1153" name="Slice of Sticky Orange Bread"/><item id="1154" name="Slice of Hard Honey Bread"/><item id="1155" name="Earthworm"/><item id="1156" name="Cream Grenade"/><item id="1157" name="Chocolate-Dipped Cream Puffs"/><item id="1158" name="Chocolate Delight"/><item id="1159" name="Shiny Mushroom"/><item id="1160" name="Peppermint Candy"/><item id="1161" name="Breaded Anchovies"/><item id="1162" name="Grapefruit Sushi"/><item id="1163" name="Octopus in Ink"/><item id="1164" name="Curly"/><item id="1165" name="Strawberry Tart"/><item id="1166" name="Pear Egg"/><item id="1167" name="Tasteless Sticky Truffle"/><item id="1168" name="Sardines"/><item id="1169" name="Starfish"/><item id="1170" name="Sugar Transformer"/><item id="1171" name="Evil Robot"/><item id="1172" name="RoboCop"/><item id="1173" name="R2D2"/><item id="1174" name="Dalek ! Exterminate !"/><item id="1175" name="Robo-Malin"/><item id="1176" name="Johnny 6"/><item id="1177" name="Biscuit Transformer"/><item id="1178" name="Statue: Sorbex Lemon"/><item id="1179" name="Statue: Bombino"/><item id="1180" name="Statue: Baddybomb Pear"/><item id="1181" name="Statue: Tagada-Strawberry"/><item id="1182" name="Statue: Kiwi Sapper"/><item id="1183" name="Statue: Leaper"/><item id="1184" name="Statue: Armaggedon-Pineapple"/><item id="1185" name="Bristling Ring"/><item id="1190" name="Wooden Master Key"/><item id="1191" name="Rigor Dangerous Key"/><item id="1192" name="Méluzzine Key"/><item id="1193" name="Wine Key"/><item id="1194" name="Frozen Key"/><item id="1195" name="Old Rusty Key"/><item id="1196" name="Authorization of Bois-Joli"/><item id="1197" name="Key of Difficult Worlds "/><item id="1198" name="Pungent Key"/><item id="1199" name="Tuber\'s Key"/><item id="1200" name="Nightmare Key"/><item id="1201" name="Sony Controller"/><item id="1202" name="Nintendo 64 Controller"/><item id="1203" name="Game-Cube Controller"/><item id="1204" name="Genesis Controller"/><item id="1205" name="SNES Controller"/><item id="1206" name="NES Controller"/><item id="1207" name="Master System Controller"/><item id="1208" name="Joystick"/><item id="1209" name="Can Beer"/><item id="1210" name="Sealed Beer"/><item id="1211" name="Draft Beer"/><item id="1212" name="Wonderful Wine"/><item id="1213" name="Evil Liquor"/><item id="1214" name="MT Stamp"/><item id="1215" name="Free Bill"/><item id="1216" name="Sticky Notes"/><item id="1217" name="Pencil Container"/><item id="1218" name="Chaos Stapler"/><item id="1219" name="Mirror of Bancal"/><item id="1220" name="Star of the Devil"/><item id="1221" name="Shovel"/><item id="1222" name="Bucket of Sand"/><item id="1223" name="Sand Castle"/><item id="1224" name="Winkel O\'Riely"/><item id="1225" name="Mirror of Sables"/><item id="1226" name="Star of the Devil\'s Twins"/><item id="1227" name="Mixed Heart"/><item id="1228" name="Symbol of the Ninjas"/><item id="1229" name="Magic Katana"/><item id="1230" name="Fūma Shuriken"/><item id="1231" name="Shuriken"/><item id="1232" name="Kunai"/><item id="1233" name="Blowgun of Precision"/><item id="1234" name="Singing Ocarina"/><item id="1235" name="Armor of the Night"/><item id="1236" name="Award of Merit"/><item id="1237" name="Snow Bomb"/><item id="1238" name="Pyramid Pass"/></items><quests><quest id="0" title="Constellations">\t\t\tFrom now on and forever, Igor can place 2 bombs!</quest><quest id="1" title="Zodiac mixture">\t\t\tIgor knows how to place his bombs vertically now and he can place them\n\t\t\ton the upper platform! Press DOWN arrow key beside a placed bomb.</quest><quest id="2" title="First steps">\t\t\tFrom now on Igor will always start the game with an extra life.</quest><quest id="3" title="The adventure begins">\t\t\tIgor will start from now on the game with another extra life.</quest><quest id="4" title="An epic fate">\t\t\tIgor will start the game with another life!</quest><quest id="5" title="Perseverance">\t\t\tIgor will start the game with an extra life.</quest><quest id="6" title="Delicacies">\t\t\tNew food, healthier and nicer, will appear now in the game.</quest><quest id="7" title="Some sugar!">\t\t\tIgor wants more sweet things! From now on sweets will appear in the game.</quest><quest id="8" title="Malnutrition">\t\t\tIgor&apos;s taste for badly-balanced food will lead him to find even more food of questionable quality.</quest><quest id="9" title="Good taste">\t\t\tIgor&apos;s weird diet brings exotic new food to the game!</quest><quest id="10" title="Technological advance">\t\t\tA lot of items of weird effects appear now in the game to help you in\n\t\t\tthe adventure!</quest><quest id="11" title="Small guide to mushrooms">\t\t\tIgor has discovered a rare text. It&apos;s a kind of cookbook about hallucinogenic\n\t\t\tmushrooms. Igor will be able to find them now.</quest><quest id="12" title="Find the secret golden coins!">\t\t\tGreat new richness will appear now in the game!</quest><quest id="13" title="The book of Magic Stars">\t\t\tThis mysterious in-depth book talks about the 12 Zodiac Constellations.\n\t\t\tThey are now available in the game!</quest><quest id="14" title="Armageddon">\t\t\tItems with devastating powers will appear now in the game!</quest><quest id="15" title="MotionTwin Diet">\t\t\tAfter eating every kind of food, no matter what, Igor has become a Master of\n\t\t\tthe games! Now he will be able to collect rare items and exceptional food\n\t\t\tduring his adventure!</quest><quest id="16" title="Creator of games">\t\t\tIgor appreciates basic food. He could be offered a nice career in videogames!\n\t\t\tAdapted food will be available in the game, and also a deck of cards!</quest><quest id="17" title="Life is like a box of chocolates...">\t\t\tDelicious chocolates will be now available in Hammerfest!</quest><quest id="18" title="Difaïned treasure">\t\t\tAfter collecting so many diamonds of mysterious origin, Igor adquires the\n\t\t\tability of finding precious stones! He&apos;ll be able now to find them throughout\n\t\t\this quests.</quest><quest id="19" title="Super size me!">\t\t\tKeeping far from good food, Igor discovers now that he can also eat\n\t\t\tcanned food!</quest><quest id="20" title="Master jeweller">\t\t\tIgor has become an expert in precious stones. Now his knowledge permits him to\n\t\t\tdiscover precious stones which are even more powerful!</quest><quest id="21" title="Great Predator">\t\t\tIgor is fed up hunting sweets! As he has turned into an unconditional predator,\n\t\t\tnow he will be able to devour any sort of delicacy.</quest><quest id="22" title="Expert in salads and stews">\t\t\tThey say that Igor is the reencarnation of Saladou, the world famous master\n\t\t\tof salads for virtual rabbits. With this new talent, he will be able to collect\n\t\t\ta great variety of vegetables!</quest><quest id="23" title="Hammerfest Feast">\t\t\tAfter a complete lunch, Igor will now have access to really delicious cakes.</quest><quest id="24" title="Birthday party">\t\t\tIgor has found all the required elements for his next birthday party!\n\t\t\tFrom now on he will get even more until that time comes.</quest><quest id="25" title="Bon vivant">\t\t\tWe can&apos;t imagin a good lunch without a nice aperitive.\n\t\t\tIgor knows now what he needs.</quest><quest id="26" title="Norwegian fondue">\t\t\tThe smells that come from Igor&apos;s lair don&apos;t leave a doubt about it: he has become\n\t\t\ta great lover of cheese. New dairy products will appear now in the caverns.</quest><quest id="27" title="Guu\'s mistery">\t\t\tThis quest hasn&apos;t any practical use. It&apos;s only to suggest to you the great\n\t\t\tanime &quot;Jungle wa itsumo Hare nochi Guu&quot;. Banyaaaaaïï ^__^</quest><quest id="28" title="Divine sweets">\t\t\tSweets are no longer a secret to Igor. Now he will be able to collect\n\t\t\tany of Good-looking Harry&apos;s legendary sweets that he finds in Hammerfest!</quest><quest id="29" title="Igor and Cortex">\t\t\tIgor has started to make mysterious gadgets... get ready to collect strange\n\t\t\tmachines in the game!</quest><quest id="30" title="Facing the darkness">\t\t\tIgor has learnt to light himself up! Thanks to this torch he won&apos;t\n\t\t\tget lost any more in the darkness of the advanced levels!</quest><quest id="31" title="And light appeared!">\t\t\tReady for every danger, Igor won&apos;t ever be afraid of the darkness of\n\t\t\tthe advanced levels!</quest><quest id="32" title="Christmas in Hammerfest!">\t\t\tYou won 5 extra lives!!</quest><quest id="33" title="Happy birthday Igor">\t\t\tYou won 10 free games!!</quest><quest id="34" title="Celestial present">\t\t\tYou won 20 free games!!</quest><quest id="35" title="Game purchase enhanced">\t\t\tFrom now on, each time you buy games with our payment service, you&apos;ll\n\t\t\tobtain more games for the same price!</quest><quest id="36" title="Sorbex exterminator">\t\t\tYou squeezed the Sorbex lemons without mercy!</quest><quest id="37" title="Bombino disposal expert">\t\t\tYou survived the Bombinos explosions!</quest><quest id="38" title="Pear killer">\t\t\tYou killed a new monster: the Baddybombs Pears!</quest><quest id="39" title="Tagadas mixer">\t\t\tYou have only had a Tagada-Strawberry mouthful!</quest><quest id="40" title="Kiwi itches! ouch, it scratches!">\t\t\tYou knew how to avoid all the traps from the Kiwi Sappers!</quest><quest id="41" title="Leaping Hunter">\t\t\tGreat, you haven&apos;t been impressed by the herd of Leapers!</quest><quest id="42" title="Armaggedon-Pineapple Killer">\t\t\tYou have gloriously come to the end of the fight against the\n\t\t\tArmageddon-Pineapples!</quest><quest id="43" title="Hammerfest King">\t\t\tYou finished the game! Your perseverance and tenacity permitted you to\n\t\t\tdefeat the evil Tuber and to recover Igor&apos;s nose. From now on you&apos;ll\n\t\t\thave an extra life and you&apos;ll play with the carrot!</quest><quest id="44" title="Mad hatter">\t\t\tIgor has discovered a mad passion for hats. From now on, disguise Igor by pressing\n\t\t\t&quot;D&quot; key.</quest><quest id="45" title="Eco-terrorist pony">\t\t\tIgor has accumulated enough wealth to finance his terrorist activities\n\t\t\tin Hammerfest. From now on, he will be able to disguise himself by pressing &quot;D&quot;\n\t\t\tkey during the game! By the way, have you already played www.dinoparc.com?</quest><quest id="46" title="The Pioupiou is in you">\t\t\tWe should have expected that: after collecting no matter what kind of item,\n\t\t\tIgor has finished by devouring a Pioupiou! Press &quot;D&quot; key during the game to change\n\t\t\tfancy dress.</quest><quest id="47" title="Mushrooms hunter">\t\t\tLike his Italian counterpart (plumber in a red hat), Igor feels a\n\t\t\tgreat passion for mushrooms! You&apos;ll be able from now on to change Igor&apos;s\n\t\t\tdress during the game by pressing &quot;D&quot; key.</quest><quest id="48" title="Tuber\'s successor">\t\t\tIt seems like Igor has... changed. His look has become cool. He hides\n\t\t\tbehind a great cloak and his face is dark. He&apos;s maybe changing little by\n\t\t\tlittle into the monster who was once his enemy.\n\t\t\tDisguise Igor as Tuber by pressing &quot;D&quot; key during the game!</quest><quest id="49" title="The first Key!">\t\t\tIgor has found a Master Key. Undoubtedly, it will open a door somewhere in the\n\t\t\tcaverns...</quest><quest id="50" title="Rigor Dangerous">\t\t\tYou have discovered an old Rusty Key in your adventure!\n\t\t\tIt has a small inscription: &quot;Rick&quot;. Obviously, this was its previous owner.</quest><quest id="51" title="The lost Meluzin">\t\t\tThey say that the Meluzin, the legendary key of the ancient Hammerfest\n\t\t\ttales, opens the door to the greatest wealth. But where is the door?</quest><quest id="52" title="At last, the drunk man!">\t\t\tThis strange little key smells of wine: it&apos;s the Wine Key!</quest><quest id="53" title="Frozen">\t\t\tLike any other snowman, Igor can&apos;t keep a key like this one in his hand.\n\t\t\tThe door that it opens gives access to the most hidden places of Hammerfest.</quest><quest id="54" title="A rusty key">\t\t\tThis amorphous metallic thing does not seem to be worth a lot. Although,\n\t\t\twe should not trust appearances! Who knows what adventures hide beyond\n\t\t\tthe door it opens?</quest><quest id="55" title="Let it go!">\t\t\tAll your administrative power will be backed up by the LovelyWood BJ22a\n\t\t\tAuthorization Form.</quest><quest id="56" title="The Arduous Worlds">\t\t\tYou achieved level 50 in the Nightmare mode! You win the Arduous\n\t\t\tWorlds Key!</quest><quest id="57" title="Quickly!">\t\t\tYou don&apos;t really know why, but the Itchy Key you&apos;ve found, makes you\n\t\t\trun and roll everywhere!</quest><quest id="58" title="Empty Tuber\'s pocket">\t\t\tTuber, the evil sorcerer, used to have a key...</quest><quest id="59" title="Tuber, Master of Hell">\t\t\tAll your great power and skill in combat techniques gave you the\n\t\t\tchance to defeat Tuber in the Nightmare mode! You&apos;re now the privileged\n\t\t\towner of the Unique Key.</quest><quest id="60" title="Metallic water">\t\t\tAlcohol abuse is bad for your health, according to the health\n\t\t\tauthorities.\n\t\t\tHowever, in Hammerfest it has enabled you to unblock the Wine\n\t\t\tKey! From now on, you have a lot of chances of finding it in\n\t\t\tthe caverns.</quest><quest id="61" title="Paperwork">\t\t\tIgor is an expert in filling in administrative forms. You&apos;ll soon\n\t\t\tfind the LovelyWood BJ22a Authorization Form in the caverns.</quest><quest id="62" title="Best Player">\t\t\tThe best videogame player is you, there&apos;s no doubt about it! Your\n\t\t\tcontrol pads collection is enviable! From now on, Igor has a speed bonus\n\t\t\tat the beginning of each game.</quest><quest id="63" title="Mirror, my beautiful mirror">\t\t\tThe Bancal Mirror you have found in the game shows you things from\n\t\t\tanother point of view. The extra option &quot;Mirror&quot; has been unblocked!</quest><quest id="64" title="Nightmare mode">\t\t\tYou&apos;re talented. Very talented... but, are you able to help Igor\n\t\t\tin the Nightmare mode? This new option has been unblocked!</quest><quest id="65" title="The adventure continues!">\t\t\tThe Carrotian Council has chosen you to explore the\n\t\t\tCaverns of Hammerfest in-depth. The Gordon&apos;s Key is only the first step of\n\t\t\ta new great mission...</quest><quest id="66" title="Ankhel Jewel">\t\t\tYou have shown your great ability and dexterity in finding the Ankhel Jewel.\n\t\t\tThe Advanced Ball Control option has been unblocked in the Soccerfest mode game!</quest><quest id="67" title="Sandy\'s adventure begins!">            You’ve got all the necessary elements to bring Sandy the Sandman to life!\n            This new character can join you in the Adventure Mode (Two players mode\n            on the same computer).           </quest><quest id="68" title="Mirror, OUR beautiful mirror">            You can now see things from a different angle with your friend. The game option &quot;Mirror&quot;\n            has been added to the Two Players game mode.</quest><quest id="69" title="Double-nightmare option">            It is evident that you are able to do great things together with a friend! The game option &quot;Nightmare&quot;\n            has been added to the Two Players game mode.</quest><quest id="70" title="A great Friendship">            Better late than never! Igor and Sandy have finally understood the importance of staying together\n            in order to survive in the Caverns. The game option &quot;Lives sharing&quot; has been unblocked for\n            the Two Players mode! With this option enabled, if a player loses his latest life, he will\n            take a life from the second player and then he will be able to continue playing!</quest><quest id="71" title="Learning how to launch Shurikens">            Jumping and jumping around has improved Igor&apos;s agility and now he&apos;s turning into a ninja!\n            In order to prove his courage, he must collect all the ninja items hidden in Hammerfest!</quest><quest id="72" title="Shinobi do!">            Igor has completed his starting quest and now he&apos;s a grand master of Ninjutsu weapons!\n            Nevertheless, his Code of Honour prevents him from using them. Although now he will be able\n            to test his skills in the &quot;Ninjutsu&quot; game mode he has unblocked!</quest><quest id="73" title="As quick as the lightning!">            Igor is unbeatable when he needs to move fast. He now has to prove it to the others!\n            The TIME ATTACK mode and its ranking have been unblocked. Be the fastest snowman in all Hammerfest!</quest><quest id="74" title="Bomb Master">            You are a well-known expert in explosive material. To demonstrate this to everybody, you can\n            now enable the &quot;Unstable explosives&quot; option in the Adventure mode. Pay attention to everything\n            that explodes in the game! You can also push the bombs farther and move while you hit them.</quest><quest id="75" title="Tuber\'s tomb">            You have discovered a bizarre pocket-size pyramid in Tuber&apos;s tomb.</quest></quests><dimensions><dimension id="0"><level id="0" name="The Village of Hammerfest"/><level id="2" name="The First Caverns"/><level id="10" name="The Slippery Tunnel"/><level id="16" name="The Forgotten Depths"/><level id="20" name="Caverns of Green"/><level id="31" name="The Cold Caverns"/><level id="41" name="The Stairs of Morgenfell"/><level id="51" name="Territory of the Rolling Mangoes"/><level id="61" name="Cavern of the Plum"/><level id="71" name="Underground Jungle"/><level id="81" name="The Colorful Caves"/></dimension><dimension id="1"><level id="0" name="The Temple Before Tuber"/><level id="12" name="Plumbers Hideout"/><level id="14" name="BubbleFest"/><level id="24" name="Caves of Loop"/><level id="33" name="Small Bonus"/><level id="35" name="Diffiland"/><level id="41" name="Pretty Forest of Tournefleur"/><level id="55" name="Tuber\'s Secret Base"/><level id="60" name="Cavavin"/><level id="66" name="Cavavin: Secret Room"/><level id="68" name="Secret Tubzian Prototype"/><level id="70" name="Inferior Frimas"/><level id="76" name="Crystal Trap"/><level id="78" name="Secret forest of Hammerfest"/><level id="80" name="Den of Géluloz"/><level id="86" name="Room of the Guardian"/><level id="90" name="The Second Well"/><level id="108" name="Haste Bonus"/><level id="110" name="The Troglogrotto"/><level id="119" name="The World of Mirrors"/><level id="125" name="Cavern of Nausea"/><level id="127" name="The Carrotian Council "/><level id="134" name="The Dojo"/><level id="141" name="Hell"/><level id="148" name="Sachas Mixer"/><level id="149" name="Well of Zâmes"/><level id="154" name="The Lair of the Raspberries"/><level id="157" name="Howling Metal"/><level id="168" name="Raspberry Crypt"/><level id="188" name="The Tomb of Tuber"/><level id="206" name="The Entrance to the Tomb"/><level id="211" name="The Real Tomb of Tuber"/><level id="217" name="The Counselors Hideout"/></dimension><dimension id="2"/><dimension id="3"/></dimensions><keys><key id="0" name="Wooden Master Key"/><key id="1" name="Rigor Dangerous Key"/><key id="2" name="Méluzzine Key"/><key id="3" name="Wine Key"/><key id="4" name="Frozen Key"/><key id="5" name="Old Rusty Key"/><key id="6" name="Authorization of Bois-Joli"/><key id="7" name="Key of Difficult Worlds"/><key id="8" name="Pungent Key"/><key id="9" name="Skeleton Key"/><key id="10" name="Nightmare Key"/><key id="11" name="Key of inversion"/><key id="12" name="Gordon\'s Key"/><key id="13" name="Key of the apprentice"/><key id="14" name="Pyramid Pass"/></keys></lang>'
    setVariable
  end // of frame 0

  frame 0
    push 'esXml'
    push '<lang id="es"><statics><t id="1" v="Nivel"/><t id="2" v="Aprendizaje"/><t id="3" v="siguiente nivel"/><t id="4" v="¡RÁPIDO!"/><t id="5" v="Instrucciones"/><t id="6" v="Haz clic aquí para continuar..."/><t id="7" v="Desplazarse"/><t id="8" v="Poner una bomba"/><t id="9" v="Pausa"/><t id="10" v="ESPACIO"/><t id="11" v="¡PERFECTO!"/><t id="12" v="¡PELIGRO, MONSTRUO!"/><t id="13" v="Nueva zona"/><t id="14" v="Zona: "/><t id="15" v="Es necesario Flash 8 para jugar a Hammerfest. Puedes descargarlo gratuitamente aquí:"/><t id="16" v="Bosque de Hammerfest"/><t id="17" v="Caverna del aprendiz"/><t id="18" v="MINUTO"/><t id="19" v="MINUTOS"/><t id="20" v="¡CONTRARRELOJ!"/><t id="21" v="Prepárate..."/><t id="22" v="¡GOL!"/><t id="23" v="EN PROPIA PUERTA"/><t id="24" v="¡Saque!"/><t id="25" v="Fin del partido"/><t id="26" v="VICTORIA: ¡IGOR!"/><t id="27" v="VICTORIA: ¡SANDY!"/><t id="28" v="¡PARTIDO NULO!"/><t id="29" v="Desplazarse"/><t id="30" v="Golpear"/><t id="31" v="Patada vertical"/><t id="32" v="Lleva el balón a la portería contraria. ¡Todo está permitido! Este modo es para 2 jugadores en el mismo ordenador."/><t id="33" v="Presiona la tecla P para obtener ayuda"/><t id="34" v="Teleportación imposible de realizar"/><t id="35" v="Bonus vidas: "/><t id="36" v="Tubercarrera"/><t id="37" v="Tu tiempo:"/><t id="38" v="Tiempo medio por nivel:"/><t id="39" v="Tiempo:"/><t id="40" v="Llave requerida:"/><t id="41" v="Igor ha usado:"/><t id="42" v="Bomba"/><t id="43" v="ha sido eliminado"/><t id="44" v="¡Objetivo no permitido!"/><t id="45" v="¡Objetivo eliminado!"/><t id="50" v="Viajando a Hammerfest..."/><t id="51" v="km"/><t id="52" v="¡Ya has llegado!"/><t id="53" v="Haz clic aquí para comenzar..."/><t id="54" v="Inicio de la partida..."/><t id="55" v="Guardando partida..."/><t id="56" v="Por favor, espera unos instantes..."/><t id="57" v="Una partida va a ser descontada."/><t id="58" v="¡Este modo es gratis e ilimitado!"/><t id="59" v="----[ MODE DEV LOCAL ]----"/><t id="60" v="Error en el envío de datos, intento: "/><t id="61" v="Cargando música..."/><t id="62" v="¡Todo está listo!"/><t id="63" v="Lectura de pistas..."/><t id="64" v="Ha ocurrido un error en la descarga de datos."/><t id="65" v="Debes instalar la última versión de Flash"/><t id="66" v="El servidor de ficheros no ha sido encontrado."/><t id="67" v="Error de descarga"/><t id="68" v="Error en el inicio de la descarga"/><t id="69" v="¡Error en la descarga de música!"/><t id="70" v="¡Error en la descarga del juego! Consulta el Soporte Técnico para Vacíar la memoria caché de tu navegador."/><t id="71" v="Guardando la partida..."/><t id="72" v="Error para terminar de guardar la partida."/><t id="73" v="Guardado con éxito :)"/><t id="74" v="Casi..."/><t id="100" v="¡Salta *en el pozo* para empezar la aventura!"/><t id="101" v="¡Pasaje secreto!"/><t id="102" v="Una risa inquietante resuena desde lo más profundo del pozo..."/><t id="103" v="¡Has descubierto un *pasaje secreto*!"/><t id="104" v="¡Acabas de liberar a las últimas frutas sometidas a Tubérculo!"/><t id="105" v="A tan sólo unos metros, se oye una jauría de horribles idombinos*..."/><t id="106" v="¡Los Bombinos han sido destruidos! Igor ya puede continuar su aventura."/><t id="107" v="Se oyen extraños ruidos que provienen de niveles inferiores..."/><t id="108" v="Entras en la guarida de las *Peras Malabombas*..."/><t id="109" v="¡Igor ha vencido a las Peras Malabombas!"/><t id="110" v="La temperatura ha bajado de golpe... ¡Igor ha llegado al antro de los *Sorbetex*!"/><t id="111" v="¡Los terribles Limones Sorbetex han sido abatidos! Igor puede continuar su aventura..."/><t id="112" v="Igor acaba de llegar a un partido de Fresbol. ¡Habrá que vencer al equipo de las *Tagadas*!"/><t id="113" v="El equipo de las *Tagadas* ha sido vencido por 4 a 0!"/><t id="114" v="¡Igor ha entrado en la caverna de los *Kiwis Zapadores*!"/><t id="115" v="¡Has vencido a los Kiwis Zapadores!"/><t id="116" v="La prueba del *Destino*"/><t id="117" v="Has entrado en un lugar secreto y olvidado... El *Templo de los Desafíos* !"/><t id="118" v="El desafío de la *Destreza*"/><t id="119" v="Has *fracasado* en esta prueba"/><t id="120" v="¡Has *superado* esta difícil prueba!"/><t id="121" v="El desafío del *Corage*"/><t id="122" v="¡Has abierto un *atajo*!"/><t id="123" v="Oyes el silbido inconfundible de sandías hammerfestianas... no hay duda ¡estás en territorio de *Sandinas*!"/><t id="124" v="La última Sandina cae abatida por el hielo de Igor..."/><t id="125" v="BETA-TEST: cette porte mène au niveau 51."/><t id="126" v="*La oscuridad* invade las cavernas a medida que desciendes..."/><t id="127" v="El suelo tiembla... ¡prepárate para afrontar a los *Piñaguedones*!"/><t id="128" v="La calma por fin regresa tras la derrota del último Piñaguedón..."/><t id="129" v="Elige el terreno para este partido. Puedes *darle a la tecla P* para obtener ayuda en cualquier momento."/><t id="130" v="Este *terreno especial* está dedicado al VolleyFest!"/><t id="131" v="¡Has encontrado la *Joya de Ankhel*! Rick estaría orgulloso de ti..."/><t id="132" v="La *Joya de Ankhel* ha sido sellada para siempre. ¡Tanto esfuerzo para nada!"/><t id="133" v="¡Protege el botón rojo!"/><t id="134" v="¡Recoge el máximo número posible de cristales!"/><t id="135" v="¡La puerta acaba de abrirse!"/><t id="136" v="Un hedor sospechoso de moho emana de los niveles inferiores..."/><t id="137" v="Algo horrible parece haber germinado del Bastón de Bobble... ¡Estás en el antro de los Bobbles Caídos!"/><t id="138" v="Los Bobbles han sido vencidos... Pero visto el tamaño de este Bastón, es muy posible que haya dejado un gran rastro en las cavernas."/><t id="139" v="*NeOJuAnChO99*: Bienvenido a la *Guarida de los Zanahorios y Panteonianos*, te esperábamos."/><t id="140" v="*Uchi*: Somos el reflejo de ti mismo. Provenimos de una dimensión paralela."/><t id="141" v="*GuzbelSFLO*: Una entitad superior nos ha enviado aquí para ayudarte en tus exploraciones y mostrarte la Vía."/><t id="142" v="*Serjah*: ¡Oca y oca y tiro porque me toca!"/><t id="143" v="*Motimer*: Aquí tienes la *Llave de Gordon* que te permitirá abrir el acceso a nuestra cueva."/><t id="144" v="*Phoenix1985*: Hemos escondido en el fondo de esta cueva un idastón de Bobble*, ¡un objeto de leyenda y de gran valor!"/><t id="145" v="*Motimer*: Fue escondido aquí hace mucho tiempo... antes de la Gran Actualización..."/><t id="146" v="*Todos*: ooooooooh.... ¡la Gran Actualización! "/><t id="147" v="*NeOJuAnChO99*: ¡SILENCIO!"/><t id="148" v="*NeOJuAnChO99*: Te confío la dura tarea de *encontrar el Bastón de Bobble*. Puedes partir."/><t id="149" v="*NeOJuAnChO99*: ¡El Bastón de Bobble ha sido *corrompido*!"/><t id="150" v="*Todos*: ooooooooh...."/><t id="151" v="*Serjah*: ¡Corrompido! Oca y oca..."/><t id="152" v="*Motimer*: ¡Apostaría a que esta corrupción se debe a los grandes cambios!"/><t id="153" v="*GuzbelSFLO*: Debes volver a explorar las cavernas, Igor. No hay duda de que *hay más lugares secretos* por descubrir..."/><t id="154" v="*NeOJuAnChO99*: ¡Es cierto! ¡No cabe duda! Debes estar atento, Igor. Explora cada rincón de este mundo..."/><t id="155" v="*NeOJuAnChO99*: ¡¡Hemos descubierto recientemente que hay numerosos secretos escondidos en *Las Cavernas de Hammerfest*!!"/><t id="156" v="*Phoenix1985*: ¿como el de poner una bomba en el Pozo?"/><t id="157" v="*Todos*: ooooooooooooooooh...."/><t id="158" v="¡Te encuentras en el modo de juego *Ninjutsu*! Elimina prioritariamente a todo enemigo marcado con una *CALAVERA*. ¡No mates a los enemigos que tienen un *CORAZÓN*!"/><t id="159" v="El ruido viscoso que se oye desde estas cavernas no deja ninguna duda: ¡estás en *la caverna de las Framhuecas*!"/><t id="160" v="¡Has vencido a las primeras *Framhuecas*! Pero se diría que no serán las últimas..."/><t id="161" v="¡Cuidado! Esta región es extremadamente peligrosa y difícil. ¡Ni lo intentes!"/><t id="162" v="¡Tan solo los más temerarios pasarán!"/><t id="163" v="Una voz femenina dulce y tranquila anuncia: *\'Acceso no permitido a ninjas\'*."/><t id="200" v="Usa las *flechas del teclado* para desplazarte de derecha a izquierda."/><t id="201" v="¡Para franquear este muro, salta pulsando la tecla *de la flecha superior*!"/><t id="202" v="Déjate caer al vacío para pasar al siguiente nivel."/><t id="203" v="Usa la *barra espacio* del teclado para poner una bomba junto a un enemigo. Estas bombas *congelan al enemigo*, pero no tienen *ningún efecto sobre ti*."/><t id="204" v="Un enemigo derrotado te aporta un *objeto*. ¡No tardes en recuperarlo pues no durará eternamente!"/><t id="205" v="A veces tendrás que congelar *varias veces* al enemigo para conseguir empujarle hasta el vacío. Pon otra bomba junto al monstruo congelado para empujarle más."/><t id="206" v="No olvides coger el objeto antes de abandonar el nivel."/><t id="207" v="¡Puedes *empujar las bombas*! Dale de nuevo a la tecla Espacio junto a la bomba para empujarla."/><t id="208" v="¡Pon una bomba aquí y *dale a la tecla Espacio de nuevo*!"/><t id="209" v="¡Bien! ¡Repite la misma operación para el siguiente!"/><t id="210" v="¡Esta técnica es muy importante! Aprende bien a manejarla."/><t id="211" v="Hay varios tipos de bomba. Pasa por el *campo de energía* para cambiar de tipo de bomba."/><t id="212" v="Las *bombas verdes* al contactar con un enemigo"/><t id="213" v="¡Pon una *bomba verde* aquí!"/><t id="214" v="Fíjate bien en que si golpeas a una bomba verde, ¡ésta explotará *en cuanto toque el suelo*!"/><t id="215" v="Puedes cambiar de nuevo de tipo de bomba al franquear otros campos de energía."/><t id="216" v="Verás que *la bufanda de Igor cambia de color* según el tipo de bomba"/><t id="217" v="Las bombas *azules* permiten dejar KO a un enemigo y *hacerle caer de plataforma*."/><t id="218" v="Fíjate en que la bombas azules explotan más rápidamente."/><t id="219" v="¡Algunos objetos tienen efectos sorprendentes que te ayudarán en la aventura!"/><t id="220" v="Algunos tendrán efectos positivos, otros no. ¡En tu mano está descubrirlos!"/><t id="221" v="Acumula las letras de la palabra *CRISTAL* que aparecerán a lo largo de tu aventura."/><t id="222" v="¡Todos los enemigos serán destruidos y podrás acceder directamente al siguiente nivel!"/><t id="223" v="Usa las bombas *rojas* para propulsarte hacia las plataformas muy altas. *¡Pon una bomba roja aquí y no te muevas*!"/><t id="224" v="¡Anda mira! ¿pero qué haces aquí?"/><t id="225" v="¡No! ¡¡Por ahí no!!"/><t id="226" v="Bueno ya te vale..."/><t id="227" v="Puedes saltar más alto *lanzando bombas en pleno salto*. Esto te dará un pequeño empuje suplementario si lo haces en el buen momento."/><t id="228" v="¡Bien hecho!"/><t id="229" v="Esta misma técnica te permite también *saltar más lejos*. Lanza una bomba durante el salto."/><t id="230" v="Lanza la bomba justo *cuando empiezas a descender* durante el salto."/><t id="231" v="¡Bien!"/><t id="232" v="¡Veamos si consigues llegar a la salida de este nivel!"/><t id="233" v="¡El aprendizaje ha finalizado! Pasemos a cuestiones más serias..."/><t id="234" v="*Secreto*: hay una puerta secreta en el *primer nivel* de este Aprendizaje ¡Pon *una bomba justo a la derecha, en el primer nivel*!"/><t id="235" v="Puedes saltar a través del techo para *subir una plataforma*."/><t id="236" v="Para saltar por encima de este paso, dale a la tecla *flecha superior* y después *mantén la tecla izquierda* pulsada."/><t id="237" v="¡Casi! *Vuelve a subir por la derecha* e inténtalo de nuevo."/><t id="238" v="¡Aquí tienes un *enemigo*!"/><t id="239" v="Sólo puedes abandonar el nivel si los enemigos han sido vencidos."/><t id="240" v="No te preocupes si mueres. Contrariamente a los otros modos de juego de Hammerfest, ¡en el modo Aprendizaje tienes *vidas ilimitadas*!"/><t id="241" v="¡Bien! Vuelve a bajar *por la derecha*..."/><t id="242" v="Si no lo consigues, puedes continuar por el pasaje a la izquierda. *Salta bien en el preciso instante en el que la bomba roja explota*!"/><t id="243" v="No, por aquí no :) ¡En el *primer nivel de este Aprendizaje*! De momento puedes simplemente continuar."/><t id="244" v="Esta puerta te permite acortar el Aprendizaje. Para continuar, ¡sigue la flecha!"/><t id="245" v="¡No vayas más lejos! ¡Es el fin del mundo!"/><t id="246" v="Este cubo no se caerá."/><t id="247" v="¡No podrás ir más lejos!"/><t id="248" v="Las *bombas rojas* pueden igualmente sacar a un enemigo de una situación bloqueada."/><t id="400" v="He aquí un pequeño avance de lo que encontrarás en las Cavernas de Hammerfest... *¡Salta al vacío para continuar!*"/><t id="401" v="¡Ya estás listo para empezar la gran aventura!"/><t id="402" v="Si no es el caso, no tienes más que *inscribirte* y guíar a Igor al fondo de las Cavernas de Hammerfest..."/><t id="403" v="Hammerfest: más de *100 niveles* a explorar, más de *150 objetos* a coleccionar y *15 tipos de enemigos* diferentes"/><t id="404" v="En la zona inferior de la pantalla de juego puedes ver las *vidas restantes*, el *número del nivel* y *tu puntuación*."/><t id="405" v="Una vez inscrito, podrás ganar vidas extra durante el juego o gracias a las *exploraciones*"/><t id="406" v="Una exploración consiste en recoger un cierto número de objetos repartidos a lo largo del juego."/><t id="407" v="¡En cada partida tu puntuación es guardada y entras en la *clasificación piramidal*!"/><t id="408" v="¡Venga, atrévete y lánzate a la aventura!"/><t id="500" v="Utilisez la *barre espace* pour poser une bombe près du fruit. Ces bombes *gèlent les ennemis*, mais n\'ont *aucun effet sur vous*."/><t id="501" v="Faites attention aux *pièges* !!"/><t id="502" v="Bravo ! Vous avez réussi à terminer cette série de niveaux ! Hammerfest vous attend, avec ses *centaines de niveaux uniques* et ses *milliers de surprises* !"/><t id="503" v="Essayez donc d\'attraper ce *joyau*... Attention, les *scies ne peuvent pas être détruites* !"/><t id="300" v="Consejo: "/><t id="301" v="Pon una bomba justo cuando empiezas a caer para aprovechar un impulso extra."/><t id="302" v="Pulsa la tecla M para cortar temporalmente la música durante la partida."/><t id="303" v="¡Un monstruo congelado puede también matar a otros si se les golpea con la suficiente velocidad!"/><t id="304" v="Si matas a varios monstruos con una sola bomba, recibes bonus más importantes."/><t id="305" v="Si consigues 100.000 puntos, ganas una vida extra."/><t id="306" v="Algunas letras de la palabra CRISTAL son más raras que otras (la \'L\' o la \'S\', por ejemplo)"/><t id="307" v="En cada nivel recibes un objeto de puntos, un objeto especial y algunas letras."/><t id="308" v="El inmenso cristal que cae a veces al final de algunos niveles no cae por casualidad..."/><t id="309" v="Formar la palabra CRISTAL no está mal, pero en realidad puedes hacerlo mejor..."/><t id="310" v="¡Pulsa de nuevo la tecla Espacio junto a una bomba ya puesta y la empujarás!"/><t id="311" v="Pulsa CONTROL + SHIFT + K para abandonar la partida (así será guardada)."/><t id="312" v="Mantén la tecla F pulsada para mostrar la velocidad del juego (el valor ideal es 30)."/><t id="313" v="Si pulsas C, ¡verás el Mapa del Mundo!"/></statics><families><family id="0" name="Objetos de base"/><family id="1" name="Kit de primeros auxilios"/><family id="2" name="Destrucción masiva"/><family id="3" name="Setas"/><family id="4" name="Baraja de cartas"/><family id="5" name="Signos del zodíaco"/><family id="6" name="Pociones del zodíaco"/><family id="7" name="Corazón 1"/><family id="8" name="Corazón 2"/><family id="9" name="Corazón 3"/><family id="10" name="Tesoro de los piratas"/><family id="11" name="Cachibaches Motion-Twin"/><family id="12" name="Armamento noruego experimental"/><family id="13" name="Alumbrado antiguo"/><family id="14" name="Igor-Newton"/><family id="15" name="Copo 1"/><family id="16" name="Copo 2"/><family id="17" name="Copo 3"/><family id="18" name="Kit de manos libres"/><family id="19" name="Fiesta Mario"/><family id="1000" name="Pequeñas travesuras"/><family id="1001" name="Alimentos clásicos"/><family id="1002" name="Golosinas"/><family id="1003" name="Piedras preciosas"/><family id="1004" name="Comida basura"/><family id="1005" name="¡Exotismo!"/><family id="1006" name="Aperitivos"/><family id="1007" name="Kapital Risk"/><family id="1008" name="Trofeos del Gran Depredador"/><family id="1009" name="Delicias de Harry \'el guapo\'"/><family id="1010" name="Bombones de chocolate"/><family id="1011" name="Quesos"/><family id="1012" name="Conservas"/><family id="1013" name="Legumbres"/><family id="1014" name="Delicias MotionTwin"/><family id="1015" name="Kit de supervivencia MotionTwin"/><family id="1016" name="Hora de la merienda"/><family id="1017" name="El Chico pastelero"/><family id="1018" name="Las leyes de la robótica"/><family id="1019" name="Trofeos del peleón"/><family id="1020" name="Llaves de Helados"/><family id="1021" name="Llave Borracha"/><family id="1022" name="Papeleo"/><family id="1023" name="Llaves Perdidas"/><family id="1024" name="Bastones de alegría"/><family id="1025" name="Bebidas divertidas"/><family id="1026" name="Material administrativo del Papá"/><family id="1027" name="Artefactos míticos"/><family id="1028" name="Sandy en kit"/><family id="1029" name="Recompensas del Ninjutsu"/><family id="1030" name="Artefactos del Ninjutsu"/></families><items><item id="0" name="Alfabeto Cristalino"/><item id="1" name="Escudo de madera y oro"/><item id="2" name="Escudo plateado"/><item id="3" name="Balón de playa"/><item id="4" name="Lámpara Pidedeseo"/><item id="5" name="Lámpara Negra"/><item id="6" name="Paz interior"/><item id="7" name="Botín IcePump"/><item id="8" name="Estrella de las nieves"/><item id="9" name="Ojo malo"/><item id="10" name="Teléfono-no-no"/><item id="11" name="Paraguas rojo"/><item id="12" name="Paraguas azul"/><item id="13" name="Casco"/><item id="14" name="Delicia alucinógena azul"/><item id="15" name="Seta chuli y roja"/><item id="16" name="Pequeña Walalu de los bosques"/><item id="17" name="Seta dorada trepadora"/><item id="18" name="Flor tropical"/><item id="19" name="Giraluna"/><item id="20" name="Cofre de Anarchipiélago"/><item id="21" name="Altavoz Bájaloya"/><item id="22" name="Zapato viejo con agujeros"/><item id="23" name="Bola cristalina"/><item id="24" name="Pedrusco de los montes"/><item id="25" name="Llama fría"/><item id="26" name="Bombilla 30 watios"/><item id="27" name="Posa-ranas"/><item id="28" name="Baratija de plata"/><item id="29" name="Anillo \'Termostato 8\'"/><item id="30" name="Gafas volteadoras azules"/><item id="31" name="Gafas revolteadoras rojas"/><item id="32" name="As de picas"/><item id="33" name="As de tréboles"/><item id="34" name="As de diamantes"/><item id="35" name="As de corazones"/><item id="36" name="Igoro"/><item id="37" name="Collar refrescante"/><item id="38" name="Tótem de los Dinos"/><item id="39" name="Cabeza de granito"/><item id="40" name="Sagitario"/><item id="41" name="Capricornio"/><item id="42" name="Leo"/><item id="43" name="Tauro"/><item id="44" name="Libra"/><item id="45" name="Aries"/><item id="46" name="Escorpión"/><item id="47" name="Cáncer"/><item id="48" name="Acuario"/><item id="49" name="Géminis"/><item id="50" name="Piscis"/><item id="51" name="Virgo"/><item id="52" name="Elixir de Sagitario"/><item id="53" name="Elixir de Capricornio"/><item id="54" name="Elixir de Leo"/><item id="55" name="Elixir de Tauro"/><item id="56" name="Elixir de Libra"/><item id="57" name="Elixir de Aries"/><item id="58" name="Elixir de Escorpión"/><item id="59" name="Elixir de Cáncer"/><item id="60" name="Elixir de Acuario"/><item id="61" name="Elixir de Géminis"/><item id="62" name="Elixir de Acuario"/><item id="63" name="Elixir de Virgo"/><item id="64" name="Arcomiel"/><item id="65" name="Flotador de pato"/><item id="66" name="Rama de Kipik"/><item id="67" name="Anillo de Omaíta"/><item id="68" name="Vela"/><item id="69" name="Tortuga de las islas"/><item id="70" name="Trébol común"/><item id="71" name="¡Emana calor!"/><item id="72" name="Sombrero de Mago Gris"/><item id="73" name="Hoja de árbol"/><item id="74" name="Espíritu de la naranja"/><item id="75" name="Espíritu de la lluvia"/><item id="76" name="Espíritu de los árboles"/><item id="77" name="Lucidjané con cresta azul"/><item id="78" name="Fibroso rojito"/><item id="79" name="Pez emperador"/><item id="80" name="Caracol Noempujes"/><item id="81" name="Perla de nácar de los murlocs"/><item id="82" name="Juicio penúltimo"/><item id="83" name="Juicio final"/><item id="84" name="Talismán escorpión"/><item id="85" name="Martillo"/><item id="86" name="Sorpresa fantasmagórica"/><item id="87" name="Larva de oenopterius"/><item id="88" name="Pokuté"/><item id="89" name="Ojo de Tzongre"/><item id="90" name="Fulguro pies-pesados"/><item id="91" name="Sombrero de paja de Luffy"/><item id="92" name="Sombrero de Tony Tony Chopper"/><item id="93" name="Buzón zon"/><item id="94" name="Anillo Antok"/><item id="95" name="Saco de Tubérculo"/><item id="96" name="Perla flameante"/><item id="97" name="Perla verdeante"/><item id="98" name="Gran pe-perla"/><item id="99" name="Pelos de Chourou"/><item id="100" name="Guu de bolsillo"/><item id="101" name="Paquete sorpresa"/><item id="102" name="La Zanahoria de Igor"/><item id="103" name="Vida palpitante"/><item id="104" name="Vida aventurosa"/><item id="105" name="Vida épica"/><item id="106" name="Libro de los hongos"/><item id="107" name="Libro de las estrellas"/><item id="108" name="Paraguas Frutiparc"/><item id="109" name="Copo de nieve simple"/><item id="110" name="Copo de nieve raro"/><item id="111" name="Copo de nieve gigantesco"/><item id="112" name="Píopío carnívoro"/><item id="113" name="Capa de Tubérculo"/><item id="114" name="Modo Mario"/><item id="115" name="Casco de Volleyfest"/><item id="116" name="Joya de Ankhel"/><item id="117" name="Llave de Gordon"/><item id="1000" name="Cristales de Hammerfest"/><item id="1001" name="Pan de carne"/><item id="1002" name="Bastón de Bobble"/><item id="1003" name="Caramelo Berlinmalva"/><item id="1004" name="Caramelo Chamagros"/><item id="1005" name="Caramelo rosa-praliné"/><item id="1006" name="Chupakups de frutas azules"/><item id="1007" name="Chupakups de clorofila"/><item id="1008" name="Diamante difaïned"/><item id="1009" name="Ojo del Tigre"/><item id="1010" name="Jade de 12kg"/><item id="1011" name="Reflejo-de-luna"/><item id="1012" name="Sorpresa de Cerezas"/><item id="1013" name="Magdalena de piedras"/><item id="1014" name="Pastel de madera"/><item id="1015" name="Tarta suprema de frambuesas"/><item id="1016" name="Tarta muy rica"/><item id="1017" name="Piedras del Cambio"/><item id="1018" name="Helado de fresa"/><item id="1019" name="Copa helada"/><item id="1020" name="Fideos chinos"/><item id="1021" name="Pollo congelado"/><item id="1022" name="Líquido raro"/><item id="1023" name="Gran azucarado"/><item id="1024" name="Líquido extraño"/><item id="1025" name="Huevo crudo"/><item id="1026" name="Gran ñan-ñan"/><item id="1027" name="Chuchería"/><item id="1028" name="Huevo frito"/><item id="1029" name="Salchica con palillo de dientes"/><item id="1030" name="Cereza con palillo de dientes"/><item id="1031" name="Queso con palillo de dientes"/><item id="1032" name="Aceituna de Estepa"/><item id="1033" name="Aceituna negra de Estepa"/><item id="1034" name="Ojo del cochino"/><item id="1035" name="Aperitivo de Blandiblú"/><item id="1036" name="Queso holandés blando"/><item id="1037" name="Tapa de calamar"/><item id="1038" name="Aceituna olvidada"/><item id="1039" name="Señor rábano"/><item id="1040" name="Sushi de atún"/><item id="1041" name="Piruleta"/><item id="1042" name="Delicia de Arale"/><item id="1043" name="Polo de plástico"/><item id="1044" name="Manda"/><item id="1045" name="Pétalo misterioso"/><item id="1046" name="Gump"/><item id="1047" name="Seta azulada"/><item id="1048" name="Rojeante"/><item id="1049" name="Verdifiante"/><item id="1050" name="Rompedientes"/><item id="1051" name="Moneda de oro secreta"/><item id="1052" name="Moneda de plata"/><item id="1053" name="Moneda de oro"/><item id="1054" name="Un montón de dinero"/><item id="1055" name="Bebida kipik"/><item id="1056" name="Dedos-de-Tubérculo"/><item id="1057" name="Pizza de Donatello"/><item id="1058" name="Canelé de Burdeos"/><item id="1059" name="Rebanada de pan con crema de cacao y avellana"/><item id="1060" name="Nadie sabe lo que es"/><item id="1061" name="Loro decapitado en salsa"/><item id="1062" name="Morvo-morfo"/><item id="1063" name="Chuchería de fresa"/><item id="1064" name="Píldoras de mentira"/><item id="1065" name="Bolitas de chuches"/><item id="1066" name="Chuchería de cocodrilo"/><item id="1067" name="Chuche de infancia irlandesa"/><item id="1068" name="Una botellita de cola"/><item id="1069" name="Cacahuete secreto"/><item id="1070" name="Fantasmita"/><item id="1071" name="Cookie deshidratada"/><item id="1072" name="Tronco de Navidad"/><item id="1073" name="Pimiento del piquillo"/><item id="1074" name="Helado de Soja Max"/><item id="1075" name="Ramo de carne"/><item id="1076" name="Daruma-Sandía"/><item id="1077" name="Semilla de girasol"/><item id="1078" name="Croissant confitado"/><item id="1079" name="Judía perezosa"/><item id="1080" name="Conejo de chocolate"/><item id="1081" name="Hidromiel de los Bosques"/><item id="1082" name="Gota viscosa"/><item id="1083" name="Bombón de nueces"/><item id="1084" name="Emi-Praliné"/><item id="1085" name="Bombón con pepitas de chocolate"/><item id="1086" name="Yumi de café"/><item id="1087" name="Esponjita de gel de Vodka"/><item id="1088" name="Caracol de chocolate de pasta de queso"/><item id="1089" name="Fósil de chocolate de márbol de foie gras"/><item id="1090" name="Caramelo marinado a la cerveza"/><item id="1091" name="Jamón"/><item id="1092" name="Lomo embuchado de jabugo"/><item id="1093" name="Mortadela torcida"/><item id="1094" name="Bacon al sirope de fresa"/><item id="1095" name="Salchichón de jabato salvaje"/><item id="1096" name="Rebanada de Canguro"/><item id="1097" name="Salchichón de la Sierra de Aracena"/><item id="1098" name="Pavo trufado sin cabello de ángel"/><item id="1099" name="Salchichón maldito"/><item id="1100" name="Lata de atún en aceite de oliva"/><item id="1101" name="Minisalchichas en conserva"/><item id="1102" name="Judías blancas"/><item id="1103" name="Litchis incaducables"/><item id="1104" name="Fabada asturiana en conserva"/><item id="1105" name="Conserva de lentejas madrileñas con chorizo"/><item id="1106" name="Camembert"/><item id="1107" name="Emmental"/><item id="1108" name="Queso barnizado"/><item id="1109" name="Roquefort"/><item id="1110" name="Queso fresco"/><item id="1111" name="Quesito"/><item id="1112" name="Ensalado"/><item id="1113" name="Puerro de agua"/><item id="1114" name="Cacahuete aceitoso de malva"/><item id="1115" name="Manzana de piedra"/><item id="1116" name="Patatas dulces"/><item id="1117" name="Porra bio"/><item id="1118" name="Tomate pacífico"/><item id="1119" name="Rabanox"/><item id="1120" name="Judías verdes"/><item id="1121" name="Cebolla Sapik"/><item id="1122" name="Ajos Sapu"/><item id="1123" name="Chalote"/><item id="1124" name="Sal que ríe"/><item id="1125" name="Pimiento verde"/><item id="1126" name="Pimiento amarillo"/><item id="1127" name="Pimiento rojo"/><item id="1128" name="Brocolis digerido"/><item id="1129" name="Rácimo de Rabanox"/><item id="1130" name="Lanza-guisantes"/><item id="1131" name="Judías blancas"/><item id="1132" name="Puñado de puerros de agua"/><item id="1133" name="Alcachofa"/><item id="1134" name="Coliflor"/><item id="1135" name="Lombarda"/><item id="1136" name="Disparador de Maíz"/><item id="1137" name="Sandwich tostado Holandés"/><item id="1138" name="Pastel XXL de choco-mantequilla"/><item id="1139" name="Gaseosa"/><item id="1140" name="Tarta salada de queso"/><item id="1141" name="Magdalena brioche"/><item id="1142" name="Café de fin de proyecto"/><item id="1143" name="Laxante de almendras"/><item id="1144" name="Smiley crujiente"/><item id="1145" name="Barquillo de lava"/><item id="1146" name="Nonuez"/><item id="1147" name="Almendra crujiente"/><item id="1148" name="Nuez"/><item id="1149" name="Fideos chinos deshidratados"/><item id="1150" name="Brioche de vapor"/><item id="1151" name="Tostada con crema de chocolate"/><item id="1152" name="Tostada con hemoglobina"/><item id="1153" name="Tostada de aceite"/><item id="1154" name="Tostada de mermelada pegajosa"/><item id="1155" name="Lombriz al natural"/><item id="1156" name="Granada de crema"/><item id="1157" name="Profiteroles apetitosos"/><item id="1158" name="Megaprofiterol"/><item id="1159" name="Champiñón podrido"/><item id="1160" name="Caramelo de reyes"/><item id="1161" name="Nem de anchoas"/><item id="1162" name="Surimi de pomelo"/><item id="1163" name="Calamar en su tinta"/><item id="1164" name="Curly"/><item id="1165" name="Tarta de frambuesa"/><item id="1166" name="Huevo de pera"/><item id="1167" name="Trufa pegajosa sin sabor"/><item id="1168" name="Sardinas en lata"/><item id="1169" name="Estrella Toulaho"/><item id="1170" name="Transformer de azúcar"/><item id="1171" name="Kitchisime"/><item id="1172" name="Igorocop"/><item id="1173" name="Arto deedo"/><item id="1174" name="¡Dalek, el Exterminador!"/><item id="1175" name="Robo-loco"/><item id="1176" name="Johnny 6"/><item id="1177" name="Galleta de Transformer"/><item id="1178" name="Estatua: Sorbetex de Limón"/><item id="1179" name="Estatua: Bombino"/><item id="1180" name="Estatua: Pera Malabomba"/><item id="1181" name="Estatua: Tagada"/><item id="1182" name="Estatua: Kiwi-Zapador"/><item id="1183" name="Estatua: Sandina"/><item id="1184" name="Estatua: Piñaguedón"/><item id="1185" name="Anillo de erizo azul"/><item id="1190" name="Llave Maestra de Madera"/><item id="1191" name="Llave de Rigor Dangerous"/><item id="1192" name="Meluzina"/><item id="1193" name="Llave del Bruto"/><item id="1194" name="Furtok Glacial"/><item id="1195" name="Vieja Llave Oxidada"/><item id="1196" name="Autorización Madera-Bonita"/><item id="1197" name="Llave de los Mundos Arduos"/><item id="1198" name="Llave Que Rasca"/><item id="1199" name="Llave Maestra de Tubérculo"/><item id="1200" name="Llave de las Pesadillas"/><item id="1201" name="Mando Suni"/><item id="1202" name="Mando Nientiendo 64"/><item id="1203" name="Mando Game-Pirámide"/><item id="1204" name="Mando Sey-Ga"/><item id="1205" name="Mando Super Nientiendo"/><item id="1206" name="Mando del Sistema Maestro"/><item id="1207" name="Mando Nientiendo Entertainment System"/><item id="1208" name="Mando S-Tehef"/><item id="1209" name="Lata Express"/><item id="1210" name="Cerveza de las fiestas en Bordeaux"/><item id="1211" name="Espuma voladora"/><item id="1212" name="Vino burgués del Gran Teatro"/><item id="1213" name="Licor maléfico"/><item id="1214" name="Sello MotionTwin"/><item id="1215" name="Factura gratis"/><item id="1216" name="Post-It de François, no entiendo tu letra"/><item id="1217" name="Lapicero solitario sin pegatina de perro"/><item id="1218" name="Grapadora del Caos"/><item id="1219" name="Espejo de la cábala"/><item id="1220" name="Estrella demoníaca"/><item id="1221" name="Polvo de playa mágica"/><item id="1222" name="Material de arquitecto"/><item id="1223" name="Maqueta de arena"/><item id="1224" name="Wink"/><item id="1225" name="Espejo de las Arenas"/><item id="1226" name="Estrella de los Diablos Gemelos"/><item id="1227" name="Sello de amistad"/><item id="1228" name="Insignia de la orden de los Ninjas"/><item id="1229" name="Cuchillo suizo japonés"/><item id="1230" name="Shuriken de segundo rango"/><item id="1231" name="Shuriken de entrenamiento"/><item id="1232" name="Najinata"/><item id="1233" name="Lanza-bolitas de Precisión"/><item id="1234" name="Ocarina cantante"/><item id="1235" name="Casco samurai"/><item id="1236" name="Insignia del Mérito"/><item id="1237" name="Nieve a la glicerina"/><item id="1238" name="Pase VIP Pirámide"/></items><quests><quest id="0" title="Las constelaciones">\t\t\tA partir de ahora y para siempre, ¡Igor puede poner 2 bombas!</quest><quest id="1" title="Influencias del zodíaco">\t\t\tIgor ya sabe lanzar verticalmente sus bombas y colocarlas en la\n\t\t\tplataforma superior. Pulsa ABAJO junto a una bomba ya lanzada.</quest><quest id="2" title="Primeros pasos">\t\t\tA partir de ahora Igor empezará siempre la partida con una vida extra.</quest><quest id="3" title="La aventura comienza">\t\t\tIgor empezará para siempre la partida con otra vida extra.</quest><quest id="4" title="Un destino épico">\t\t\t¡Igor empezará la partida con otra vida extra más!</quest><quest id="5" title="Perseverancia">\t\t\tIgor empezará la partida con una vida extra.</quest><quest id="6" title="Gourmand">\t\t\tNuevos alimentos, más sanos y apetitosos, aparecerán ahora en el juego.</quest><quest id="7" title="¡Quiero algo dulce!">\t\t\t¡Igor quiere azúcar! A partir de ahora aparecerán chucherías en el juego.</quest><quest id="8" title="Malnutrición">\t\t\tEl gusto de Igor por los alimentos poco equilibrados le permitirá encontrar\n\t\t\taún más alimentos de dudosa capacidad nutritiva.</quest><quest id="9" title="Gusto refinado">\t\t\t¡El régimen extraño de Igor trae nuevos y exóticos alimentos en el juego!</quest><quest id="10" title="Avance tecnológico">\t\t\t¡Un montón de objetos con efectos raros aparecen ahora en el juego para\n\t\t\tayudarte en tu aventura!</quest><quest id="11" title="La pequeña guía de hongos">\t\t\tIgor ha descubierto una obra extraña. Es una especie de libro de cocina\n\t\t\tsobre hongos alucinógenos. Ahora podrá encontrarlos en sus búsquedas.</quest><quest id="12" title="¡Encuentra las monedas de oro secretas!">\t\t\t¡Nuevas riquezas de gran valor aparecerán ahora en el juego!</quest><quest id="13" title="El grimorio de las Estrellas">\t\t\tEsta obra misteriosa habla extensamente sobre las 12 constelaciones del Zodíaco.\n\t\t\t¡Ya están disponibles en el juego!</quest><quest id="14" title="Armageddon">\t\t\t¡Objetos con efectos devastadores aparecerán ahora en el juego!</quest><quest id="15" title="Régimen MotionTwin">\t\t\tA base de comer cualquier cosa, ¡Igor se ha vuelto un maestro de los juegos!\n\t\t\tAhora podrá coleccionar objetos y alimentos muy raros durante su aventura.</quest><quest id="16" title="Creador de juegos innovadores">\t\t\tIgor aprecia los alimentos básicos, así que puede que se le presente\n\t\t\tuna bella carrera en el mundo de los videojuegos. Alimentos adaptados a él\n\t\t\tle serán a partir de ahora propuestos durante el juego, ¡así como una baraja\n\t\t\tde cartas!</quest><quest id="17" title="La vida es como una caja de bombones...">\t\t\t¡Deliciosos bombones tan sorprendentes como la vida misma son distribuidos ahora\n\t\t\ten Hammerfest!</quest><quest id="18" title="El tesoro difaïned">\t\t\tA base de recoger tantos diamantes de misteriosa aparición, ¡Igor ha adquirido\n\t\t\tla facultad de localizar las piedras preciosas raras! Ahora podrá encontrarlas\n\t\t\ta lo largo de sus exploraciones.</quest><quest id="19" title="Super size me!">\t\t\tComo siempre, bien lejos de la buena alimentación, Igor descubre que puede\n\t\t\ttambién alimentarse de alimentos en conserva.</quest><quest id="20" title="Maestro joyero">\t\t\tIgor se ha convertido en un auténtico experto en piedras preciosas. ¡Ahora sus\n\t\t\tconocimientos le permiten descubrir joyas preciosas aún más poderosas!</quest><quest id="21" title="Gran Depredador">\t\t\t¡Igor está bien harto cazar chucherías! Depredador incondicional, ahora podrá\n\t\t\tdevorar toda clase de charcutería.</quest><quest id="22" title="Experto en ensaladas y potajes">\t\t\tSe cuenta que Igor es la reencarnación de Ensalado, el reconocido maestro de la ensalada\n\t\t\tpara conejos virtuales. ¡Con su don podrá recoger una gran variedad de verduras!</quest><quest id="23" title="Festín de Hammerfest">\t\t\tTras una comida tan completa, Igor está al fin listo para acceder a pasteles\n\t\t\tde exquisito gusto.</quest><quest id="24" title="Merienda de cumpleaños">\t\t\t¡Igor ha encontrado todos los elementos para asegurar una buena merienda\n\t\t\tde cumpleaños! A partir de ahora podrá obtener aún más dulces para una segunda merienda.</quest><quest id="25" title="Vividor">\t\t\tUna buena comida no puede ser concebida sin tomarse antes unas tapas.\n\t\t\tAhora Igor conoce bien lo que se necesita para un buen tapeo.</quest><quest id="26" title="Fondue noruega">\t\t\tLos olores que emanan de la guarida de Igor no dejan ninguna duda: ha caído\n\t\t\ten el lado pestoso de los quesos. Ahora aparecerán nuevos productos lácteos\n\t\t\ten las cavernas.</quest><quest id="27" title="Misterio de Guu">\t\t\tEsta exploración no tiene ningún interés, sólo el de aconsejarte el genial\n\t\t\tanime &quot;Jungle wa itsumo Hare nochi Guu&quot;. Banyaaaaaïï ^__^</quest><quest id="28" title="Chucherías divinas">\t\t\tLas golosinas ya no son ningún secreto para Igor. ¡Ahora podrá arrasar con\n\t\t\tcualquier chuchería legendaria de Harry &quot;el guapo&quot; que se le presente!</quest><quest id="29" title="Igor y Cortex">\t\t\tIgor se ha puesto a fabricar cachibaches misteriosos... ¡prepárate para\n\t\t\tcoleccionar máquinas extrañas en el juego!</quest><quest id="30" title="Afrontar la oscuridad">\t\t\t¡Igor ha aprendido a autoiluminarse! ¡Gracias a su antorcha incorporada ya\n\t\t\tno se perderá en la oscuridad de los niveles más avanzados!</quest><quest id="31" title="¡Y se hizo la luz!">\t\t\tPreparado para los peligros de la vida moderna, ¡Igor no temerá nunca más a la\n\t\t\toscuridad de los niveles avanzados!</quest><quest id="32" title="¡Navidad en Hammerfest!">\t\t\t¡¡Has ganado 5 vidas extra!!</quest><quest id="33" title="Feliz cumpleaños Igor">\t\t\t¡¡Has ganado 10 partidas gratis!!</quest><quest id="34" title="Regalo celestial">\t\t\t¡¡Has ganado 20 partidas gratis!!</quest><quest id="35" title="Compra de partidas mejorada">\t\t\tA partir de ahora cada vez que compres partidas con nuestro sistema de pago,\n\t\t\t¡obtendrás más partidas que antes por el mismo precio!</quest><quest id="36" title="Exterminador de Sorbetex">\t\t\t¡Has presionado sin piedad a los limones Sorbetex!</quest><quest id="37" title="Desactivador de Bombinos">\t\t\t¡Has sobrevivido a las explosiones de los Bombinos!</quest><quest id="38" title="Asesino de peras">\t\t\tHas matado un nuevo monstruo: ¡las Peras Malabombas!</quest><quest id="39" title="Triturador de Tagadas">\t\t\tReceta de Batido de fresa: azúcar, leche, un huevo y un buen montón de\n\t\t\tFresas Tagada. Poner nata al final para decorar.</quest><quest id="40" title="Kiwi rasca, vaya cómo pica...">\t\t\t¡Conseguiste evitar todas las trampas de los Kiwis Zapadores!</quest><quest id="41" title="Cazador de Sandinas">\t\t\tEstupendo, ¡no te has dejado impresionar por la manada de Sandinas!</quest><quest id="42" title="Aniquilador de Piñaguedones">\t\t\t¡Llegaste gloriosamente hasta el final en la lucha contra los Piñaguedones!</quest><quest id="43" title="Rey de Hammerfest">\t\t\t¡Te has pasado el juego! Tu perseverancia y tu tenacidad te permitieron\n\t\t\tderrotar al malvado Tubérculo y recuperar la nariz de Igor. ¡A partir de ahora\n\t\t\tjugarás siempre con una vida extra y la zanahoria!</quest><quest id="44" title="Sombrero loco">\t\t\tIgor ha descubierto una nueva pasión por los gorros. Podrás a partir de ahora\n\t\t\tdarle a la tecla &quot;D&quot; durante la partida para cambiar de disfraz.</quest><quest id="45" title="Poni eco-terrorista">\t\t\tIgor ha acumulado suficientes riquezas que le permiten financiar sus actividades\n\t\t\tterroristas en Hammerfest. ¡Ahora puede disfrazarse pulsando a la tecla &quot;D&quot;\n\t\t\tdurante la partida! Por cierto, ¿has jugado ya a www.dinoparc.es?</quest><quest id="46" title="El Pioupiouz está en ti">\t\t\tHabía que esperarlo: a base de recoger cualquier cosa, ¡Igor ha acabado\n\t\t\tdevorando un Píopío! Pulsa la tecla &quot;D&quot; durante la partida para cambiar\n\t\t\tde disfraz.</quest><quest id="47" title="Cazador de champiñones">\t\t\tComo su homólogo italiano (fontanero con gorra roja), Igor siente una gran pasión\n\t\t\tpor los champiñones. ¡Podrás a partir de ahora disfrazar a Igor pulsando la tecla\n\t\t\t&quot;D&quot; durante la partida!</quest><quest id="48" title="Sucesor de Tubérculo">\t\t\tParece que Igor ha... cambiado. Su mirada se ha vuelto fría. Se esconde tras\n\t\t\tuna gran capa marrón y ahora su semblante es oscuro. Parece que poco a poco\n\t\t\tse está transformando en quien fue su enemigo.\n\t\t\t¡Disfrázate de Tubérculo pulsando la tecla &quot;D&quot; durante la partida!</quest><quest id="49" title="¡La primera Llave!">\t\t\tIgor ha encontrado una Llave Maestra de Madera. Sin duda debe de abrir\n\t\t\tuna puerta en algún sitio de las cavernas...</quest><quest id="50" title="Rigor Dangerous">\t\t\t¡Has descubierto en tu aventura una vieja Llave Oxidada!\n\t\t\tContiene una pequeña inscripción: &quot;Rick&quot;. Sin duda fue su antiguo propietario...</quest><quest id="51" title="La Meluzine perdida">\t\t\tLa Meluzine, llave legendaria salida de viejos cuentos hammerfestianos,\n\t\t\tabre según dicen la puerta a grandes riquezas. Queda por saber dónde está esa puerta...</quest><quest id="52" title="¡Por fin el borracho!">\t\t\tEsta extraña pequeña llave huele a vino: ¡es la Llave Borracha!</quest><quest id="53" title="Congelación">\t\t\tComo cualquier otro hombre de nieve, Igor tiene problemas para guardar esta llave en\n\t\t\tla mano. Esta puerta da acceso a los lugares más recónditos de Hammerfest.</quest><quest id="54" title="Una llave herrumbrosa">\t\t\tEste pequeño trozo metálico amorfo no parece tener un gran valor. ¡Sin embargo\n\t\t\thay que desconfiar de las apariencias! ¿Quién puede saber qué clase de aventura\n\t\t\tse esconde más allá de la puerta que abrirá?</quest><quest id="55" title="¡Dejadlo libre!">\t\t\tTodo tu poder administrativo contará con el apoyo del formulario de Autorización\n\t\t\tMadera-Bonita BJ22a.</quest><quest id="56" title="Los Mundos Arduos">\t\t\tDebido a que has alcanzado el nivel 50 en el modo Pesadilla, ¡ganas la Llave de los Mundos\n\t\t\tArduos!</quest><quest id="57" title="¡Ráaapido!">\t\t\tSin saber muy bien por qué, la Llave Que Rasca que has encontrado te da unas ganas\n\t\t\ttremendas de correr por todos lados y rodar como un ovillo.</quest><quest id="58" title="Vacíarle los bolsillos a Tubérculo">\t\t\tTubérculo, el hechicero malvado, llevaba una llave...</quest><quest id="59" title="Tubérculo, señor de los infiernos">\t\t\t¡Todo tu poder y maestría absoluta de las técnicas de combate de Hammerfest permitieron\n\t\t\tque derrotaras al mismísimo Tubérculo en el modo Pesadilla! Gracias a ello eres el\n\t\t\tprivilegiado poseedor de la Llave Única.</quest><quest id="60" title="El agua que sabe a perfume">\t\t\tEl exceso del consumo de alcohol es perjudicial para la salud. Lo dicen las autoridades\n\t\t\tsanitarias. Sin embargo, ¡en Hammerfest te ha permitido desbloquear la Llave Borracha!\n\t\t\tA partir de ahora tienes muchas posibilidades de encontrarla en las cavernas.</quest><quest id="61" title="Papeleo">\t\t\tIgor es un maestro en el arte de rellenar formularios administrativos.\n\t\t\tPronto podrás encontrar la Autorización Madera-bonita BJ22a en las cavernas.</quest><quest id="62" title="Mejor jugador">\t\t\tEl mejor jugador de videojuegos eres tú, ¡sin ninguna duda! ¡Tu colección de\n\t\t\tmandos de consola es envidiable!\n\t\t\tA partir de ahora Igor posee desde el principio de cada partida un bonus en rapidez.</quest><quest id="63" title="Espejo, mi bonito espejo">\t\t\tEl Espejo Bancal que has encontrado en el juego te permite ahora ver las cosas desde\n\t\t\tun ángulo diferente. ¡La opción suplementaria de juego &quot;Espejo&quot; ha sido desbloqueada!</quest><quest id="64" title="Modo pesadilla">\t\t\tTienes talento. Mucho talento... pero ¿serías capaz de ayudar a Igor en el modo Pesadilla?\n\t\t\t¡Esta opción ha sido desbloqueada!</quest><quest id="65" title="¡La aventura continúa!">\t\t\tEl consejo de los Zanahorios y Panteonianos te ha elegido para explorar con más atención las Cavernas\n\t\t\tde Hammerfest. La Llave de Gordon es tan sólo la primera etapa de una nueva misión.</quest><quest id="66" title="Joya de Ankhel">\t\t\tHas hecho prueba de una habilidad y perspicacia sin igual al encontrar la Joya de Ankhel.\n\t\t\t¡La opción Control Avanzado del Balón ha sido desbloqueada en el modo Fútbolfest!</quest><quest id="67" title="¡Sandy comienza la aventura!">\t\t\t¡Todos los elementos han sido reunidos para en fin dar vida a Sandy,\n\t\t\tel hombre de arena! Este nuevo personaje podrá unirse a ti en el modo\n\t\t\tCooperativo, disponible para dos jugadores en el mismo ordenador.\n\t\t\t¡Re-descubre la gran aventura con un amigo!</quest><quest id="68" title="Espejo, NUESTRO bonito espejo">\t\t\tCon el Espejo de las Arenas, puedes ver la cosas desde nuevo ángulo...\n\t\t\t¡y con un amigo! La opción de juego &quot;Espejo&quot; ha sido desbloqueada\n\t\t\tpara el modo Cooperativo.</quest><quest id="69" title="Modo Doble Pesadilla">\t\t\t¡Está claro que eres capaz de llegar lejos cuando estás acompañado!\n\t\t\tLa opción de juego &quot;Pesadilla&quot; ha sido desbloqueada para el modo Cooperativoooooo.</quest><quest id="70" title="Una gran Amistad">\t\t\t¡Más vale tarde que nunca! Igor y Sandy han comprendido que es mejor\n\t\t\ttrabajar juntos para sobrevivir en las Cavernas. La opción de juego\n\t\t\t&quot;Compartir vidas&quot; ha sido desbloqueada para el modo Cooperativo. Si\n\t\t\testa opción es activada, cuando un jugador pierde su última vida,\n\t\t\tutiliza una del segundo jugador para poder continuar la partida.</quest><quest id="71" title="Aprendizaje de lanzamiento shurikens">\t\t\tDe tanto saltar Igor ha desarrollado una habilidad digna de un gran ninja...\n\t\t\tPara probar su talento, deberá recoger artefactos ninja dispersados en las\n\t\t\tcavernas.</quest><quest id="72" title="¡Shinobi do!">\t\t\t¡Igor ha terminado la exploración de iniciación en la vía del Ninjutsu!\n\t\t\tGracias a ello la opción de juego &quot;Ninjutsu&quot; ha sido desbloqueada para\n\t\t\tel modo Aventura.</quest><quest id="73" title="Rápido como el rayo...">\t\t\tIgor es imbatible para apuntar y lanzar con precisión...\n\t\t\t¡Ahora hay que probárselo a todo el mundo! El modo CONTRARRELOJ ha sido desbloqueado\n\t\t\ty te ofrece la posibilidad de acceder a la nueva clasificación CONTRARRELOJ.\n\t\t\t¡Ya puedes ser el muñeco de nieve más rápido de Hammerfest!</quest><quest id="74" title="Maestro de Bombas">\t\t\tEres el mayor experto en materia explosiva de la región de Hammerfest. \n\t\t\tLa opción &quot;Explosivos inestables&quot; ha sido desbloqueada para el modo Aventura.\n\t\t\tAdemás, también podrás empujar las bombas más lejos al mismo tiempo que avanzas\n\t\t\ty golpeas.</quest><quest id="75" title="Tumba de Tubérculo">\t\t\tHas descubierto una extraña pirámide de bolsillo en la tumba de Tubérculo.\t\t\t</quest></quests><dimensions><dimension id="0"><level id="0" name="Poblado de Hammerfest"/><level id="2" name="Las primeras cavernas"/><level id="10" name="La madriguera suicida"/><level id="16" name="Las profundidades malditas"/><level id="20" name="Cavernas de los Ver-tigos"/><level id="31" name="Región de la Escarcha Ácida"/><level id="41" name="Estratos Superiores de Morgenfell"/><level id="51" name="Dominio de la Bomba-muy-mala"/><level id="61" name="Subterráneo misterioso de la Ciruela"/><level id="71" name="Jungla cavernícola"/><level id="81" name="La gruta multicolor"/></dimension><dimension id="1"><level id="0" name="Templo pre-tuberculiano"/><level id="12" name="Escondite del fontanero"/><level id="14" name="BubbleFest"/><level id="24" name="Grutas del looping"/><level id="33" name="Pequeña sala bonus"/><level id="35" name="Diffiland"/><level id="41" name="Madera-Bonita de Giraflor"/><level id="55" name="Base secreta de Tuber"/><level id="60" name="Cavavín"/><level id="66" name="Cavavín: sala secreta"/><level id="68" name="Prototipo Secreto Tuberqui"/><level id="70" name="Escarcha inferior..."/><level id="76" name="¡Trampa de cristal!"/><level id="78" name="Bosque escondido de Hammerfest"/><level id="80" name="Antro de los Geluloz"/><level id="86" name="Sala del Guardián"/><level id="90" name="El segundo Pozo"/><level id="108" name="Bonus: ¡Deprisa y corriendo!"/><level id="110" name="La troglogruta"/><level id="119" name="El mundo del Espejo"/><level id="125" name="Caverna de la náusea"/><level id="127" name="El escondite de los Zanahorios"/><level id="134" name="Le Dojo"/><level id="141" name="Los infiernos"/><level id="148" name="Mezclador de Sacha"/><level id="149" name="Pozo de los Zames"/><level id="154" name="La taberna de las Frambuesas"/><level id="157" name="Metal que grita"/><level id="168" name="Cripta frambuesada"/><level id="188" name="La tomba de Tubérculo"/><level id="206" name="¡La entrada a la tumba!"/><level id="211" name="¡La verdadera entrada a la tumba!"/><level id="217" name="Escondite de los consejeros"/></dimension><dimension id="2"/><dimension id="3"><level id="0" name="Mechayame"/><level id="6" name="Irish Coffee"/><level id="12" name="Los Gatotoro"/><level id="42" name="Ayamedium"/></dimension></dimensions><keys><key id="0" name="Llave Maestra de Madera"/><key id="1" name="Llave de Rigor-Dangerous"/><key id="2" name="Meluzine"/><key id="3" name="Llave Borracha"/><key id="4" name="Furtok glacial"/><key id="5" name="Vieja Llave Oxidada"/><key id="6" name="Autorización de Madera-Bonita"/><key id="7" name="Llave de los Montes Arduos"/><key id="8" name="Llave Que Rasca"/><key id="9" name="Llave Maestra de Tubérculo"/><key id="10" name="Llave de la Pesadillas"/><key id="11" name="Llave de Inversión"/><key id="12" name="Llave de Gordon"/><key id="13" name="Llave de los Aprendices"/><key id="14" name="Pase VIP pirámide"/></keys></lang>'
    setVariable
  end // of frame 0

  frame 0
    push 'frXml'
    push '<lang id="fr" debug="0"><statics><t id="1" v="Niveau"/><t id="2" v="Apprentissage"/><t id="3" v="niveau suivant"/><t id="4" v="VITE !"/><t id="5" v="Instructions"/><t id="6" v="Cliquez pour continuer..."/><t id="7" v="Se déplacer"/><t id="8" v="Poser une bombe"/><t id="9" v="Pause"/><t id="10" v="ESPACE"/><t id="11" v="PARFAIT !"/><t id="12" v="DANGER BOSS !"/><t id="13" v="Nouvelle contrée"/><t id="14" v="Contrée: "/><t id="15" v="Flash 8 est nécessaire pour jouer à Hammerfest. Vous pourrez le télécharger gratuitement à l\'adresse suivante:"/><t id="16" v="Forêt d\'Hammerfest"/><t id="17" v="Caverne de l\'apprenti"/><t id="18" v="MINUTE"/><t id="19" v="MINUTES"/><t id="20" v="TIME ATTACK !"/><t id="21" v="Préparez-vous..."/><t id="22" v="BUT !!"/><t id="23" v="CONTRE SON CAMP!"/><t id="24" v="Engagement !"/><t id="25" v="Fin du match"/><t id="26" v="VICTOIRE: IGOR !"/><t id="27" v="VICTOIRE: SANDY !"/><t id="28" v="MATCH NUL !!"/><t id="29" v="Se déplacer"/><t id="30" v="Frapper"/><t id="31" v="Pousser en haut"/><t id="32" v="Amenez le ballon dans le but adverse. Tous les coups sont permis ! Ce mode se joue à 2 sur le même ordinateur."/><t id="33" v="Appuyez sur P pour l\'aide"/><t id="34" v="Téléportation impossible"/><t id="35" v="Bonus vies: "/><t id="36" v="Tubercourse"/><t id="37" v="Votre temps:"/><t id="38" v="Temps moyen par niveau:"/><t id="39" v="Temps:"/><t id="40" v="Clé requise:"/><t id="41" v="Utilise: "/><t id="42" v="Bombe"/><t id="43" v="est eliminé !"/><t id="44" v="Cible interdite !"/><t id="45" v="Cible éliminée."/><t id="50" v="Voyage vers Hammerfest..."/><t id="51" v="km"/><t id="52" v="Vous y êtes !"/><t id="53" v="Cliquez pour commencer..."/><t id="54" v="Démarrage de la partie..."/><t id="55" v="Sauvegarde en cours !"/><t id="56" v="Merci de patienter..."/><t id="57" v="Une partie vous sera alors décomptée."/><t id="58" v="Ce mode est gratuit et illimité !"/><t id="59" v="----[ MODE DEV LOCAL ]----"/><t id="60" v="Echec d\'envoi des données, tentative: "/><t id="61" v="Chargement des musiques..."/><t id="62" v="Tout est prêt !"/><t id="63" v="Lecture des pistes..."/><t id="64" v="Une erreur a eu lieu lors du chargement des données."/><t id="65" v="Vous devez installer Flash version 7"/><t id="66" v="Le serveur de fichiers n\'a pas été trouvé."/><t id="67" v="Erreur lors du téléchargement"/><t id="68" v="Une erreur a eu lieu lors de l\'initialisation du téléchargement"/><t id="69" v="Une erreur a eu lieu lors du téléchargement de la musique !"/><t id="70" v="Une erreur a eu lieu lors du téléchargement du jeu ! Veuillez consulter le SUPPORT TECHNIQUE en bas du site pour savoir comment Vider le cache de votre navigateur."/><t id="71" v="Sauvegarde de la partie précédente..."/><t id="72" v="Impossible de terminer la sauvegarde."/><t id="73" v="Sauvegarde réussie ! :)"/><t id="74" v="Enfin presque..."/><t id="100" v="Sautez *dans le puits* pour démarrer l&apos;aventure !"/><t id="101" v="Passage secret !"/><t id="102" v="Au fond du puits, tout au fond, résonne un rire inquiétant..."/><t id="103" v="Vous avez découvert *un passage secret* !"/><t id="104" v="Vous venez de libérer les derniers fruits de l\'emprise de Tuberculoz !"/><t id="105" v="A quelques mètres seulement, le cliquetis d\'une meute de idombinos* se fait entendre..."/><t id="106" v="Les Bombinos sont détruits ! Igor peut maintenant continuer son aventure."/><t id="107" v="D\'étranges bruits vous parviennent des étages inférieurs..."/><t id="108" v="Vous entrez dans la tanière des *Poires Melbombes*..."/><t id="109" v="Igor a vaincu les Poires Melbombes !"/><t id="110" v="La température semble avoir baissé d\'un seul coup... Igor est arrivé dans l\'antre des *Sorbex* !"/><t id="111" v="Les terribles citrons Sorbex ont été terrassés ! Igor peut poursuivre son exploration..."/><t id="112" v="Igor vient d\'arriver en plein match de Fraise-Ball. Il va falloir vaincre l\'équipe des *Tagadas* !"/><t id="113" v="L\'équipe des *Tagadas* a été battue 4-0 par Igor !"/><t id="114" v="Igor est entré dans la caverne des facétieux *Sapeurs Kiwis* !"/><t id="115" v="Vous avez vaincu les Sapeurs Kiwis !"/><t id="116" v="L\'épreuve du *Destin*"/><t id="117" v="Vous entrez un lieu secret et oublié... Le *Temple des épreuves* !"/><t id="118" v="L\'épreuve de *Dextérité*"/><t id="119" v="Vous avez *échoué* à cette épreuve"/><t id="120" v="Vous avez *remporté* cette terrible épreuve !"/><t id="121" v="L\'épreuve de *Courage*"/><t id="122" v="Vous avez ouvert un *raccourci* !"/><t id="123" v="Le sifflement des corps filant dans les airs... Pas de doute ! Vous êtes sur le territoire des idondissantes* !"/><t id="124" v="Le silence retombe tandis que la dernière Bondissante est terrassée..."/><t id="125" v="BETA-TEST: cette porte mène au niveau 51."/><t id="126" v="*L\'obscurité* envahit les cavernes, à mesure que vous descendez..."/><t id="127" v="Le sol tremble... Préparez-vous à affronter les *Ananargeddons* !"/><t id="128" v="Le calme revient alors que le dernier Ananargeddon est terrassé..."/><t id="129" v="Choisissez le terrain pour ce match. Vous pouvez *appuyer sur P* pour obtenir de l\'aide à tout moment."/><t id="130" v="Ce *terrain spécial* est dédié au VolleyFest !"/><t id="131" v="Vous avez retrouvé le *Joyau d\'Ankhel* ! Rick serait fier de vous..."/><t id="132" v="Le *Joyau d\'Ankhel* a été scellé à tout jamais. Tant d\'effort pour rien !"/><t id="133" v="Protégez le bouton rouge !"/><t id="134" v="Ramassez un maximum de cristaux !"/><t id="135" v="La porte vient de s\'ouvrir !"/><t id="136" v="Une odeur de moisissure suspecte émane des niveaux inférieurs..."/><t id="137" v="Quelque chose d\'horrible semble avoir germé de la Canne de Bobble... Vous êtes dans l\'antre des Bobbles Déchus !"/><t id="138" v="Les Bobbles déchus ont été vaincus... Mais vu la taille de cette Canne, il est fort probable qu\'il y en ait d\'autres ailleurs dans les cavernes."/><t id="139" v="*mrloxa* : Bienvenue dans le *Repaire des Carotteux*, nous t\'attendions."/><t id="140" v="*Chipseater* : Nous sommes des reflets de ta personne. Nous provenons chacun d\'une dimension parallèle où avons réussi à vaincre Tuberculoz."/><t id="141" v="*steph0808* : Une entité supérieure nous a envoyé ici afin de t\'aider dans ta quête et te montrer la voie."/><t id="142" v="*blaguenul* : Poil au doigt !"/><t id="143" v="*GlobZOsiris* : Voici la *Clé de Gordon* qui te permettra d\'ouvrir l\'accès à notre cave."/><t id="144" v="*rollerfou* : Nous avons caché au fond de cette cave une *Canne de Bobble*, un objet de légende et de grande valeur !"/><t id="145" v="*GlobZOsiris* : Mais c\'était il y a bien longtemps... Avant la Grande Mise à Jour..."/><t id="146" v="*Tout le monde* : ooooooooh.... la Grande Mise à Jour ! "/><t id="147" v="*mrloxa* : SILENCE !"/><t id="148" v="*mrloxa* : En tant que Premier Panthéonien, je te confie la lourde tâche de *retrouver cette Canne de Bobble*. Va, maintenant."/><t id="149" v="*mrloxa* : La Canne de Bobble a été *corrompue* !"/><t id="150" v="*Tout le monde* : ooooooooh...."/><t id="151" v="*blaguenul* : Corrompue ! Poil au ..."/><t id="152" v="*GlobZOsiris* : Il y a fort à parier que cette corruption sera à l\'origine de grands changements !"/><t id="153" v="*steph0808* : Tu dois retourner explorer les cavernes, Igor. Nul doute que *d\'autres lieux secrets* comme celui-ci existent..."/><t id="154" v="*mrloxa* : Oui c\'est évident ! Tu dois être attentif, Igor. Fouille chaque recoin de ce monde..."/><t id="155" v="*mrloxa* : Nous ne l\'avions jamais remarqué... Mais de nombreux secrets se cachent encore dans *Les Cavernes de Hammerfest* !!"/><t id="156" v="*rollerfou* : ... Comme le coup de la bombe dans le Puits ?"/><t id="157" v="*Tout le monde* : ooooooooooooooooh...."/><t id="158" v="Vous êtes en mode *Ninjutsu* ! Visez en priorité tout ennemi marqué d\'un *CRANE*. Ne tuez jamais un ennemi portant un *COEUR* !"/><t id="159" v="Le bruit visqueux que l\'on entend dans ces cavernes ne laisse aucun doute: vous êtes dans *la tanière des Framboijules* !"/><t id="160" v="Vous avez vaincu les premières *Framboijules* ! Mais il y a fort à parier que ce ne seront pas les dernières..."/><t id="161" v="Attention ! Cette région est extrêmement dangereuse et difficile ! Ne vous y aventurez surtout pas !"/><t id="162" v="Seuls les plus téméraires passeront !"/><t id="163" v="Un voix féminine douce et calme annonce : *\'Accès interdit aux ninjas\'*."/><t id="200" v="Utilisez les *flèches du clavier* pour vous déplacer à gauche et à droite."/><t id="201" v="Pour franchir ce mur, sautez en appuyant sur la touche *flèche du haut* !"/><t id="202" v="Laissez vous tomber dans le vide pour passer au niveau suivant."/><t id="203" v="Utilisez la *barre espace* pour poser une bombe près de lui. Ces bombes *gèlent les ennemis*, mais n\'ont *aucun effet sur vous*."/><t id="204" v="Un ennemi vaincu vous donne un *bonus*. Ne tardez pas à le ramasser, il ne durera pas éternellement !"/><t id="205" v="Parfois il faudra vous y reprendre à *plusieurs fois* avant de réussir à pousser un ennemi dans le vide. Posez une bombe pres d\'un monstre gelé pour le pousser encore."/><t id="206" v="N\'oubliez pas votre bonus avant de quitter le niveau"/><t id="207" v="Vous pouvez *pousser vos bombes* ! Appuyez a nouveau sur Espace une fois la bombe posée."/><t id="208" v="Posez une bombe ici, et *appuyez sur Espace à nouveau, sitôt la bombe posée* !"/><t id="209" v="Bien ! Faites de même pour le suivant !"/><t id="210" v="Cette technique est très importante ! Apprenez à la maîtriser."/><t id="211" v="Il existe d\'autres types de bombes à poser. Passez dans les *champs d\'énergie* pour changer d\'arme."/><t id="212" v="Les *bombes vertes* explosent comme des *mines* au contact d\'un ennemi !"/><t id="213" v="Posez une *mine verte* ici !"/><t id="214" v="Notez que si vous tapez dans une mine verte, elle explosera *dès qu\'elle touchera le sol* !"/><t id="215" v="Vous pourrez changer d\'arme à nouveau en franchissant d\'autres champs d\'énergie."/><t id="216" v="Vous noterez que *votre écharpe change de couleur* selon votre type de bombe !"/><t id="217" v="Les bombes *bleues* permettent d\'assomer un ennemi et de *le faire tomber d\'un étage*."/><t id="218" v="Notez que les bombes bleues explosent plus rapidement."/><t id="219" v="Certains objets aux effets surprenants viendront parfois vous aider au fil de votre exploration !"/><t id="220" v="Certains auront des effets positifs, d\'autres non. A vous de tous les découvrir !"/><t id="221" v="Accumulez les lettres du mot *CRISTAL* qui apparaitront au fil des niveaux."/><t id="222" v="Tous les ennemis seront alors détruits, vous libérant l\'accès au niveau suivant !"/><t id="223" v="Utilisez les bombes *rouges* pour vous propulser vers des plate-formes trop élevées. *Posez une bombe rouge ici, puis ne bougez plus* !"/><t id="224" v="Dites donc, qu\'est-ce que vous faites ici ?"/><t id="225" v="Ah mais non ! Pas par là !"/><t id="226" v="Bon ca suffit maintenant..."/><t id="227" v="Vous pouvez monter plus haut *en jetant vos bombes en plein saut* ! Cela vous donnera une petite poussée supplémentaire si vous le faites au bon moment."/><t id="228" v="Bravo !"/><t id="229" v="Cette même technique vous permet aussi de *sauter plus loin*. Jetez une bombe pendant votre saut."/><t id="230" v="Lancez votre bombe *pendant la descente* et non pendant la montée dans votre saut."/><t id="231" v="Bien !"/><t id="232" v="Voyons si vous arrivez à atteindre la sortie dans ce niveau !"/><t id="233" v="Votre apprentissage est terminé ! Passons aux travaux appliqués..."/><t id="234" v="*Secret*: il existe une porte secrète dans le *premier niveau* de cet apprentissage ! Posez *une bombe tout à droite, au premier niveau* !"/><t id="235" v="Vous pouvez sauter au travers d\'un plafond, pour *monter d\'un étage*."/><t id="236" v="Pour sauter par dessus ce trou, appuyez sur la *flèche haut*, puis *maintenez la flèche de gauche* enfoncée."/><t id="237" v="Presque ! *Remontez par la droite*, puis essayez à nouveau."/><t id="238" v="Voici un *ennemi* !"/><t id="239" v="Vous ne pouvez pas quitter un niveau si tous les ennemis ne sont pas vaincus !"/><t id="240" v="Ne vous inquiétez pas si vous mourrez, contrairement aux autres modes de jeu, vous disposez de *vies illimitées* pendant l\'apprentissage !"/><t id="241" v="Bien ! Re-descendez *par la droite*..."/><t id="242" v="Si vous n\'y parvenez pas, vous pouvez continuer par le passage à gauche. *Sautez bien à l\'instant précis où la bombe rouge explose* !"/><t id="243" v="Non, pas ici :) Dans le *premier niveau de cet apprentissage* ! Pour l\'instant, vous pouvez simplement continuer."/><t id="244" v="Cette porte permet d\'écourter l\'apprentissage. Pour continuer, suivez la flèche !"/><t id="245" v="N\'allez pas plus loin ! C\'est le bout du monde !"/><t id="246" v="Ce seau là ne tombera pas."/><t id="247" v="Vous n\'irez pas plus loin !"/><t id="248" v="Les *bombes rouges* peuvent également sortir un ennemi d\'une situation bloquée."/><t id="400" v="Voici un petit avant-goût de ce que vous rencontrerez dans les Cavernes d\'Hammerfest... *Sautez dans le vide pour continuer* !"/><t id="401" v="Vous voila paré à débuter la grande aventure !"/><t id="402" v="Si ca n\'est pas déjà fait, il ne vous reste plus qu\'à *vous inscrire* et à guider Igor au fin fond des Cavernes de Hammerfest..."/><t id="403" v="Hammerfest, c\'est plus de *320 NIVEAUX* à explorer, plus de *210 OBJETS* à collectionner et *18 TYPES D\'ENNEMIS* uniques !"/><t id="404" v="Vous pouvez voir en bas de l\'écran vos *vies restantes*, le *numéro du niveau* et *vos points*."/><t id="405" v="Une fois inscrit, vous pourrez par exemple améliorer votre nombre de vies grâce aux *quêtes* !"/><t id="406" v="Une quête consiste à ramasser un certain nombre d\'objets impartis en jeu."/><t id="407" v="A chaque partie, votre score sera enregistré et vous participerez aux *classements des joueurs* !"/><t id="408" v="Allez, ne soyez pas timide, sautez !"/><t id="500" v="Utilisez la *barre espace* pour poser une bombe près du fruit. Ces bombes *gèlent les ennemis*, mais n\'ont *aucun effet sur vous*."/><t id="501" v="Faites attention aux *pièges* !!"/><t id="502" v="Bravo ! Vous avez réussi à terminer cette série de niveaux ! Hammerfest vous attend, avec ses *centaines de niveaux uniques* et ses *milliers de surprises* !"/><t id="503" v="Essayez donc d\'attraper ce *joyau*... Attention, les *scies ne peuvent pas être détruites* !"/><t id="300" v="Conseil: "/><t id="301" v="Vous pouvez poser une bombe pendant que vous tombez pour profiter d\'une petite impulsion de saut supplémentaire."/><t id="302" v="Appuyez sur la touche M pour couper temporairement la musique en cours de partie."/><t id="303" v="Un monstre gelé peut en tuer d\'autres s\'il les percute à grande vitesse !"/><t id="304" v="Si vous tuez plusieurs monstres avec une seule bombe, vous recevrez des bonus plus importants."/><t id="305" v="Si vous atteignez 100.000 points, vous recevrez une vie supplémentaire."/><t id="306" v="Certaines lettres du mot CRISTAL sont plus rares que d\'autres (le \'L\' ou le \'S\', par exemple)"/><t id="307" v="Dans chaque niveau, vous recevrez un objet à points, un objet spécial et quelques lettres."/><t id="308" v="L\'immense cristal à la fin de certains niveaux ne tombe pas par hasard..."/><t id="309" v="Former le mot CRISTAL, c\'est déjà bien, mais on peut peut être faire mieux..."/><t id="310" v="Appuyez à nouveau sur Espace alors qu\'une bombe est posée devant vous, vous la pousserez !"/><t id="311" v="Appuyez sur CONTROL + SHIFT + K pour abandonner la partie (elle sera quand même enregistrée)."/><t id="312" v="Maintenez la touche F enfoncée pour afficher la vitesse du jeu (la valeur idéale est de 30)."/><t id="313" v="En appuyant sur C, vous pouvez voir la Carte du Monde !"/></statics><families><family id="0" name="Objets de base"/><family id="1" name="Kit de premiers secours"/><family id="2" name="Destruction massive"/><family id="3" name="Champignons"/><family id="4" name="Cartes à jouer"/><family id="5" name="Signes du zodiaque"/><family id="6" name="Potions du zodiaque"/><family id="7" name="Coeur 1"/><family id="8" name="Coeur 2"/><family id="9" name="Coeur 3"/><family id="10" name="Trésor des pirates"/><family id="11" name="Gadgets Motion-Twin"/><family id="12" name="Armement norvégien expérimental"/><family id="13" name="Eclairage antique"/><family id="14" name="Igor-Newton"/><family id="15" name="Flocon 1"/><family id="16" name="Flocon 2"/><family id="17" name="Flocon 3"/><family id="18" name="Kit d\'appel main libre"/><family id="19" name="Mario party"/><family id="1000" name="Petites gâteries"/><family id="1001" name="Aliments classiques"/><family id="1002" name="Friandises"/><family id="1003" name="Pierres précieuses"/><family id="1004" name="Junk food"/><family id="1005" name="Exotic !"/><family id="1006" name="Apéritifs"/><family id="1007" name="Kapital Risk"/><family id="1008" name="Trophées de Grand Prédateur"/><family id="1009" name="Délices de Harry \'le beau\'"/><family id="1010" name="Chocolats"/><family id="1011" name="Fromages"/><family id="1012" name="Conserves"/><family id="1013" name="Légumes"/><family id="1014" name="Délices MT"/><family id="1015" name="Kit de survie MT"/><family id="1016" name="L\'heure du goûter"/><family id="1017" name="Garçon patissier"/><family id="1018" name="Les lois de la robotique"/><family id="1019" name="Trophées de baroudeur"/><family id="1020" name="Clés des Glaces"/><family id="1021" name="Clé avinée"/><family id="1022" name="Paperasse"/><family id="1023" name="Clés perdues"/><family id="1024" name="Batons de joie"/><family id="1025" name="Boissons rigolotes"/><family id="1026" name="Matériel administratif d\'El Papah"/><family id="1027" name="Artefacts mythiques"/><family id="1028" name="Sandy en kit"/><family id="1029" name="Récompenses du Ninjutsu"/><family id="1030" name="Artefacts du Ninjutsu"/></families><items><item id="0" name="Alphabet Cristallin"/><item id="1" name="Bouclidur en or"/><item id="2" name="Bouclidur argenté"/><item id="3" name="Ballon de banquise"/><item id="4" name="Lampe Fétvoveu"/><item id="5" name="Lampe Léveussonfé"/><item id="6" name="Paix intérieure"/><item id="7" name="Basket IcePump"/><item id="8" name="Etoile des neiges"/><item id="9" name="Mauvais-oeil"/><item id="10" name="Téléphone-phone-phone"/><item id="11" name="Parapluie rouge"/><item id="12" name="Parapluie bleu"/><item id="13" name="Cass-Tet"/><item id="14" name="Délice hallucinogène bleu"/><item id="15" name="Champignon rigolo rouge"/><item id="16" name="Figonassée grimpante"/><item id="17" name="Petit Waoulalu des bois"/><item id="18" name="Pissenlit tropical"/><item id="19" name="Tournelune"/><item id="20" name="Coffre d\'Anarchipel"/><item id="21" name="Enceinte Bessel-Son"/><item id="22" name="Vieille chaussure trouée"/><item id="23" name="Boule cristalline"/><item id="24" name="Hippo-flocon"/><item id="25" name="Flamme froide"/><item id="26" name="Ampoule 30 watts"/><item id="27" name="Porte-grenouilles"/><item id="28" name="Bibelot en argent"/><item id="29" name="Bague \'Thermostat 8\'"/><item id="30" name="Lunettes tournantes bleues"/><item id="31" name="Lunettes renversantes rouges"/><item id="32" name="As de pique"/><item id="33" name="As de trêfle"/><item id="34" name="As de carreau"/><item id="35" name="As de coeur"/><item id="36" name="Ig\'or"/><item id="37" name="Collier rafraîchissant"/><item id="38" name="Totem des dinoz"/><item id="39" name="Tête de granit lestée de plomb"/><item id="40" name="Sagittaire"/><item id="41" name="Capricorne"/><item id="42" name="Lion"/><item id="43" name="Taureau"/><item id="44" name="Balance"/><item id="45" name="Bélier"/><item id="46" name="Scorpion"/><item id="47" name="Cancer"/><item id="48" name="Verseau"/><item id="49" name="Gémeaux"/><item id="50" name="Poisson"/><item id="51" name="Vierge"/><item id="52" name="Elixir du Sagittaire"/><item id="53" name="Elixir du Capricorne"/><item id="54" name="Elixir du Lion"/><item id="55" name="Elixir du Taureau"/><item id="56" name="Elixir de la Balance"/><item id="57" name="Elixir du Bélier"/><item id="58" name="Elixir du Scorpion"/><item id="59" name="Elixir du Cancer"/><item id="60" name="Elixir du Verseau"/><item id="61" name="Elixir des Gémeaux"/><item id="62" name="Elixir du Poisson"/><item id="63" name="Elixir de la Vierge"/><item id="64" name="Arc-en-miel"/><item id="65" name="Bouée canard"/><item id="66" name="Branche de Kipik"/><item id="67" name="Anneau de Guillaume Tell"/><item id="68" name="Bougie"/><item id="69" name="Koulraoule des îles"/><item id="70" name="Trêfle commun"/><item id="71" name="Chaud devant !"/><item id="72" name="Chapeau de Mage-Gris"/><item id="73" name="Feuille de sinopée"/><item id="74" name="Esprit de l\'orange"/><item id="75" name="Esprit de la pluie"/><item id="76" name="Esprit des arbres"/><item id="77" name="Lucidjané à crête bleue"/><item id="78" name="Filandreux rougeoyant"/><item id="79" name="Poisson empereur"/><item id="80" name="Escargot Poussépa"/><item id="81" name="Perle nacrée des murlocs"/><item id="82" name="Jugement avant-dernier"/><item id="83" name="Jugement dernier"/><item id="84" name="Talisman scorpide"/><item id="85" name="Baton tonnerre"/><item id="86" name="Surprise de paille"/><item id="87" name="Larve d\'oenopterius"/><item id="88" name="Pokuté"/><item id="89" name="Oeuf de Tzongre"/><item id="90" name="Fulguro pieds-en-mousse"/><item id="91" name="Couvre-chef de Luffy"/><item id="92" name="Chapeau violin"/><item id="93" name="Boit\'a\'messages"/><item id="94" name="Anneau Antok"/><item id="95" name="Cagnotte de Tuberculoz"/><item id="96" name="Perle flamboyante"/><item id="97" name="Perle vertillante"/><item id="98" name="Grosse pé-perle "/><item id="99" name="Poils de Chourou"/><item id="100" name="Pocket-Guu"/><item id="101" name="Colis surprise"/><item id="102" name="La Carotte d\'Igor"/><item id="103" name="Vie palpitante"/><item id="104" name="Vie aventureuse"/><item id="105" name="Vie épique"/><item id="106" name="Livre des champignons"/><item id="107" name="Livre des étoiles"/><item id="108" name="Parapluie Frutiparc"/><item id="109" name="Flocon simple"/><item id="110" name="Flocon bizarre"/><item id="111" name="Flocon ENORME !"/><item id="112" name="Pioupiou carnivore"/><item id="113" name="Cape de Tuberculoz"/><item id="114" name="Mode Mario"/><item id="115" name="Casque de Volleyfest"/><item id="116" name="Joyau d\'Ankhel"/><item id="117" name="Clé de Gordon"/><item id="1000" name="Cristaux d\'Hammerfest"/><item id="1001" name="Pain à la viande"/><item id="1002" name="Canne de Bobble"/><item id="1003" name="Bonbon Berlinmauve"/><item id="1004" name="Bonbon Chamagros"/><item id="1005" name="Bonbon rosamelle-praline"/><item id="1006" name="Sucette aux fruits bleus"/><item id="1007" name="Sucette chlorophylle"/><item id="1008" name="Diamant Oune-difaïned"/><item id="1009" name="Oeil de tigre"/><item id="1010" name="Jade de 12kg"/><item id="1011" name="Reflet-de-lune"/><item id="1012" name="Surprise de Cerises"/><item id="1013" name="Muffin aux cailloux"/><item id="1014" name="Gelée des bois"/><item id="1015" name="Suprême aux framboises"/><item id="1016" name="Petit pétillant"/><item id="1017" name="Pierres du Changement"/><item id="1018" name="Glace Fraise"/><item id="1019" name="Coupe Glacée"/><item id="1020" name="Noodles"/><item id="1021" name="Poulet Surgelé"/><item id="1022" name="Liquide bizarre"/><item id="1023" name="Gros acidulé"/><item id="1024" name="Liquide étrange"/><item id="1025" name="Oeuf cru"/><item id="1026" name="Gland gnan-gnan"/><item id="1027" name="Réglisse rouge"/><item id="1028" name="Oeuf Au Plat"/><item id="1029" name="Saucisse piquée"/><item id="1030" name="Cerise-apéro confite"/><item id="1031" name="Fromage piqué"/><item id="1032" name="Olive pas mûre"/><item id="1033" name="Olive noire"/><item id="1034" name="Oeil de pomme"/><item id="1035" name="Blob intrusif"/><item id="1036" name="Gouda mou"/><item id="1037" name="Poulpi empalé"/><item id="1038" name="Olive oubliée"/><item id="1039" name="Monsieur radis"/><item id="1040" name="Sushi thon"/><item id="1041" name="Sucette acidulée"/><item id="1042" name="Délice d\'Aralé"/><item id="1043" name="Sorbet au plastique"/><item id="1044" name="Manda"/><item id="1045" name="Pétale mystérieuse"/><item id="1046" name="Gump"/><item id="1047" name="Bleuet"/><item id="1048" name="Rougeoyant"/><item id="1049" name="Verdifiant"/><item id="1050" name="KassDent"/><item id="1051" name="Pièce d\'or secrète"/><item id="1052" name="Sou d\'argent"/><item id="1053" name="Sou d\'or"/><item id="1054" name="Gros tas de sous"/><item id="1055" name="Boisson kipik"/><item id="1056" name="Doigts-de-tuberculoz"/><item id="1057" name="Pizza de Donatello"/><item id="1058" name="Canelé du sud-ouest"/><item id="1059" name="Eclair noisette choco caramel et sucre"/><item id="1060" name="Bleuette rouge"/><item id="1061" name="Perroquet décapité en sauce"/><item id="1062" name="Morvo-morphe"/><item id="1063" name="Fraise Tagada"/><item id="1064" name="Car-En-Sac"/><item id="1065" name="Dragibus"/><item id="1066" name="Krokodile"/><item id="1067" name="Demi Cocobats"/><item id="1068" name="Happy Cola"/><item id="1069" name="Cacahuete secrète"/><item id="1070" name="P\'tit fantome"/><item id="1071" name="Cookie deshydraté"/><item id="1072" name="Arbuche de noël"/><item id="1073" name="Piment farceur"/><item id="1074" name="Soja Max IceCream"/><item id="1075" name="Bouquet de steack"/><item id="1076" name="Daruma-Pastèque"/><item id="1077" name="Graine de tournesol"/><item id="1078" name="Croissant confit"/><item id="1079" name="Haricot paresseux"/><item id="1080" name="Lapin-choco"/><item id="1081" name="Biloo"/><item id="1082" name="Graine de pechume en gelée"/><item id="1083" name="Sombrino aux amandes"/><item id="1084" name="Emi-Praline"/><item id="1085" name="Frogmaliet aux pepites de chocolat"/><item id="1086" name="Yumi au café"/><item id="1087" name="Bouchée mielleuse nappée au gel de Vodka"/><item id="1088" name="Escargot au chocolat persillé"/><item id="1089" name="Fossile de cacao marbré au fois gras"/><item id="1090" name="Cerisot mariné a la bière"/><item id="1091" name="Jambon de Bayonne"/><item id="1092" name="Saucisson entamé"/><item id="1093" name="Raide red reste"/><item id="1094" name="Torchon madrangeais au sirop d\'érable"/><item id="1095" name="Saucisson de marcassin sauvage"/><item id="1096" name="Tranches de Jaret de Kangourou"/><item id="1097" name="Saucissaille de St-Morgelet"/><item id="1098" name="Paté d\'ongles au truffes"/><item id="1099" name="Saucisson maudit scellé"/><item id="1100" name="Manquereau-sauce-au-citron"/><item id="1101" name="Mini-saucisses en boite"/><item id="1102" name="Haricots blanc"/><item id="1103" name="Lychees premier prix"/><item id="1104" name="Zion\'s Calamar"/><item id="1105" name="Aubergines au sirop"/><item id="1106" name="Camembert"/><item id="1107" name="Emmental"/><item id="1108" name="Fromage verni"/><item id="1109" name="Roquefort"/><item id="1110" name="Fromage frais vigné"/><item id="1111" name="Pâte dessert fromagée"/><item id="1112" name="Saladou"/><item id="1113" name="Poire d\'eau"/><item id="1114" name="Cacahuète mauve et juteuse"/><item id="1115" name="Pommes de pierre"/><item id="1116" name="Patates douces"/><item id="1117" name="Matraque bio"/><item id="1118" name="Tomate pacifique"/><item id="1119" name="Radix"/><item id="1120" name="Haricots verts"/><item id="1121" name="Pomme Sapik"/><item id="1122" name="Pomme Sapu"/><item id="1123" name="Echalottes"/><item id="1124" name="Sel rieur"/><item id="1125" name="Poivron vert"/><item id="1126" name="Poivron jaune"/><item id="1127" name="Poivron rouge"/><item id="1128" name="Brocolis digérés"/><item id="1129" name="Grappe de Radix"/><item id="1130" name="Lance-poix"/><item id="1131" name="Haricots blancs"/><item id="1132" name="Poires d\'eau en grappe"/><item id="1133" name="Artifroid"/><item id="1134" name="Choux-flou"/><item id="1135" name="Chourou"/><item id="1136" name="Pom pom pom.."/><item id="1137" name="Hollandais"/><item id="1138" name="Fondant XXL au choco-beurre"/><item id="1139" name="Pétillante"/><item id="1140" name="Warpoquiche"/><item id="1141" name="Brioche dorée"/><item id="1142" name="Café de fin de projet"/><item id="1143" name="Laxatif aux amandes"/><item id="1144" name="Smiley croquant"/><item id="1145" name="Barquette de lave"/><item id="1146" name="Nonoix"/><item id="1147" name="Amande croquante"/><item id="1148" name="Noisette"/><item id="1149" name="Noodles crus"/><item id="1150" name="Brioche vapeur"/><item id="1151" name="Tartine chocolat-noisette"/><item id="1152" name="Tartine hémoglobine"/><item id="1153" name="Tartine à l\'orange collante"/><item id="1154" name="Tartine au miel"/><item id="1155" name="Lombric nature"/><item id="1156" name="Grenade de chantilly"/><item id="1157" name="Profies très drôles"/><item id="1158" name="Chouchocos"/><item id="1159" name="Blob périmé"/><item id="1160" name="Bonbon Hélène-fraiche"/><item id="1161" name="Nem aux anchois"/><item id="1162" name="Surimi pamplemousse"/><item id="1163" name="Poulpi à l\'encre"/><item id="1164" name="Curly"/><item id="1165" name="Tartelette framboise"/><item id="1166" name="Oeuf de Poire"/><item id="1167" name="Truffe collante sans goût"/><item id="1168" name="Sardines"/><item id="1169" name="Etoile Toulaho"/><item id="1170" name="Transformer en sucre"/><item id="1171" name="Kitchissime"/><item id="1172" name="Igorocop"/><item id="1173" name="Tidouli didi"/><item id="1174" name="Dalek ! Exterminate !"/><item id="1175" name="Robo-malin"/><item id="1176" name="Johnny 6"/><item id="1177" name="Biscuit Transformer"/><item id="1178" name="Statue: Citron Sorbex"/><item id="1179" name="Statue: Bombino"/><item id="1180" name="Statue: Poire Melbombe"/><item id="1181" name="Statue: Tagada"/><item id="1182" name="Statue: Sapeur-kiwi"/><item id="1183" name="Statue: Bondissante"/><item id="1184" name="Statue: Ananargeddon"/><item id="1185" name="Anneau hérissé"/><item id="1190" name="Passe-partout en bois"/><item id="1191" name="Clé de Rigor Dangerous"/><item id="1192" name="Méluzzine"/><item id="1193" name="Clé du Bourru"/><item id="1194" name="Furtok Glaciale"/><item id="1195" name="Vieille clé rouillée"/><item id="1196" name="Autorisation du Bois-Joli"/><item id="1197" name="Clé des Mondes Ardus"/><item id="1198" name="Clé piquante"/><item id="1199" name="Passe-partout de Tuberculoz"/><item id="1200" name="Clé des cauchemars"/><item id="1201" name="Pad Sounie"/><item id="1202" name="Pad Frusion 64"/><item id="1203" name="Pad Game-Pyramid"/><item id="1204" name="Pad Sey-Ga"/><item id="1205" name="Pad Super Frusion"/><item id="1206" name="Pad du Système Maitre"/><item id="1207" name="Pad Frusion Entertainment System"/><item id="1208" name="Manette S-Téhéf"/><item id="1209" name="Canette Express"/><item id="1210" name="Bouteille aux 2064 bulles"/><item id="1211" name="Mousse volante"/><item id="1212" name="Vin Merveilleux"/><item id="1213" name="Liqueur maléfique"/><item id="1214" name="Tampon MT"/><item id="1215" name="Facture gratuite"/><item id="1216" name="Post-It de François"/><item id="1217" name="Pot à crayon solitaire"/><item id="1218" name="Agrafeuse du Chaos"/><item id="1219" name="Miroir bancal"/><item id="1220" name="Etoile du Diable"/><item id="1221" name="Poudre de plage magique"/><item id="1222" name="Matériel d\'architecte"/><item id="1223" name="Maquette en sable"/><item id="1224" name="Winkel"/><item id="1225" name="Miroir des Sables"/><item id="1226" name="Etoile des Diables Jumeaux"/><item id="1227" name="Sceau d\'amitié"/><item id="1228" name="Insigne de l\'ordre des Ninjas"/><item id="1229" name="Couteau suisse japonais"/><item id="1230" name="Shuriken de second rang"/><item id="1231" name="Shuriken d\'entraînement"/><item id="1232" name="Najinata"/><item id="1233" name="Lance-boulettes de Précision"/><item id="1234" name="Ocarina chantant"/><item id="1235" name="Armure de la nuit"/><item id="1236" name="Insigne du Mérite"/><item id="1237" name="Neige-o-glycérine"/><item id="1238" name="Pass-Pyramide"/></items><quests><quest id="0" title="Les constellations">\t\t\tIgor peut maintenant poser 2 bombes au lieu d&apos;une seule à la fois, et ce,\n\t\t\tde manière permanente !</quest><quest id="1" title="Mixtures du zodiaque">\t\t\tIgor sait désormais envoyer ses bombes à l&apos;étage supérieur ! Maintenez BAS enfoncé\n\t\t\tpendant que vous tapez dans une bombe posée.</quest><quest id="2" title="Premiers pas">\t\t\tIgor débutera désormais la partie avec une vie supplémentaire.</quest><quest id="3" title="L\'aventure commence">\t\t\tIgor débutera désormais la partie avec une autre vie supplémentaire.</quest><quest id="4" title="Une destinée épique">\t\t\tIgor débutera désormais la partie avec encore une vie supplémentaire !</quest><quest id="5" title="Persévérance">\t\t\tIgor débutera désormais la partie avec une vie supplémentaire.</quest><quest id="6" title="Gourmandise">\t\t\tDe nouveaux aliments (plus sains) apparaîtront désormais en jeu !</quest><quest id="7" title="Du sucre !">\t\t\tIgor veut encore plus de sucre ! Des délicieuses friandises apparaîtront\n\t\t\tdésormais en jeu.</quest><quest id="8" title="Malnutrition">\t\t\tLe goût d&apos;Igor pour les aliments peu équilibrés lui permettra de trouver\n\t\t\tdes aliments encore plus... &quot;douteux&quot; en jeu.</quest><quest id="9" title="Goût raffiné">\t\t\tLe régime bizarre d&apos;Igor a amené de nouveaux aliments exotiques en jeu !</quest><quest id="10" title="Avancée technologique">\t\t\tPour vous aider dans votre aventure, tout un tas d&apos;objets aux effets bizarres\n\t\t\tfont leur apparition en jeu !</quest><quest id="11" title="Le petit guide des Champignons">\t\t\tIgor a découvert un étrange ouvrage, une sorte de livre de cuisine traitant\n\t\t\tdes champignons hallucinogènes. Il pourra désormais en trouver lors de ses\n\t\t\texplorations.</quest><quest id="12" title="Trouver les pièces d\'or secrètes !">\t\t\tDes richesses supplémentaires de très grande valeur apparaitront maintenant\n\t\t\ten jeu !</quest><quest id="13" title="Le grimoire des Etoiles">\t\t\tCet ouvrage mystérieux en dit long sur les 12 constellations du Zodiaque.\n\t\t\tVous pourrez désormais les collectionner en jeu !</quest><quest id="14" title="Armageddon">\t\t\tDes objets aux effets ravageurs vont maintenant apparaître en jeu !</quest><quest id="15" title="Régime MotionTwin">\t\t\tA force de manger n&apos;importe quoi, Igor a acquis la maîtrise des jeux ! Il\n\t\t\tpourra collectionner des objets rarissimes pendant son exploration et\n\t\t\ttrouver\tdes aliments d&apos;exception !</quest><quest id="16" title="Créateur de jeu en devenir">\t\t\tIgor semble apprécier les aliments basiques, une belle carrière dans le monde\n\t\t\tdu jeu vidéo pourrait s&apos;offrir à lui ! Des aliments adaptés lui seront\n\t\t\tdorénavant proposés et quelques cartes à jouer pour se faire la main.</quest><quest id="17" title="La vie est une boîte de chocolats">\t\t\tDe délicieux chocolats sont maintenant distribués en Hammerfest !</quest><quest id="18" title="Le trésor Oune-difaïned">\t\t\tA force de ramasser des diamants apparus on ne sait comment, Igor a acquis\n\t\t\tla faculté de repérer des pierres précieuses rares ! Il en trouvera au cours\n\t\t\tde ses explorations.</quest><quest id="19" title="Super size me !">\t\t\tToujours plus loin dans la malnutrition, Igor a découvert qu&apos;il pouvait aussi\n\t\t\tse nourrir de produits en boite.</quest><quest id="20" title="Maître joaillier">\t\t\tIgor est devenu un véritable expert en pierres précieuses. Il pourra maintenant\n\t\t\tdécouvrir de puissants bijoux magiques au fil de ses explorations !</quest><quest id="21" title="Grand prédateur">\t\t\tIgor en a plus qu&apos;assez de chasser des choses sucrées ! Désormais devenu un prédateur\n\t\t\tsans pitié, il pourra traquer et dévorer toute sorte de charcuteries.</quest><quest id="22" title="Expert en salades et potages">\t\t\tOn raconte partout qu&apos;Igor serait la réincarnation de Saladou, le maître mondialement\n\t\t\treconnu de la salade. Fort de ce don, il pourra à l&apos;avenir cueillir une très grande variété\n\t\t\tde légumes !</quest><quest id="23" title="Festin d\'Hammerfest">\t\t\tAvec un repas aussi complet, Igor est fin prêt pour avoir accès aux patisseries les\n\t\t\tplus raffinées qui existent.</quest><quest id="24" title="Goûter d\'anniversaire">\t\t\tIgor a trouvé tous les éléments pour assurer à son prochain goûter d&apos;anniversaire !\n\t\t\tIl trouvera maintenant pleins de petits en-cas pour patienter jusque là.</quest><quest id="25" title="Bon vivant">\t\t\tUn bon repas ne peu se concevoir sans des petits trucs à grignotter à l&apos;apéritif.\n\t\t\tIgor le sait, maintenant, et il pourra trouver ce qu&apos;il faut en jeu.</quest><quest id="26" title="Fondue norvégienne">\t\t\tLes odeurs qui émanent de la tanière d&apos;Igor ne laissent aucun doute là-dessus:\n\t\t\til est devenu un grand amateur de fromages. De nouveaux produits laitiers\n\t\t\tapparaîtront dans les cavernes.</quest><quest id="27" title="Mystère de Guu">\t\t\tCette quête n&apos;a aucun intérêt, à part vous conseiller de découvrir au plus vite\n\t\t\tl&apos;excellent dessin animé &quot;Haré + Guu&quot; disponible en DVD dans toutes les bonnes\n\t\t\tboutiques ! Banyaaaaaïï. ^^</quest><quest id="28" title="Friandises divines">\t\t\tLes sucreries n&apos;ont plus aucun secret pour Igor. Il saura, à compter de ce jour,\n\t\t\tdébusquer les délices légendaires de Harry &quot;le beau&quot; disséminés à travers tout\n\t\t\tHammerfest.</quest><quest id="29" title="Igor et Cortex">\t\t\tIgor a entrepris la fabrication de gadgets mystérieux... Attendez-vous à\n\t\t\tcollectionner des machines étranges en jeu !</quest><quest id="30" title="Affronter l\'obscurité">\t\t\tIgor sait maintenant s&apos;éclairer ! Il pensera maintenant à apporter avec lui une\n\t\t\ttorche pour ne pas trop se perdre dans l&apos;obscurité des niveaux avancés !</quest><quest id="31" title="Et la lumière fût !">\t\t\tPréparé à tous les dangers, le courageux Igor ne craint plus du tout\n\t\t\tl&apos;obscurité dans les niveaux avancés !</quest><quest id="32" title="Noël sur Hammerfest !">\t\t\tVous avez gagné 5 parties supplémentaires.</quest><quest id="33" title="Joyeux anniversaire Igor">\t\t\tVous avez gagné 10 parties supplémentaires !</quest><quest id="34" title="Cadeau céleste">\t\t\tVous avez gagné 20 parties supplémentaires !</quest><quest id="35" title="Achat de parties amélioré">\t\t\tVos appels dans la boutique vous rapporteront dorénavant plus de flocons !</quest><quest id="36" title="Exterminateur de Sorbex">\t\t\tVous avez pressé sans pitié les Citrons Sorbex !</quest><quest id="37" title="Désamorceur de Bombinos">\t\t\tVous avez survécu aux explosions des Bombinos !</quest><quest id="38" title="Tueur de poires">\t\t\tVous avez vaincu un nouveau boss: les Poires-Melbombes !</quest><quest id="39" title="Mixeur de Tagadas">\t\t\tVous n&apos;avez fait qu&apos;une bouchée des Fraises-Tagada !</quest><quest id="40" title="Kiwi frotte s\'y pique">\t\t\tVous avez su éviter tous les pièges des Sapeur-kiwis !</quest><quest id="41" title="Chasseur de Bondissantes">\t\t\tVous ne vous êtes pas laissé impressionner par la meute de Bondissantes !</quest><quest id="42" title="Tronçonneur d\'Ananargeddons">\t\t\tVous êtes venu à bout d&apos;un groupe d&apos;Ananargeddons surentraînés !</quest><quest id="43" title="Roi de Hammerfest">\t\t\tVotre persévérance et votre ténacité ont eu raison du sorcier Tuberculoz:\n\t\t\tvous avez retrouvé la carotte de Igor ! Vous débuterez vos prochaines parties\n\t\t\tavec une vie supplémentaire et Igor portera fièrement sa carotte préférée.</quest><quest id="44" title="Chapelier fou">\t\t\tIgor s&apos;est découvert une passion nouvelle pour les coiffes. Vous pourrez\n\t\t\tmaintenant appuyer sur la touche &quot;D&quot; pendant la partie pour changer de\n\t\t\tdéguisement !</quest><quest id="45" title="Poney éco-terroriste">\t\t\tIgor a accumulé suffisament de richesses pour financer ses activités\n\t\t\tlouches en Hammerfest. Il peut maintenant se déguiser en appuyant sur la\n\t\t\ttouche &quot;D&quot; pendant la partie ! Et sinon, si ca n&apos;est pas déjà fait, avez-vous\n\t\t\tdéjà visité www.dinoparc.com ?</quest><quest id="46" title="Le Pioupiouz est en toi">\t\t\tIl fallait s&apos;y attendre: à force de ramasser n&apos;importe quoi, Igor s&apos;est\n\t\t\tfait gober par un Pioupiou ! Appuyez sur la touche &quot;D&quot; pendant la partie\n\t\t\tpour changer de déguisement (au fait, vous connaissiez le site\n\t\t\twww.pioupiouz.com ?)</quest><quest id="47" title="Chasseur de champignons">\t\t\tComme son homologue italien (plombier de son état), Igor a une passion\n\t\t\tbizarre pour les champignons. Il pourra désormais se déguiser\n\t\t\ten appuyant sur la touche &quot;D&quot; pendant la partie !</quest><quest id="48" title="Successeur de Tuberculoz">\t\t\tIgor semble avoir... changé.. Son regard est maintenant plus froid. Il\n\t\t\taffiche une mine sombre et se cache maintenant sous une grande cape\n\t\t\tpourpre. Petit à petit, il devient ce qu&apos;il a combattu... Vous pouvez\n\t\t\tmaintenant revêtir l&apos;apparence du sorcier Tuberculoz en appuyant sur &quot;D&quot;\n\t\t\tpendant la partie !</quest><quest id="49" title="La première clé !">\t\t\tIgor a trouvé une sorte de Passe-partout en Bois. Nul doute qu&apos;il ouvre\n\t\t\tune porte quelque part dans les cavernes...</quest><quest id="50" title="Rigor Dangerous">\t\t\tVous avez découvert dans votre aventure une vieille clé rouillée\n\t\t\tmystérieuse ! Elle comporte une petite mention gravée: &quot;Rick&quot;. Sans\n\t\t\tdoute son ancien propriétaire...</quest><quest id="51" title="La Méluzzine perdue">\t\t\tLa Méluzzine, clé légendaire sortie des vieux contes hammerfestiens,\n\t\t\touvre à ce qu&apos;on raconte la porte de grandes richesses. Reste à savoir où ?</quest><quest id="52" title="Enfin le Bourru !">\t\t\tCette étrange petite clé sent le vin.</quest><quest id="53" title="Congélation">\t\t\tBien qu&apos;étant un bonhomme de neige, Igor lui-même a du mal à garder cette clé\n\t\t\ten main, tant le froid qu&apos;elle dégage est intense. La porte qu&apos;elle ouvre\n\t\t\tdonne sûrement sur les endroits les plus reculés de Hammerfest.</quest><quest id="54" title="Une clé rouillée">\t\t\tCe petit bout de ferraille difforme n&apos;a pas l&apos;air d&apos;avoir une grande valeur.\n\t\t\tMais il faut parfois ce méfier des apparences ! Qui peut savoir quel genre d&apos;aventure\n\t\t\tse cache au delà de la porte qu&apos;elle ouvre ?</quest><quest id="55" title="Laissez passer !">\t\t\tVotre toute puissance administrative sera dorénavant appuyée par le formulaire\n\t\t\td&apos;Autorisation du Bois-Joli BJ22a.</quest><quest id="56" title="Les mondes ardus">\t\t\tPour avoir atteint le niveau 50 en mode Cauchemar, vous avez gagné la Clé\n\t\t\tdes Mondes Ardus.</quest><quest id="57" title="Viiiite !">\t\t\tSans trop savoir pourquoi, la Clé Piquante que vous avez trouvé vous donne une\n\t\t\tfolle envie de courir partout et de vous rouler en boule.</quest><quest id="58" title="Faire les poches à Tubz">\t\t\tTuberculoz, le vilain sorcier, portait sur lui une clé...</quest><quest id="59" title="Tuberculoz, seigneur des enfers">\t\t\tVotre toute puissance et votre maîtrise absolue des techniques de combat de\n\t\t\tHammerfest vous ont permi de gagner une clé unique en terrassant Tuberculoz\n\t\t\ten mode Cauchemar !</quest><quest id="60" title="L\'eau ferrigineuneuse">\t\t\tVotre excès dans la consommation de boissons alcoolisées vous a permis de\n\t\t\tdébloquer la Clé du Bourru ! Vous aurez maintenant de bonnes chances de la\n\t\t\ttrouver au cours de vos explorations.</quest><quest id="61" title="Paperasse administrative">\t\t\tIgor est un maître dans l&apos;art de remplir des formulaires administratifs.\n\t\t\tIl trouvera donc sans problème bientôt l&apos;Autorisation du Bois-Joli dans\n\t\t\tles dédales des cavernes...</quest><quest id="62" title="Meilleur joueur">\t\t\tLe meilleur joueur du net, c&apos;est vous, plus personne n&apos;a de doute là dessus !\n\t\t\tVotre collection de manettes de jeu fera sans nul doute des envieux ^^\n\t\t\tIgor dispose maintenant de l&apos;option Tornade qui procure un boost de vitesse\n\t\t\tau début de chaque partie, quelques soient les options choisies !</quest><quest id="63" title="Miroir, mon beau miroir">\t\t\tLe Miroir Bancal que vous avez trouvé en jeu vous permet maintenant de voir les choses\n\t\t\tsous un angle nouveau. L&apos;option de jeu &quot;Miroir&quot; a été débloquée !</quest><quest id="64" title="Mode cauchemar">\t\t\tVous êtes doué. Surement très doué même... Mais saurez-vous aider Igor en mode Cauchemar ?\n\t\t\tCette option a été débloquée !</quest><quest id="65" title="L\'aventure continue !">\t\t\tLe conseil des Carotteux vous a choisi pour explorer plus en avant les cavernes de Hammerfest.\n\t\t\tLa Clé de Gordon est une première étape dans cette nouvelle mission.</quest><quest id="66" title="Joyau d\'Ankhel">\t\t\tVous avez fait preuve d&apos;une dextérité et d&apos;une perspicacité sans égal en retrouvant le Joyau\n\t\t\td&apos;Ankhel. L&apos;option Controle du Ballon a été débloquée pour le mode SoccerFest !</quest><quest id="67" title="Sandy commence l\'aventure !">\t\t\tTous les éléments sont réunis pour donner naissance à Sandy, le bonhomme de sable !\n\t\t\tCe nouveau personnage pourra se joindre à vous dans le mode Multi Coopératif,\n\t\t\tjouable à deux sur le même ordinateur. Re-découvrez la grande aventure avec un ami !</quest><quest id="68" title="Miroir, NOTRE beau miroir">\t\t\tAvec le Miroir des Sables, vous pouvez maintenant voir les choses sous un angle nouveau\n\t\t\tmais à deux ! L&apos;option de jeu &quot;Miroir&quot; a été débloquée pour le mode Multi Coopératif !</quest><quest id="69" title="Mode double cauchemar">\t\t\tDe toute évidence, vous êtes capables de grandes choses à deux ! L&apos;option de jeu &quot;Cauchemar&quot;\n\t\t\ta été débloquée pour le mode Multi Coopératif !</quest><quest id="70" title="Une grande Amitié">\t\t\tMieux vaut tard que jamais ! Igor et Sandy ont enfin compris qu&apos;il valait mieux s&apos;entraider\n\t\t\ts&apos;ils voulaient survivre à deux dans les Cavernes de Hammerfest. L&apos;option de jeu &quot;Partage de vies&quot;\n\t\t\ta été débloquée pour le mode Multi Coopératif ! Si cette option est activée, lorsqu&apos;un joueur\n\t\t\tperd sa dernière vie, il en prend une au second joueur et peut ainsi continuer la partie !</quest><quest id="71" title="Apprentissage des canifs volants">\t\t\tA toujours sauter partout, Igor a fini par acquérir une souplesse digne d&apos;un ninja et une\n\t\t\tdextérité hors du commun ! Mais pour prouver sa valeur, il doit maintenant collecter les\n\t\t\tartefacts ninjas dispersés en Hammerfest !</quest><quest id="72" title="Shinobi do !">\t\t\tIgor a rempli sa quêté initiatique et maîtrise maintenant à la perfection un grand\n\t\t\tnombre d&apos;armes du Ninjutsu ! Mais comme son nouveau de l&apos;honneur le lui interdit, il ne\n\t\t\tpourra pas s&apos;en servir. Toutefois, il pourra mettre à l&apos;épreuve ses compétences grâce\n\t\t\tà l&apos;option de jeu &quot;Ninjutsu&quot; qu&apos;il vient de débloquer pour le mode Aventure !</quest><quest id="73" title="Rapide comme l\'éclair !">\t\t\tIgor est imbattable dès lors qu&apos;il s&apos;agit de viser juste et se déplacer avec précision...\n\t\t\tIl faut maintenant le prouver aux autres ! Le mode TIME ATTACK est débloqué et vous ouvre\n\t\t\ten plus l&apos;accès à un nouveau classement sur le site. Soyez le bonhomme de neige le\n\t\t\tplus rapide de tout Hammerfest !</quest><quest id="74" title="Maître des Bombes">\t\t\tVous êtes l&apos;expert réputé dans tout Hammerfest en matière d&apos;explosifs. Et pour montrer\n\t\t\tqu&apos;on ne vous ne la fait pas, à vous, l&apos;option &quot;Explosifs instables&quot; a été débloquée\n\t\t\tpour le mode Aventure ! Gare à TOUT ce qui explose en jeu ! De plus, vous pouvez pousser\n\t\t\ttoutes vos bombes plus loin en avançant en même temps que vous la frappez.</quest><quest id="75" title="Tombeau de Tuberculoz">\t\t\tVous avez découvert une étrange pyramide de poche dans le tombeau de Tuberculoz.</quest></quests><dimensions><dimension id="0"><level id="0" name="Village de Hammerfest"/><level id="2" name="Les premières cavernes"/><level id="10" name="Le terrier glissant"/><level id="16" name="Profondeurs oubliées"/><level id="20" name="Cavernes des Vert-tiges"/><level id="31" name="Région du Frimas"/><level id="41" name="Strates supérieures de Morgenfell"/><level id="51" name="Domaine Mangue-qui-roule"/><level id="61" name="Souterrain de la Prune"/><level id="71" name="Jungle cavernicole"/><level id="81" name="La grotte versicolore"/></dimension><dimension id="1"><level id="0" name="Temple pré-tuberculien"/><level id="12" name="Cachette du plombier"/><level id="14" name="BubbleFest"/><level id="24" name="Grottes du looping"/><level id="33" name="Petite salle bonus"/><level id="35" name="Diffiland"/><level id="41" name="Bois-Joli de Tournefleur"/><level id="55" name="Base secrète de Tubz"/><level id="60" name="Cavavin"/><level id="66" name="Cavavin: salle secrète"/><level id="68" name="Prototype Secret Tubzien"/><level id="70" name="Frimas Moejulien"/><level id="76" name="Piège de cristal !"/><level id="78" name="Bois caché de Hammerfest"/><level id="80" name="Antre des Géluloz"/><level id="86" name="Salle du Gardien"/><level id="90" name="Le deuxième Puits"/><level id="97" name="Les autres premières cavernes"/><level id="108" name="Bonus de Hâte"/><level id="110" name="La troglogrotte"/><level id="119" name="Le monde du Miroir"/><level id="125" name="Caverne de la nausée"/><level id="127" name="La cachette des Carotteux"/><level id="134" name="Le Dojo"/><level id="141" name="Les enfers"/><level id="148" name="Mixer de Sacha"/><level id="149" name="Puits des Zâmes"/><level id="154" name="La tanière des Framboijules"/><level id="157" name="Metal hurlant"/><level id="168" name="Crypte framboisée"/><level id="188" name="Le tombeau de Tuberculoz"/><level id="206" name="L\'entrée du tombeau !"/><level id="211" name="La véritable tombe de Tubz !"/><level id="217" name="Cachette des conseillers"/></dimension><dimension id="2"/><dimension id="3"><level id="0" name="Mechayame"/><level id="6" name="Irish Coffee"/><level id="12" name="Les Chat-vernes"/><level id="42" name="Ayamedium"/></dimension></dimensions><keys><key id="0" name="Passe-partout en bois"/><key id="1" name="Clé de Rigor-Dangerous"/><key id="2" name="Méluzzine"/><key id="3" name="Clé du Bourru"/><key id="4" name="Furtok glaciale"/><key id="5" name="Vieille clé rouillée"/><key id="6" name="Autorisation du Bois-Joli"/><key id="7" name="Clé des Mondes Ardus"/><key id="8" name="Clé Piquante"/><key id="9" name="Passe-partout de Tuberculoz"/><key id="10" name="Clé des cauchemars"/><key id="11" name="Clé d\'inversion"/><key id="12" name="Clé de Gordon"/><key id="13" name="Clé des apprentis"/><key id="14" name="Pass-Pyramide"/></keys></lang>'
    setVariable
  end // of frame 0

  defineMovieClip 1 // total frames: 1

    frame 0
      constants 'clearInterval', 'Std', 'setInterval', '}wUHz', 'length', 'split', 'indexOf', 'substring', '0', '19{O', 'set', 'get', '1', '=', '+-(0', 'onRelease', '5e{K', 'removeMovieClip', 'Log', 'trace', 'clear', '_xscale', '_alpha', 'gotoAndStop', '0tJ', 'stop', '_currentframe', '{aq}5', 'createEmptyMovieClip', 'songs', '0D 6', '6NDsC(', 'flashVersion', 'firstChild', 'nextSibling', '\n', 'substr', 'parseInt', 'isNaN', '(x5W-(', 'text', '?', '}3sU6', 'isDebug', '$', '7T4cF(', ' ', 'localeXml', 'Loader', '6{X3L', '+(*', '$mode', 'bind', 'params', 'saveRetry', '}kOb+(', ',EsGk', 'statics', 'options', '$options', 'mode', ')[ 4M', 'getStatic', '0QyZ*', '6I', 'LoadVars', 'send', 'initMid', '$mid', 'mid', 'POST', '_self', 'songNames', 'songId', '$hd', 'SharedObject', 'getLocal', 'saveGameUrl', 'data', 'savePreviousGame', '8PSfr', ',jYQm', 'changeView', ')3 =h', '}F', '(K', '0OA', '\nURL=', '[O)yj', 'loadSong', '9UZ}m', 'onLoad', '[uO7_', ' zpF (', '7F4[q', '};l3I', 'startGame', '2jZ*c', '(L-6', 'url', 'onData', 'addGetParams', '3Ywi)', '&', 'getRunId', '$_', 'qid', '*hr_A', '9tEy4(', 'charAt', '$runid', ';Rs=c', 'load', '\r', '\t', 'gid', '$gid', 'key', 'families', '=FDiF(', 'isLocal', '(R6pe', '},Iin', '=Lq7B', 'getProgress', 'bytesTotal', 'getBytesTotal', '$score', ' gyH6(', '8Qw0J', '[cs8B(', 'bottom', '0CaDf', '&retry=', '3[M_w', '4UtfI(', '-Ynhn', '1hOnL', '+o148', '$v', '$dev', 'genSongName', ';+', '4]y)n', 'GET', '$ok'  
      push 'Loader'
      function2 (r:2='params') (r:1='this')
        push r:this, 'params', r:'params'
        setMember
        push 16776960, 1, 'Log'
        getVariable
        push 'setColor'
        callMethod
        pop
        push r:this, 'saveRetry', 0
        setMember
        push r:this, '}kOb+(', 0
        setMember
        push r:this, ',EsGk', 0
        setMember
        push r:this, 'params'
        getMember
        push '$lang'
        getMember
        setRegister r:0
        push 'fr'
        strictEquals
        branchIfTrue label3
        push r:0, 'es'
        strictEquals
        branchIfTrue label4
        push r:0, 'en'
        strictEquals
        branchIfTrue label5
        branch label6
       label3:
        push r:this, 'localeXml', '_root'
        getVariable
        push 'frXml'
        getMember
        setMember
        branch label6
       label4:
        push r:this, 'localeXml', '_root'
        getVariable
        push 'esXml'
        getMember
        setMember
        branch label6
       label5:
        push r:this, 'localeXml', '_root'
        getVariable
        push 'enXml'
        getMember
        setMember
        branch label6
       label6:
        push r:this, 'statics', r:this, 'localeXml'
        getMember
        push 1, 'XML'
        new
        push 'firstChild'
        getMember
        push 'firstChild'
        getMember
        setMember
       label7:
        push r:this, 'statics'
        getMember
        push NULL
        equals
        not
        dup
        not
        branchIfTrue label8
        pop
        push r:this, 'statics'
        getMember
        push 'nodeName'
        getMember
        push 'statics'
        equals
        not
       label8:
        not
        branchIfTrue label9
        push r:this, 'statics', r:this, 'statics'
        getMember
        push 'nextSibling'
        getMember
        setMember
        branch label7
       label9:
        push r:this, 'options', r:this, 'params'
        getMember
        push '$options'
        getMember
        setMember
        push r:this, 'mode', r:this, 'params'
        getMember
        push '$mode'
        getMember
        setMember
        push 0, r:this, 'initMid'
        callMethod
        pop
        push '_root'
        getVariable
        push 'redirect'
        getMember
        setRegister r:6
        pop
        push r:6, NULL
        equals
        branchIfTrue label13
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push r:this, '6I'
        getMember
        push 'mid', r:this, 'mid'
        getMember
        setMember
        push 'POST', '_self', r:6, 3, r:this, '6I'
        getMember
        push 'send'
        callMethod
        pop
        push UNDEF
        return
       label13:
        push r:this, 'songNames', 'hurryUp.mp3', 'boss.mp3', 'music.mp3', 3
        initArray
        setMember
        push r:this, 'songId', 0
        setMember
        push r:this, 'songs', 0, 'Array'
        new
        setMember
        push r:this, 'flashVersion', 10, ',', 1, ' ', 1, '_root'
        getVariable
        push '$version'
        getMember
        push 'split'
        callMethod
        push 1
        getMember
        push 'split'
        callMethod
        push 0
        getMember
        push 2, 'parseInt'
        callFunction
        setMember
        push r:this, 'flashVersion'
        getMember
        push NULL
        equals
        not
        dup
        not
        branchIfTrue label1
        pop
        push r:this, 'flashVersion'
        getMember
        push 1, 'isNaN'
        callFunction
        not
       label1:
        dup
        not
        branchIfTrue label2
        pop
        push r:this, 'flashVersion'
        getMember
        push 8
        lessThan
        not
       label2:
        branchIfTrue label14
        push 15, 1, r:this, 'getStatic'
        callMethod
        push '\n\nhttp://www.macromedia.com/go/getflash'
        add
        push 1, r:this, '0QyZ*'
        callMethod
        pop
        branch label16
       label14:
        push '$hd', 1, 'SharedObject'
        getVariable
        push 'getLocal'
        callMethod
        setRegister r:7
        pop
        push r:this, 'saveGameUrl', r:7, 'data'
        getMember
        push 'data'
        getMember
        setMember
        push r:this, 'saveGameUrl'
        getMember
        push NULL
        equals
        branchIfTrue label15
        push 0, r:this, 'savePreviousGame'
        callMethod
        pop
        branch label16
       label15:
        push 0, r:this, '8PSfr'
        callMethod
        pop
       label16:
      end // of function 

      setRegister r:0
      setVariable
      push r:0, 'prototype'
      getMember
      setRegister r:1
      pop
      push r:1, '8PSfr'
      function2 () (r:1='this')
        push r:this, '+-(0', 1, r:this, 'params'
        getMember
        push 2, 'Std'
        getVariable
        push 'createEmptyMovieClip'
        callMethod
        setMember
        push r:this, ',jYQm', 2, r:this, 'params'
        getMember
        push 2, 'Std'
        getVariable
        push 'createEmptyMovieClip'
        callMethod
        setMember
        push 1, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '+(*'
        getMember
        push '_xscale', 0
        setMember
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '}F'
        getMember
        push 'text', ''
        setMember
        push r:this, ')3 =h'
        getMember
        push '_url'
        getMember
        setRegister r:2
        pop
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        push 'length'
        getMember
        push 0, 2, r:2, 'substr'
        callMethod
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        equals
        branchIfTrue label17
        push 66, 1, r:this, 'getStatic'
        callMethod
        push 1, r:this, '0QyZ*'
        callMethod
        pop
        push UNDEF
        return
       label17:
        push '(K', r:this
        varEquals
        push r:this, '0OA', 0, 'MovieClipLoader'
        new
        setMember
        push r:this, '0OA'
        getMember
        push 'onLoadError'
        function2 (r:2=';)', r:3='6uA') (r:1='this')
          push 67, 1, '(K'
          getVariable
          push 'getStatic'
          callMethod
          push ' : '
          add
          push r:'6uA'
          add
          push '\nURL='
          add
          push '_root'
          getVariable
          push 'baseUrl'
          getMember
          add
          push 1, '(K'
          getVariable
          push '0QyZ*'
          callMethod
          pop
        end // of function 

        setMember
        push r:this, '0OA'
        getMember
        push 'onLoadInit'
        function2 (r:2=';)') (r:1='this')
          push 0, '(K'
          getVariable
          push '[O)yj'
          callMethod
          pop
        end // of function 

        setMember
        push r:this, '+-(0'
        getMember
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        push '_root'
        getVariable
        push 'swf'
        getMember
        add
        push 2, r:this, '0OA'
        getMember
        push 'loadClip'
        callMethod
        branchIfTrue label18
        push 68, 1, r:this, 'getStatic'
        callMethod
        push '\n\nURL='
        add
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        add
        push 1, r:this, '0QyZ*'
        callMethod
        pop
       label18:
      end // of function 

      setMember
      push r:1, 'loadSong'
      function2 () (r:1='this')
        push 7, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        push r:this, ',jYQm'
        getMember
        push 1, 'Sound'
        new
        setMember
        push FALSE, '_root'
        getVariable
        push 'baseUrl'
        getMember
        push 'Loader'
        getVariable
        push '9UZ}m'
        getMember
        add
        push r:this, 'songNames'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        add
        push 2, r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        push 'loadSound'
        callMethod
        pop
        push r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        push 'onLoad', '[uO7_', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        push r:this, '}kOb+(', 'Loader'
        getVariable
        push ' zpF ('
        getMember
        setMember
        push r:this, ',EsGk', 'Loader'
        getVariable
        push '7F4[q'
        getMember
        setMember
      end // of function 

      setMember
      push r:1, '[O)yj'
      function2 () (r:1='this')
        push r:this, 'params'
        getMember
        push '$music'
        getMember
        push '0'
        equals
        branchIfTrue label19
        push 0, r:this, 'loadSong'
        callMethod
        pop
        branch label20
       label19:
        push 0, r:this, '};l3I'
        callMethod
        pop
       label20:
      end // of function 

      setMember
      push r:1, '[uO7_'
      function2 (r:2='[6') (r:1='this')
        push r:'[6'
        not
        branchIfTrue label23
        push r:this, 'songId', r:this, 'songId'
        getMember
        increment
        setMember
        push r:this, 'songId'
        getMember
        push r:this, 'songNames'
        getMember
        push 'length'
        getMember
        equals
        not
        branchIfTrue label21
        push 0, r:this, '};l3I'
        callMethod
        pop
        branch label22
       label21:
        push 0, r:this, '[O)yj'
        callMethod
        pop
       label22:
        branch label24
       label23:
        push 69, 1, r:this, 'getStatic'
        callMethod
        push 1, r:this, '0QyZ*'
        callMethod
        pop
       label24:
      end // of function 

      setMember
      push r:1, '};l3I'
      function2 () (r:1='this')
        push r:this, '+-(0'
        getMember
        push '{aq}5'
        getMember
        push '8pUQ2('
        getMember
        push NULL
        equals
        not
        branchIfTrue label25
        push 70, 1, r:this, 'getStatic'
        callMethod
        push 1, r:this, '0QyZ*'
        callMethod
        pop
        push UNDEF
        return
        branch label28
       label25:
        push 2, 1, r:this, 'changeView'
        callMethod
        pop
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        push ''
        equals
        dup
        branchIfTrue label26
        pop
        push '_root'
        getVariable
        push 'srv'
        getMember
        push NULL
        equals
       label26:
        not
        branchIfTrue label27
        push r:this, ')3 =h'
        getMember
        push 'onRelease', 'startGame', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        branch label28
       label27:
        push r:this, ')3 =h'
        getMember
        push 'onRelease', '2jZ*c', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
       label28:
      end // of function 

      setMember
      push r:1, '(L-6'
      function2 ('url') (r:1='this')
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push 'url'
        getVariable
        push NULL
        equals
        not
        branchIfTrue label29
        push 'url', ''
        setVariable
       label29:
        push '(K', r:this
        varEquals
        push r:this, '6I'
        getMember
        push 'onData'
        function2 (r:2='+') (r:1='this')
          push 'url'
          getVariable
          push 1, '(K'
          getVariable
          push '(L-6'
          callMethod
          pop
        end // of function 

        setMember
        push '', '_self', 'url'
        getVariable
        push 3, r:this, '6I'
        getMember
        push 'send'
        callMethod
        pop
      end // of function 

      setMember
      push r:1, 'addGetParams'
      function2 ('url', r:2='*feM7(') (r:1='this')
        push 0, '?', 2, 'url'
        getVariable
        push 'indexOf'
        callMethod
        push -1
        equals
        not
        setRegister r:3
        pop
        push '3Ywi)'
        var
        push r:3
        not
        branchIfTrue label30
        push '3Ywi)', '&'
        setVariable
        branch label31
       label30:
        push '3Ywi)', '?'
        setVariable
       label31:
        function2 (r:2='4', r:3='}') (r:1='this')
          push 'url', 'url'
          getVariable
          push '3Ywi)'
          getVariable
          add
          setVariable
          push 'url', 'url'
          getVariable
          push r:'4'
          add
          setVariable
          push 'url', 'url'
          getVariable
          push '='
          add
          setVariable
          push 'url', 'url'
          getVariable
          push r:'}'
          add
          setVariable
          push '3Ywi)', '&'
          setVariable
        end // of function 

        push 1, r:'*feM7(', '5uJ2'
        callMethod
        pop
        push 'url'
        getVariable
        return
      end // of function 

      setMember
      push r:1, 'getRunId'
      function2 () (r:1='this')
        push NULL
        setRegister r:2
        pop
        push 0, 'set_', 2, r:this, 'options'
        getMember
        push 'indexOf'
        callMethod
        setRegister r:3
        pop
        push r:3, 0
        lessThan
        branchIfTrue label32
        push r:3, 4
        add
        push '_', 2, r:this, 'options'
        getMember
        push 'indexOf'
        callMethod
        setRegister r:3
        pop
        push 10, 1, r:3, 1
        add
        push 2, r:this, 'options'
        getMember
        push 'substr'
        callMethod
        push 2, 'parseInt'
        callFunction
        setRegister r:2
        pop
       label32:
        push r:2
        return
      end // of function 

      setMember
      push r:1, '2jZ*c'
      function2 () (r:1='this')
        push r:this, 'qid', ''
        setMember
        push -1
        setRegister r:2
        pop
       label33:
        push r:2
        increment
        setRegister r:2
        push 16
        lessThan
        not
        branchIfTrue label34
        push r:this, 'qid', r:this, 'qid'
        getMember
        push 1, 63
        random
        add
        push 1, '*hr_A'
        getVariable
        push '9tEy4('
        getMember
        push 'charAt'
        callMethod
        add
        setMember
        branch label33
       label34:
        push 3, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push 'onRelease'
        delete
        pop
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push 0, '19{O'
        new
        setRegister r:3
        pop
        push r:this, 'qid'
        getMember
        push 'qid', 2, r:3, 'set'
        callMethod
        pop
        push r:this, 'mid'
        getMember
        push 'mid', 2, r:3, 'set'
        callMethod
        pop
        push r:this, 'mode'
        getMember
        push 'mode', 2, r:3, 'set'
        callMethod
        pop
        push r:this, 'options'
        getMember
        push 'options', 2, r:3, 'set'
        callMethod
        pop
        push 0, r:this, 'getRunId'
        callMethod
        setRegister r:4
        pop
        push r:4, NULL
        equals
        not
        branchIfTrue label35
        push r:4
        toString
        push '$runid', 2, r:3, 'set'
        callMethod
        pop
       label35:
        push r:3, '_root'
        getVariable
        push 'baseUrl'
        getMember
        push '_root'
        getVariable
        push 'srv'
        getMember
        add
        push 2, r:this, 'addGetParams'
        callMethod
        setRegister r:5
        pop
        push r:this, '6I'
        getMember
        push 'onData', ';Rs=c', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        push r:5, 1, r:this, '6I'
        getMember
        push 'load'
        callMethod
        pop
      end // of function 

      setMember
      push r:1, ';Rs=c'
      function2 (r:2='[') (r:1='this')
        push r:'[', NULL
        equals
        not
        branchIfTrue label36
        push 64, 1, r:this, 'getStatic'
        callMethod
        push 1, r:this, '0QyZ*'
        callMethod
        pop
        push UNDEF
        return
       label36:
        push TRUE
        not
        branchIfTrue label42
        push r:'[', 'length'
        getMember
        push 1
        subtract
        push 1, r:'[', 'charAt'
        callMethod
        setRegister r:3
        pop
        push r:3, ' '
        equals
        dup
        branchIfTrue label37
        pop
        push r:3, '\n'
        equals
       label37:
        dup
        branchIfTrue label38
        pop
        push r:3, '\r'
        equals
       label38:
        dup
        branchIfTrue label39
        pop
        push r:3, '\t'
        equals
       label39:
        not
        branchIfTrue label40
        push r:'[', 'length'
        getMember
        push 1
        subtract
        push 0, 2, r:'[', 'substr'
        callMethod
        setRegister r:'['
        pop
        branch label41
       label40:
        branch label42
       label41:
        branch label36
       label42:
        push TRUE
        not
        branchIfTrue label48
        push 0, 1, r:'[', 'charAt'
        callMethod
        setRegister r:4
        pop
        push r:4, ' '
        equals
        dup
        branchIfTrue label43
        pop
        push r:4, '\n'
        equals
       label43:
        dup
        branchIfTrue label44
        pop
        push r:4, '\r'
        equals
       label44:
        dup
        branchIfTrue label45
        pop
        push r:4, '\t'
        equals
       label45:
        not
        branchIfTrue label46
        push 1, 1, r:'[', 'substring'
        callMethod
        setRegister r:'['
        pop
        branch label47
       label46:
        branch label48
       label47:
        branch label42
       label48:
        push '&', 1, r:'[', 'split'
        callMethod
        setRegister r:5
        pop
        push 0, '19{O'
        new
        setRegister r:7
        pop
        push -1
        setRegister r:6
        pop
       label49:
        push r:6
        increment
        setRegister r:6
        push r:5, 'length'
        getMember
        lessThan
        not
        branchIfTrue label51
        push '=', 1, r:5, r:6
        getMember
        push 'split'
        callMethod
        setRegister r:8
        pop
        push r:8, 'length'
        getMember
        push 2
        equals
        not
        branchIfTrue label50
        push r:8, 1
        getMember
        push '$', r:8, 0
        getMember
        add
        push 2, r:7, 'set'
        callMethod
        pop
       label50:
        branch label49
       label51:
        push r:this, 'gid', '$gid', 1, r:7, 'get'
        callMethod
        setMember
        push r:this, 'key', '$key', 1, r:7, 'get'
        callMethod
        setMember
        push r:this, 'families', '$families', 1, r:7, 'get'
        callMethod
        setMember
        push r:this, 'gid'
        getMember
        push NULL
        equals
        dup
        branchIfTrue label52
        pop
        push r:this, 'key'
        getMember
        push NULL
        equals
       label52:
        not
        branchIfTrue label53
        push '$url', 1, r:7, 'get'
        callMethod
        push 1, r:this, '(L-6'
        callMethod
        pop
        push UNDEF
        return
       label53:
        push 0, r:this, 'startGame'
        callMethod
        pop
      end // of function 

      setMember
      push r:1, 'startGame'
      function2 () (r:1='this')
        push r:this, ')3 =h'
        getMember
        push 'onRelease'
        delete
        pop
        push '_global'
        getVariable
        push '7T4cF(', '7T4cF(', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        push '_global'
        getVariable
        push '(x5W-(', '(x5W-(', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        push r:this, '=FDiF(', 'localeXml', r:this, 'localeXml'
        getMember
        push 'isLocal', 0, r:this, 'isLocal'
        callMethod
        push 'families', r:this, 'families'
        getMember
        push 'options', r:this, 'options'
        getMember
        push 'songs', r:this, 'songs'
        getMember
        push 5
        initObject
        push r:this, '+-(0'
        getMember
        push 2
        push r:this, '+-(0'
        getMember
        push '{aq}5'
        getMember
        push UNDEF
        newMethod
        setMember
        push r:this, '0D 6', '(R6pe', r:this, 2, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
        push 6, 1, r:this, 'changeView'
        callMethod
        pop
      end // of function 

      setMember
      push r:1, '7T4cF('
        function2 (r:2='score', r:3='runId', r:4='-_X}5') (r:1='this')
        push r:this, '6NDsC('
        getMember
        not
        branchIfTrue label57
        push UNDEF
        return
       label57:
        push r:this, '6NDsC(', TRUE
        setMember
        push r:this, '},Iin', TRUE
        setMember
        push 5, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push '_alpha', 0
        setMember
        push r:this, '=Lq7B', 0, 'LoadVars'
        new
        setMember
        push '(K', r:this
        varEquals
        push r:this, 'key'
        getMember
        push 1, '*hr_A'
        new
        setRegister r:5
        pop
        push 0, '19{O'
        new
        setRegister r:7
        pop
        push r:this, '+-(0'
        getMember
        push 1, r:this, '0OA'
        getMember
        push 'getProgress'
        callMethod
        push 'bytesTotal'
        getMember
        push 'swfsize', 2, r:7, 'set'
        callMethod
        pop
        push 0, '_root'
        getVariable
        push 'getBytesTotal'
        callMethod
        push 'lsize', 2, r:7, 'set'
        callMethod
        pop
        push r:'score', 'score', 2, r:7, 'set'
        callMethod
        pop
        push r:'runId', NULL
        equals
        branchIfTrue label58
        push r:'runId', '$runid', 2, r:7, 'set'
        callMethod
        pop
       label58:
        push r:'-_X}5', 'p', 2, r:7, 'set'
        callMethod
        pop
        push r:this, 'gid'
        getMember
        push 'gid', 2, r:7, 'set'
        callMethod
        pop
        push r:this, 'mid'
        getMember
        push 'mid', 2, r:7, 'set'
        callMethod
        pop
        push 0, '19{O'
        new
        setRegister r:6
        pop
        push r:this, 'gid'
        getMember
        push 'gid', 2, r:6, 'set'
        callMethod
        pop
        push r:7, 1, r:5, '=Z(MN'
        callMethod
        push 'score', 2, r:6, 'set'
        callMethod
        pop
        push r:this, 'saveGameUrl', r:6, '_root'
        getVariable
        push 'baseUrl'
        getMember
        push '_root'
        getVariable
        push 'srv'
        getMember
        add
        push 2, r:this, 'addGetParams'
        callMethod
        setMember
        push 0, r:this, 'isLocal'
        callMethod
        not
        branchIfTrue label61
        push r:'score', ' / '
        add
        push 'score', 1, r:6, 'get'
        callMethod
        push 'length'
        getMember
        add
        push 'b '
        add
        push 1, r:this, '0QyZ*'
        callMethod
        pop
        branch label65
       label61:
        push r:this, '8Qw0J'
        function2 () (r:1='this')
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 0
          greaterThan
          not
          branchIfTrue label64
          push '(K'
          getVariable
          push ')3 =h'
          getMember
          push '[cs8B('
          getMember
          push 'text', 60, 1, '(K'
          getVariable
          push 'getStatic'
          callMethod
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 1
          add
          add
          setMember
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 3
          lessThan
          branchIfTrue label64
          push '(K'
          getVariable
          push ')3 =h'
          getMember
          push 'bottom'
          getMember
          push 'text', ''
          setMember
          push '(K'
          getVariable
          push ')3 =h'
          getMember
          push '[cs8B('
          getMember
          push 'text', 72, 1, '(K'
          getVariable
          push 'getStatic'
          callMethod
          setMember
          push '$hd', 1, 'SharedObject'
          getVariable
          push 'getLocal'
          callMethod
          setRegister r:2
          pop
          push 0, r:2, 'clear'
          callMethod
          pop
        push r:2
        push 'data'
        getMember
          push 'data'
        push '(K'
        getVariable
          push 'saveGameUrl'
          getMember
          setMember
          push 0, r:2, 'flush'
          callMethod
          setRegister r:3
          pop
          push r:3
          not
          branchIfTrue label62
          push 'Suivez les instructions de la page Support technique (en bas du site) pour VIDER VOTRE CACHE.', 1, 'Log'
          getVariable
          push 'trace'
          callMethod
          pop
          push 'Déconnectez-vous puis reconnectez-vous au site, avant de lancer le jeu à nouveau: une nouvelle tentative de sauvegarde sera alors effectuée.', 1, 'Log'
          getVariable
          push 'trace'
          callMethod
          pop
          branch label63
         label62:
          push 'Sauvegarde impossible.', 1, 'Log'
          getVariable
          push 'trace'
          callMethod
          pop
          push 'Pour éviter ce genre de problème à l\'avenir, faites un clic droit sur le jeu, choisissez Paramètres, puis l\'icone du Dossier dans la nouvelle fenetre. Décochez "Jamais" et assurez-vous d\'autoriser au moins 100ko de données (en déplaçant la petite barre).', 1, 'Log'
          getVariable
          push 'trace'
          callMethod
          pop
         label63:
          push 0, '(K'
          getVariable
          push 'params'
          getMember
          push 'stop'
          callMethod
          pop
          push '(K'
          getVariable
          push '0CaDf'
          getMember
          push 1, 'clearInterval'
          callFunction
          pop
         label64:
          push '(K'
          getVariable
          push 'saveRetry', '(K'
          getVariable
          push 'saveRetry'
          getMember
          increment
          setMember
          push '(K'
          getVariable
          push 'saveGameUrl'
          getMember
          push '&retry='
          add
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          add
          push 1, '(K'
          getVariable
          push '=Lq7B'
          getMember
          push 'load'
          callMethod
          pop
        end // of function 

        setMember
        push 0, r:this, '8Qw0J'
        callMethod
        pop
        push r:this, '0CaDf', 'Loader'
        getVariable
        push '3[M_w'
        getMember
        push r:this, '8Qw0J'
        getMember
        push 2, 'setInterval'
        callFunction
        setMember
       label65:
      end // of function 

      setMember
      push r:1, 'savePreviousGame'
      function2 () (r:1='this')
        push 5, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push 'bottom'
        getMember
        push 'text', 71, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, '=Lq7B', 0, 'LoadVars'
        new
        setMember
        push r:this, '4UtfI(', TRUE
        setMember
        push r:this, '},Iin', TRUE
        setMember
        push r:this, '0CaDf', 0
        setMember
        push '(K', r:this
        varEquals
        push r:this, '8Qw0J'
        function2 () (r:1='this')
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 0
          greaterThan
          not
          branchIfTrue label66
          push '(K'
          getVariable
          push ')3 =h'
          getMember
          push '[cs8B('
          getMember
          push 'text', 60, 1, '(K'
          getVariable
          push 'getStatic'
          callMethod
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 1
          add
          add
          setMember
         label66:
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          push 3
          lessThan
          branchIfTrue label67
          push 'Impossible de terminer cette sauvegarde, cette partie ne pourra pas être enregistrée.', 1, 'Log'
          getVariable
          push 'trace'
          callMethod
          pop
          push '$hd', 1, 'SharedObject'
          getVariable
          push 'getLocal'
          callMethod
          setRegister r:2
          pop
          push 0, r:2, 'clear'
          callMethod
          pop
          push 0, '(K'
          getVariable
          push 'params'
          getMember
          push 'stop'
          callMethod
          pop
          push '(K'
          getVariable
          push '0CaDf'
          getMember
          push 1, 'clearInterval'
          callFunction
          pop
         label67:
          push '(K'
          getVariable
          push 'saveRetry', '(K'
          getVariable
          push 'saveRetry'
          getMember
          increment
          setMember
          push '(K'
          getVariable
          push 'saveGameUrl'
          getMember
          push '&retry='
          add
          push '(K'
          getVariable
          push 'saveRetry'
          getMember
          add
          push 1, '(K'
          getVariable
          push '=Lq7B'
          getMember
          push 'load'
          callMethod
          pop
        end // of function 

        setMember
        push 0, r:this, '8Qw0J'
        callMethod
        pop
        push r:this, '0CaDf', 'Loader'
        getVariable
        push '3[M_w'
        getMember
        push r:this, '8Qw0J'
        getMember
        push 2, 'setInterval'
        callFunction
        setMember
      end // of function 

      setMember
      push r:1, '(x5W-('
      function2 (r:2='url', r:3='*feM7(') (r:1='this')
        push r:this, '-Ynhn'
        getMember
        not
        branchIfTrue label68
        push UNDEF
        return
       label68:
        push r:this, '},Iin', TRUE
        setMember
        push r:this, '-Ynhn', TRUE
        setMember
        push r:this, '1hOnL', r:url
        setMember
        push r:this, '+o148', r:'*feM7('
        setMember
        push 6, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push '_alpha', 0
        setMember
      end // of function 

      setMember
      push r:1, '}wUHz'
      function2 (r:2='=rJ', r:3='7olX7', r:4='}wUHz') (r:1='this')
        push r:'=rJ', NULL
        equals
        not
        branchIfTrue label69
        push NULL
        return
       label69:
        push r:'7olX7', 'length'
        getMember
        setRegister r:5
        pop
        push r:5, 1
        equals
        not
        branchIfTrue label70
        push r:'}wUHz', 1, r:'7olX7', 1, r:'=rJ', 'split'
        callMethod
        push 'join'
        callMethod
        return
       label70:
        push 0
        setRegister r:7
        pop
        push ''
        setRegister r:8
        pop
       label71:
        push TRUE
        not
        branchIfTrue label73
        push r:7, r:'7olX7', 2, r:'=rJ', 'indexOf'
        callMethod
        setRegister r:6
        pop
        push r:6, -1
        equals
        not
        branchIfTrue label72
        branch label73
       label72:
        push r:8, r:6, r:7
        subtract
        push r:7, 2, r:'=rJ', 'substr'
        callMethod
        push r:'}wUHz'
        add
        add
        setRegister r:8
        pop
        push r:6, r:5
        add
        setRegister r:7
        pop
        branch label71
       label73:
        push r:8, r:7, 1, r:'=rJ', 'substring'
        callMethod
        add
        return
      end // of function 

      setMember
      push r:1, '0QyZ*'
      function2 (r:2='6uA') (r:1='this')
        push 4, 1, r:this, 'changeView'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push '0QyZ*'
        getMember
        push 'text', 'URL=', '_root'
        getVariable
        push 'baseUrl'
        getMember
        add
        push '\n\n\n'
        add
        push r:'6uA'
        add
        setMember
        push 'build=', '01-11 11:48:50'
        add
        push 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push 'msg=', r:'6uA'
        add
        push 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push r:this, ')3 =h'
        getMember
        push 'onRelease', '', '(L-6', r:this, 3, 'Std'
        getVariable
        push 'bind'
        callMethod
        setMember
      end // of function 

      setMember
      push r:1, 'initMid'
      function2 () (r:1='this')
        push '$mid', 1, 'SharedObject'
        getVariable
        push 'getLocal'
        callMethod
        setRegister r:2
        pop
        push r:this, 'mid', r:2, 'data'
        getMember
        push '$v'
        getMember
        setMember
        push r:this, 'mid'
        getMember
        push 'length'
        getMember
        push 8
        equals
        branchIfTrue label78
        push r:this, 'mid', ''
        setMember
        push -1
        setRegister r:3
        pop
       label76:
        push r:3
        increment
        setRegister r:3
        push 8
        lessThan
        not
        branchIfTrue label77
        push r:this, 'mid', r:this, 'mid'
        getMember
        push 1, 63
        random
        add
        push 1, '*hr_A'
        getVariable
        push '9tEy4('
        getMember
        push 'charAt'
        callMethod
        add
        setMember
        branch label76
       label77:
        push r:2, 'data'
        getMember
        push '$v', r:this, 'mid'
        getMember
        setMember
       label78:
      end // of function 

      setMember
      push r:1, 'isLocal'
      function2 () (r:1='this')
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        push ''
        equals
        return
      end // of function 

      setMember
      push r:1, 'isDebug'
      function2 () (r:1='this')
        push '_root'
        getVariable
        push 'debug'
        getMember
        push True
        equals
        return
      end // of function 

      setMember
      push r:1, 'getStatic'
      function2 (r:2='id') (r:1='this')
        push r:this, 'statics'
        getMember
        push 'firstChild'
        getMember
        setRegister r:3
        pop
       label82:
        push 'id', 1, r:3, 'get'
        callMethod
        push '', r:'id'
        add
        equals
        branchIfTrue label83
        push r:3, 'nextSibling'
        getMember
        setRegister r:3
        pop
        branch label82
       label83:
        push 'v', 1, r:3, 'get'
        callMethod
        return
      end // of function 

      setMember
      push r:1, 'genSongName'
      function2 () (r:1='this')
        push 'No pain, no ', 'Searching for ', 'Unblessed ', 'Song for ', 'Wings of ', 'Hammerfest, quest for ', 'Blades of ', 'The legend of ', 'Prepare for ', 'Everlasting ', 'Beyond ', 'Desperate ', 'Spirits of ', 'An almighty ', 'Lost ', 'The great ', 'Battle for ', 17
        initArray
        setRegister r:2
        pop
        push 'carrot !', 'darkness ', 'forgiveness ', 'love ', 'flames ', 'destruction ', 'redemption ', 'glory ', 'souls ', 'death ', 'sadness ', 'hope ', 'Wanda ', 'Igor ', 14
        initArray
        setRegister r:3
        pop
        push r:this, 'songId'
        getMember
        push 10
        lessThan
        not
        branchIfTrue label84
        push '0', r:this, 'songId'
        getMember
        push 1
        add
        add
        branch label85
       label84:
        push '', r:this, 'songId'
        getMember
        push 1
        add
        add
       label85:
        push '. «'
        add
        push r:2, r:2, 'length'
        getMember
        random
        getMember
        add
        push r:3, r:3, 'length'
        getMember
        random
        getMember
        add
        push '»'
        add
        return
      end // of function 

      setMember
      push r:1, 'changeView'
      function2 (r:2=' IWA+') (r:1='this')
        push 0, r:this, ')3 =h'
        getMember
        push 'removeMovieClip'
        callMethod
        pop
        push r:this, ')3 =h', 99, ')3 =h', r:this, 'params'
        getMember
        push 3, 'Std'
        getVariable
        push 'attachMovie'
        callMethod
        setMember
        push '', r:' IWA+'
        add
        push 1, r:this, ')3 =h'
        getMember
        push 'gotoAndStop'
        callMethod
        pop
        push 0, r:this, 'isDebug'
        callMethod
        not
        branchIfTrue label86
        push '2', 1, r:this, ')3 =h'
        getMember
        push ';+'
        getMember
        push 'gotoAndStop'
        callMethod
        pop
        branch label87
       label86:
        push '1', 1, r:this, ')3 =h'
        getMember
        push ';+'
        getMember
        push 'gotoAndStop'
        callMethod
        pop
       label87:
        push r:' IWA+', 1
        equals
        not
        branchIfTrue label88
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '4]y)n', 1
        setMember
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '}3sU6'
        getMember
        push 'text', 50, 1, r:this, 'getStatic'
        callMethod
        setMember
       label88:
        push r:' IWA+', 2
        equals
        dup
        branchIfTrue label89
        pop
        push r:' IWA+', 3
        equals
       label89:
        not
        branchIfTrue label94
        push r:this, ')3 =h'
        getMember
        push '4]y)n', 1
        setMember
        push r:this, ')3 =h'
        getMember
        push '}3sU6'
        getMember
        push 'text', 50, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, ')3 =h'
        getMember
        push '}F'
        getMember
        push 'text', 52, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:' IWA+', 2
        equals
        not
        branchIfTrue label93
        push r:this, ')3 =h'
        getMember
        push 'bottom'
        getMember
        push 'text', 53, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, 'mode'
        getMember
        push 'tutorial'
        equals
        dup
        branchIfTrue label90
        pop
        push r:this, mode
        getMember
        push 'soccer'
        equals
       label90:
        not
        branchIfTrue label91
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', 58, 1, r:this, 'getStatic'
        callMethod
        setMember
        branch label93
       label91:
        push 0, r:this, 'isDebug'
        callMethod
        not
        branchIfTrue label92
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', 59, 1, r:this, 'getStatic'
        callMethod
        setMember
        branch label93
       label92:
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', 57, 1, r:this, 'getStatic'
        callMethod
        setMember
       label93:
        push r:' IWA+', 3
        equals
        not
        branchIfTrue label94
        push r:this, ')3 =h'
        getMember
        push 'bottom'
        getMember
        push 'text', 54, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', ''
        setMember
       label94:
        push r:' IWA+', 5
        equals
        not
        branchIfTrue label95
        push r:this, ')3 =h'
        getMember
        push 'bottom'
        getMember
        push 'text', 55, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', 56, 1, r:this, 'getStatic'
        callMethod
        setMember
       label95:
        push r:' IWA+', 6
        equals
        not
        branchIfTrue label96
       label96:
        push r:' IWA+', 7
        equals
        not
        branchIfTrue label97
        push r:this, ')3 =h'
        getMember
        push '4]y)n', 1
        setMember
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '}3sU6'
        getMember
        push 'text', 61, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '}F'
        getMember
        push 'text', 0, r:this, 'genSongName'
        callMethod
        setMember
       label97:
      end // of function 

      setMember
      push r:1, '1N59{'
      function2 (r:2='url') (r:1='this')
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push 'GET', '_self', r:url, 3, r:this, '6I'
        getMember
        push 'send'
        callMethod
        pop
      end // of function 

      setMember
      push r:1, '0D 6'
      function2 () (r:1='this')
        push r:this, ')3 =h'
        getMember
        push NULL
        equals
        branchIfTrue label103
        push r:this, ')3 =h'
        getMember
        push '_currentframe'
        getMember
        push 1
        equals
        not
        branchIfTrue label98
        push r:this, '+-(0'
        getMember
        push 1, r:this, '0OA'
        getMember
        push 'getProgress'
        callMethod
        setRegister r:2
        pop
        push r:2, 'bytesTotal'
        getMember
        push 0
        greaterThan
        not
        branchIfTrue label98
        push r:2, 'bytesLoaded'
        getMember
        push r:2, 'bytesTotal'
        getMember
        divide
        setRegister r:3
        pop
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '}F'
        getMember
        push 'text', r:3, 100
        multiply
        push 1, 'Math'
        getVariable
        push 'round'
        callMethod
        push ' '
        add
        push 51, 1, r:this, 'getStatic'
        callMethod
        add
        setMember
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '+(*'
        getMember
        push '_xscale', r:3, 100
        multiply
        setMember
       label98:
        push r:this, ')3 =h'
        getMember
        push '_currentframe'
        getMember
        push 7
        equals
        not
        branchIfTrue label103
        push 0, r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        push 'getBytesTotal'
        callMethod
        setRegister r:4
        pop
        push 0, r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        push 'getBytesLoaded'
        callMethod
        setRegister r:5
        pop
        push r:5, 1, 'isNaN'
        callFunction
        dup
        branchIfTrue label99
        pop
        push r:4, 1, 'isNaN'
        callFunction
       label99:
        not
        branchIfTrue label100
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '+(*'
        getMember
        push '_xscale', 0
        setMember
        push r:this, '}kOb+(', r:this, '}kOb+('
        getMember
        decrement
        setMember
        push r:this, '}kOb+('
        getMember
        push 0
        greaterThan
        branchIfTrue label100
        push 'Impossible de charger la musique. Merci de VIDER VOTRE CACHE INTERNET, comme expliqué dans la section Support technique. Vous pouvez également désactiver les musiques depuis les Paramètres du jeu.\n', 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push 'Vous pouvez enfin laisser un message dans le forum en précisant les informations suivantes:', 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push '\n---------------\n', 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push 'music #', r:this, 'songId'
        getMember
        add
        push ' timed out\nLOADED='
        add
        push r:5
        add
        push '\nTOTAL='
        add
        push r:4
        add
        push '\nURL='
        add
        push '_root'
        getVariable
        push 'baseUrl'
        getMember
        push 'Loader'
        getVariable
        push '9UZ}m'
        getMember
        add
        push r:this, 'songNames'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        add
        add
        push 1, 'Log'
        getVariable
        push 'trace'
        callMethod
        pop
        push r:this, '}kOb+(', 999999
        setMember
       label100:
        push r:4, 1, 'isNaN'
        callFunction
        not
        dup
        not
        branchIfTrue label101
        pop
        push r:4, 0
        greaterThan
       label101:
        not
        branchIfTrue label103
        push r:5, r:4
        divide
        setRegister r:6
        pop
        push r:6, 0.99
        lessThan
        branchIfTrue label102
        push r:this, ',EsGk', r:this, ',EsGk'
        getMember
        decrement
        setMember
        push r:this, ',EsGk'
        getMember
        push 0
        greaterThan
        branchIfTrue label102
        push r:this, 'songs'
        getMember
        push r:this, 'songId'
        getMember
        getMember
        push 'onLoad', NULL
        setMember
        push TRUE, 1, r:this, '[uO7_'
        callMethod
        pop
       label102:
        push r:this, ')3 =h'
        getMember
        push '0tJ'
        getMember
        push '+(*'
        getMember
        push '_xscale', r:6, 100
        multiply
        setMember
       label103:
        push r:this, '4UtfI('
        getMember
        not
        branchIfTrue label104
        push r:this, '=Lq7B'
        getMember
        push 'ok'
        getMember
        setRegister r:7
        pop
        push r:7, NULL
        equals
        branchIfTrue label104
        push '$hd', 1, 'SharedObject'
        getVariable
        push 'getLocal'
        callMethod
        setRegister r:8
        pop
        push 0, r:8, 'clear'
        callMethod
        pop
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push '', '_self', r:7, 3, r:this, '6I'
        getMember
        push 'send'
        callMethod
        pop
        push r:this, '=Lq7B', NULL
        setMember
       label104:
      end // of function 

      setMember
      push r:1, '(R6pe'
      function2 () (r:1='this')
        push r:this, '},Iin'
        getMember
        not
        branchIfTrue label107
        push r:this, ')3 =h'
        getMember
        push '_alpha', r:this, ')3 =h'
        getMember
        push '_alpha'
        getMember
        push 5
        add
        setMember
        push r:this, ')3 =h'
        getMember
        push '_alpha'
        getMember
        push 100
        lessThan
        not
        dup
        not
        branchIfTrue label105
        pop
        push r:this, '+-(0'
        getMember
        push '_name'
        getMember
        push NULL
        equals
        not
       label105:
        not
        branchIfTrue label106
        push 0, r:this, '+-(0'
        getMember
        push 'removeMovieClip'
        callMethod
        pop
        push r:this, '=FDiF(', NULL
        setMember
       label106:
        push r:this, ')3 =h'
        getMember
        push '_alpha'
        getMember
        push 220
        lessThan
        branchIfTrue label107
        push r:this, ')3 =h'
        getMember
        push '_alpha', 100
        setMember
        push r:this, '},Iin', FALSE
        setMember
       label107:
        push r:this, '-Ynhn'
        getMember
        not
        branchIfTrue label109
        push r:this, '},Iin'
        getMember
        branchIfTrue label109
        push 0, 'LoadVars'
        new
        setRegister r:2
        pop
        push r:this, '+o148'
        getMember
        push NULL
        equals
        branchIfTrue label108
        push r:2, '+', r:this, '+o148'
        getMember
        setMember
       label108:
        push 'POST', '_self', r:this, '1hOnL'
        getMember
        push 3, r:2, 'send'
        callMethod
        pop
        push r:this, '-Ynhn', FALSE
        setMember
        push r:this, '(R6pe', NULL
        setMember
        push r:this, '0D 6', NULL
        setMember
        push UNDEF
        return
       label109:
        push r:this, '6NDsC('
        getMember
        not
        branchIfTrue label112
        push r:this, '=Lq7B'
        getMember
        push 'ok'
        getMember
        setRegister r:3
        pop
        push r:3, NULL
        equals
        not
        dup
        not
        branchIfTrue label110
        pop
        push r:this, '},Iin'
        getMember
        not
       label110:
        not
        branchIfTrue label111
        push '$hd', 1, 'SharedObject'
        getVariable
        push 'getLocal'
        callMethod
        setRegister r:4
        pop
        push 0, r:4, 'clear'
        callMethod
        pop
        push r:this, '0CaDf'
        getMember
        push 1, 'clearInterval'
        callFunction
        pop
        push r:this, ')3 =h'
        getMember
        push 'bottom'
        getMember
        push 'text', 73, 1, r:this, 'getStatic'
        callMethod
        setMember
        push r:this, ')3 =h'
        getMember
        push '[cs8B('
        getMember
        push 'text', ''
        setMember
        push r:this, '6I', 0, 'LoadVars'
        new
        setMember
        push 'GET', '_self', r:3, 3, r:this, '6I'
        getMember
        push 'send'
        callMethod
        pop
        push r:this, '=Lq7B', NULL
        setMember
       label111:
        push UNDEF
        return
       label112:
        push r:this, '},Iin'
        getMember
        not
        dup
        not
        branchIfTrue label113
        pop
        push r:this, '-Ynhn'
        getMember
        not
       label113:
        dup
        not
        branchIfTrue label114
        pop
        push r:this, '6NDsC('
        getMember
        not
       label114:
        not
        branchIfTrue label115
        push r:this, ')3 =h'
        getMember
        push NULL
        equals
        branchIfTrue label115
        push r:this, ')3 =h'
        getMember
        push '_alpha', r:this, ')3 =h'
        getMember
        push '_alpha'
        getMember
        push 2
        subtract
        setMember
        push r:this, ')3 =h'
        getMember
        push '_alpha'
        getMember
        push 0
        greaterThan
        branchIfTrue label115
        push 0, r:this, ')3 =h'
        getMember
        push 'removeMovieClip'
        callMethod
        pop
        push r:this, ')3 =h', NULL
        setMember
       label115:
        push 0, r:this, '=FDiF('
        getMember
        push '0D 6'
        callMethod
        pop
      end // of function 

      setMember
      push r:0, '9UZ}m', 'sfx/'
      setMember
      push r:0, '3[M_w', 8000
      setMember
      push r:0, ' zpF (', 32, 10
      multiply
      setMember
      push r:0, '7F4[q', 32, 2.5
      multiply
      setMember

      #include './encoder.asm'
    end // of frame 0
  end // of defineMovieClip 1

  exportAssets
    1 as '90D*'
  end // of exportAssets

  defineMovieClip 7 // total frames: 2
  end // of defineMovieClip 7

  defineMovieClip 13 // total frames: 8

    frame 0
      constants '_parent'  
      push '_parent'
      getVariable
      push '_parent'
      getMember
      push '4]y)n'
      getMember
      push 1
      add
      gotoAndStop
    end // of frame 0
  end // of defineMovieClip 13

  defineMovieClip 32 // total frames: 10
  end // of defineMovieClip 32

  defineMovieClip 33 // total frames: 9

    frame 0
      constants '_parent'  
      push '_parent'
      getVariable
      push '_parent'
      getMember
      push '4]y)n'
      getMember
      push 1
      add
      gotoAndStop
    end // of frame 0
  end // of defineMovieClip 33

  defineMovieClip 53 // total frames: 14
  end // of defineMovieClip 53

  defineMovieClip 58 // total frames: 1
  end // of defineMovieClip 58

  defineMovieClip 60 // total frames: 1
  end // of defineMovieClip 60

  defineMovieClip 66 // total frames: 4

    frame 0
      stop
    end // of frame 0
  end // of defineMovieClip 66

  defineMovieClip 81 // total frames: 28
  end // of defineMovieClip 81

  defineMovieClip 92 // total frames: 7
  end // of defineMovieClip 92
  
  exportAssets
    92 as ')3 =h'
  end // of exportAssets

  frame 0
    constants '90D*'  
    push 0, ';7pIo'
    getVariable
    push 'init'
    callMethod
    pop
    push 0, '90D*', '90D*', 3, 'attachMovie'
    callFunction
    pop
  end // of frame 0

  defineMovieClip 94 // total frames: 0
  end // of defineMovieClip 94
  
  exportAssets
    94 as '__Packages.StdInc'
  end // of exportAssets
  
  initMovieClip 94
    constants '_global', ';7pIo', 'prototype', 'init', '}J', 'string', 'Array', '0ENPA', 'length', 'splice', '4]GSJ(', 'Color', '{L 35', 'XMLNode', 'get', 'attributes', 'set', 'has', '}}{7B', 'XML', 'ASSetPropFlags', 'Std', 'Object', 'mc_id', 'createTextField', 'toString', '8', '-', 'apply', '(', 'Log', 'printed', 'text', 'traced', 'Stage', '_x', '_y', '_width', '_height', 'wordWrap', 'selectable', '(K', 'append', '\r', '\n', 'split', 'join', 'textColor', 'removeMovieClip', 'movieclip', '    ', 'substring', '19{O'  
    push '_global'
    getVariable
    push ';7pIo'
    function ()
    end // of function 

    setMember
    push ';7pIo'
    getVariable
    push 'init'
    function ()
      push 0
      function2 () (r:1='this', r:2='arguments', r:3='_root', r:4='_global')
        push 'Array'
        getVariable
        push 'prototype'
        getMember
        push '0ENPA'
        function2 (r:4='+KD') (r:1='this')
          push r:this, 'length'
          getMember
          setRegister r:3
          pop
          push -1
          setRegister r:2
          pop
         label2:
          push r:2
          increment
          setRegister r:2
          push r:3
          lessThan
          not
          branchIfTrue label4
          push r:this, r:2
          getMember
          push r:'+KD'
          equals
          not
          branchIfTrue label3
          push 1, r:2, 2, r:this, 'splice'
          callMethod
          pop
          push TRUE
          return
         label3:
          branch label2
         label4:
          push FALSE
          return
        end // of function 

        setMember
        push 'Array'
        getVariable
        push 'prototype'
        getMember
        push '4]GSJ('
        function2 (r:2='2', r:3='((') (r:1='this')
          push r:'((', 0, r:'2', 3, r:this, 'splice'
          callMethod
          pop
        end // of function 

        setMember
        push 'Color'
        getVariable
        push 'prototype'
        getMember
        push '{L 35'
        function2 () (r:1='this')
          push 'ra', 100, 'rb', 0, 'ba', 100, 'bb', 0, 'ga', 100, 'gb', 0, 'aa', 100, 'ab', 0, 8
          initObject
          push 1, r:this, 'setTransform'
          callMethod
          pop
        end // of function 

        setMember
        push 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 'get'
        function2 (r:2='k') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k'
          getMember
          return
        end // of function 

        setMember
        push 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 'set'
        function2 (r:3='k', r:2='v') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k', r:'v'
          setMember
        end // of function 

        setMember
        push 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 'has'
        function2 (r:2='k') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k'
          getMember
          push NULL
          equals
          not
          return
        end // of function 

        setMember
        push 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push '}}{7B'
        function2 (r:3='-') (r:1='this')
          push r:this, 'attributes'
          getMember
          enumerateValue
         label5:
          setRegister r:0
          push NULL
          equals
          branchIfTrue label6
          push r:0
          setRegister r:2
          pop
          push r:this, 'attributes'
          getMember
          push r:2
          getMember
          push r:2, 2, r:'-', UNDEF
          callMethod
          pop
          branch label5
         label6:
        end // of function 

        setMember
        push 'XML'
        getVariable
        push 'prototype'
        getMember
        push 'get'
        function2 (r:2='k') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k'
          getMember
          return
        end // of function 

        setMember
        push 'XML'
        getVariable
        push 'prototype'
        getMember
        push 'set'
        function2 (r:3='k', r:2='v') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k', r:'v'
          setMember
        end // of function 

        setMember
        push 'XML'
        getVariable
        push 'prototype'
        getMember
        push 'has'
        function2 (r:2='k') (r:1='this')
          push r:this, 'attributes'
          getMember
          push r:'k'
          getMember
          push NULL
          equals
          not
          return
        end // of function 

        setMember
        push 1, '0ENPA', 'Array'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, '4]GSJ(', 'Array'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, '{L 35', 'Color'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'get', 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'set', 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'has', 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, '}}{7B', 'XMLNode'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'get', 'XML'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'set', 'XML'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push 1, 'has', 'XML'
        getVariable
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
        push r:this, 'Std', 0, 'Object'
        new
        setMember
        push r:this, 'Std'
        getMember
        push 'mc_id', 0
        setMember
        push r:this, 'Std'
        getMember
        push 'attachMovie'
        function2 (r:3='}J', r:4='5P55', r:5='}p j)') (r:1='this')
          push r:'5P55', '@'
          add
          push r:this, 'mc_id'
          getMember
          push r:this, 'mc_id', r:this, 'mc_id'
          getMember
          increment
          setMember
          add
          setRegister r:2
          pop
          push r:'}p j)', r:2, r:'5P55', 3, r:'}J', 'attachMovie'
          callMethod
          pop
          push r:'}J', r:2
          getMember
          return
        end // of function 

        setMember
        push r:this, 'Std'
        getMember
        push 'createEmptyMovieClip'
        function2 (r:3='}J', r:4='}p j)') (r:1='this')
          push 'empty@', r:this, 'mc_id'
          getMember
          push r:this, 'mc_id', r:this, 'mc_id'
          getMember
          increment
          setMember
          add
          setRegister r:2
          pop
          push r:'}p j)', r:2, 2, r:'}J', 'createEmptyMovieClip'
          callMethod
          pop
          push r:'}J', r:2
          getMember
          return
        end // of function 

        setMember
        push r:this, 'Std'
        getMember
        push 'duplicateMovieClip'
        function2 (r:3='}J', r:4='}p j)') (r:1='this')
          push 'dup@', r:this, 'mc_id'
          getMember
          push r:this, 'mc_id', r:this, 'mc_id'
          getMember
          increment
          setMember
          add
          setRegister r:2
          pop
          push r:'}p j)', r:2, 2, r:'}J', 'duplicateMovieClip'
          callMethod
          pop
          push r:'}J', '_parent'
          getMember
          push r:2
          getMember
          return
        end // of function 

        setMember
        push r:this, 'Std'
        getMember
        push 'createTextField'
        function2 (r:3='}J', r:4='}p j)') (r:1='this')
          push 'text@', r:this, 'mc_id'
          getMember
          push r:this, 'mc_id', r:this, 'mc_id'
          getMember
          increment
          setMember
          add
          setRegister r:2
          pop
          push 20, 100, 0, 0, r:'}p j)', r:2, 6, r:'}J', 'createTextField'
          callMethod
          pop
          push r:'}J', r:2
          getMember
          return
        end // of function 

        setMember
        push r:this, 'Std'
        getMember
        push 'bind'
        function2 ('obj', 'method') (r:1='arguments')
          push r:arguments, 'length'
          getMember
          push 2
          equals
          not
          branchIfTrue label11
          function2 () (r:1='arguments')
            push r:arguments, 'obj'
            getVariable
            push 2, 'obj'
            getVariable
            push 'method'
            getVariable
            getMember
            push 'apply'
            callMethod
            return
          end // of function 

          return
          branch label12
         label11:
          push 'args', 2, 1, r:arguments, 'slice'
          callMethod
          varEquals
          function2 () (r:1='arguments')
            push r:arguments, 1, 'args'
            getVariable
            push 'concat'
            callMethod
            push 'obj'
            getVariable
            push 2, 'obj'
            getVariable
            push 'method'
            getVariable
            getMember
            push 'apply'
            callMethod
            return
          end // of function 

          return
         label12:
        end // of function 

        setMember
        push r:this, 'Log', 0, 'Object'
        new
        setMember
        push r:this, 'Log'
        getMember
        push 'clear'
        function2 () (r:1='this')
          push r:this, 'printed'
          getMember
          push 'text', ''
          setMember
          push r:this, 'traced'
          getMember
          push 'text', ''
          setMember
        end // of function 

        setMember
        push r:this, 'Log'
        getMember
        push 'init'
        function2 () (r:1='this', r:2='_root')
          push r:this, 'traced', 99999, '_root'
          getVariable
          push 2, 'Std'
          getVariable
          push 'createTextField'
          callMethod
          setMember
          push r:this, 'printed', 99998, '_root'
          getVariable
          push 2, 'Std'
          getVariable
          push 'createTextField'
          callMethod
          setMember
          push r:this, 'traced'
          getMember
          push '_x', 5
          setMember
          push r:this, 'traced'
          getMember
          push '_y', 20
          setMember
          push r:this, 'traced'
          getMember
          push '_width', 'Stage'
          getVariable
          push 'width'
          getMember
          push 10
          subtract
          setMember
          push r:this, 'traced'
          getMember
          push '_height', 'Stage'
          getVariable
          push 'height'
          getMember
          setMember
          push r:this, 'printed'
          getMember
          push '_x', 'Stage'
          getVariable
          push 'width'
          getMember
          push 2
          divide
          setMember
          push r:this, 'printed'
          getMember
          push '_y', 20
          setMember
          push r:this, 'printed'
          getMember
          push '_width', 'Stage'
          getVariable
          push 'width'
          getMember
          push 2
          divide
          setMember
          push r:this, 'printed'
          getMember
          push '_height', 'Stage'
          getVariable
          push 'height'
          getMember
          push 30
          subtract
          setMember
          push r:this, 'traced'
          getMember
          push 'wordWrap', TRUE
          setMember
          push r:this, 'printed'
          getMember
          push 'wordWrap', TRUE
          setMember
          push r:this, 'traced'
          getMember
          push 'selectable', FALSE
          setMember
          push r:this, 'printed'
          getMember
          push 'selectable', FALSE
          setMember
          push 10
          function ()
            push 'Log'
            getVariable
            push 'printed'
            getMember
            push 'text', ''
            setMember
          end // of function 

          push 2, 'setInterval'
          callFunction
          pop
        end // of function 

        setMember
        push 0, r:this, 'Log'
        getMember
        push 'init'
        callMethod
        pop
        push r:this, 'Log'
        getMember
        push 'setColor'
        function2 (r:2='color') (r:1='this')
          push r:this, 'traced'
          getMember
          push 'textColor', r:'color'
          setMember
          push r:this, 'printed'
          getMember
          push 'textColor', r:'color'
          setMember
        end // of function 

        setMember
        push r:this, 'Log'
        getMember
        push 'print'
        function2 (r:2='msg') (r:1='this')
          push r:this, 'printed'
          getMember
          push 'text', r:this, 'printed'
          getMember
          push 'text'
          getMember
          push '~ ', r:'msg', 1, r:this, 'toString'
          callMethod
          add
          push '\n'
          add
          add
          setMember
        end // of function 

        setMember
        push r:this, 'Log'
        getMember
        push 'toString'
        function2 (r:4='o', r:3='indent') (r:1='this')
          push r:'indent', NULL
          equals
          not
          branchIfTrue label24
          push ''
          setRegister r:'indent'
          pop
          branch label25
         label24:
          push r:'indent', 'length'
          getMember
          push 20
          lessThan
          branchIfTrue label25
          push '<...>'
          return
         label25:
          push r:'o'
          typeof
          setRegister r:0
          push 'object'
          strictEquals
          branchIfTrue label26
          push r:0, 'movieclip'
          strictEquals
          branchIfTrue label26
          push r:0, 'function'
          strictEquals
          branchIfTrue label37
          push r:0, 'string'
          strictEquals
          branchIfTrue label38
          branch label39
         label26:
          push r:'o', 'Array'
          getVariable
          instanceOf
          not
          branchIfTrue label31
          push r:'o', 'length'
          getMember
          setRegister r:7
          pop
          push '['
          setRegister r:6
          pop
          push r:'indent', '    '
          add
          setRegister r:'indent'
          pop
          push -1
          setRegister r:2
          pop
         label27:
          push r:2
          increment
          setRegister r:2
          push r:7
          lessThan
          not
          branchIfTrue label30
          push r:6, r:2, 0
          greaterThan
          branchIfTrue label28
          push ''
          branch label29
         label28:
          push ','
         label29:
          push r:'indent', r:'o', r:2
          getMember
          push 2, r:this, 'toString'
          callMethod
          add
          add
          setRegister r:6
          pop
          branch label27
         label30:
          push 4, 1, r:'indent', 'substring'
          callMethod
          setRegister r:'indent'
          pop
          push r:6, ']'
          add
          setRegister r:6
          pop
          push r:6
          return
         label31:
          push 0, r:'o', 'toString'
          callMethod
          setRegister r:8
          pop
          push r:8
          typeof
          push 'string'
          equals
          dup
          not
          branchIfTrue label32
          pop
          push r:8, '[object Object]'
          equals
          not
         label32:
          not
          branchIfTrue label33
          push r:8
          return
         label33:
          push '{\n'
          setRegister r:6
          pop
          push r:'o'
          typeof
          push 'movieclip'
          equals
          not
          branchIfTrue label34
          push 'MC(', r:'o', '_name'
          getMember
          add
          push ') '
          add
          push r:6
          add
          setRegister r:6
          pop
         label34:
          push r:'indent', '    '
          add
          setRegister r:'indent'
          pop
          push r:'o'
          enumerateValue
         label35:
          setRegister r:0
          push NULL
          equals
          branchIfTrue label36
          push r:0
          setRegister r:5
          pop
          push r:6, r:'indent', r:5
          add
          push ' : '
          add
          push r:'indent', r:'o', r:5
          getMember
          push 2, r:this, 'toString'
          callMethod
          add
          push '\n'
          add
          add
          setRegister r:6
          pop
          branch label35
         label36:
          push 4, 1, r:'indent', 'substring'
          callMethod
          setRegister r:'indent'
          pop
          push r:6, r:'indent', '}'
          add
          add
          setRegister r:6
          pop
          push r:6
          return
         label37:
          push '<fun>'
          return
         label38:
          push r:'o'
          return
         label39:
          push r:'o'
          toString
          return
        end // of function 

        setMember
        push r:this, 'Log'
        getMember
        push 'append'
        function2 (r:4='[') (r:1='this')
          push r:this, 'traced'
          getMember
          push 'text' , r:this, 'traced'
          getMember
          push 'text'
          getMember
          push r:'['
          push '\n'
          add
          add
          setMember
         label20:
          push r:this, 'traced'
          getMember
          push 'textHeight'
          getMember
          push 'Stage'
          getVariable
          push 'height'
          getMember
          push 30
          subtract
          greaterThan
          not
          branchIfTrue label21
          push 0, '\r', 2, r:this, 'traced'
          getMember
          push 'text'
          getMember
          push 'indexOf'
          callMethod
          setRegister r:2
          pop
          push r:this, 'traced'
          getMember
          push 'text', r:2, 1
          add
          push 1, r:this, 'traced'
          getMember
          push 'text'
          getMember
          push 'substr'
          callMethod
          setMember
          branch label20
         label21:
        end // of function 

        setMember
        push r:this, 'Log'
        getMember
        push 'trace'
        function2 (r:5='msg') (r:1='this')
          push '\n', 1, r:'msg', 1, r:this, 'toString'
          callMethod
          push 'split'
          callMethod
          setRegister r:3
          pop
          push r:3, 'length'
          getMember
          setRegister r:4
          pop
          push -1
          setRegister r:2
          pop
         label40:
          push r:2
          increment
          setRegister r:2
          push r:4
          lessThan
          not
          branchIfTrue label41
          push r:3, r:2
          getMember
          push 1, r:this, 'append'
          callMethod
          pop
          branch label40
         label41:
        end // of function 

        setMember
        push r:this, '19{O'
        function ()
        end // of function 

        setMember
        push r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push 'get'
        function2 (r:2='k') (r:1='this')
          push r:this, r:'k'
          getMember
          return
        end // of function 

        setMember
        push r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push 'set'
        function2 (r:2='k', r:3='v') (r:1='this')
          push r:this, r:'k', r:'v'
          setMember
        end // of function 

        setMember
        push r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push '0ENPA'
        function2 (r:3='k') (r:1='this')
          push r:this, r:'k'
          getMember
          push UNDEF
          equals
          not
          setRegister r:2
          pop
          push r:this, r:'k'
          delete
          pop
          push r:2
          return
        end // of function 

        setMember
        push r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push 'has'
        function2 (r:2='k') (r:1='this')
          push r:this, r:'k'
          getMember
          push UNDEF
          equals
          not
          return
        end // of function 

        setMember
        push r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push '5uJ2'
        function2 (r:3='cb') (r:1='this')
          push r:this
          enumerateValue
         label42:
          setRegister r:0
          push NULL
          equals
          branchIfTrue label43
          push r:0
          setRegister r:2
          pop
          push r:this, r:2
          getMember
          push r:2, 2, r:'cb', UNDEF
          callMethod
          pop
          branch label42
         label43:
        end // of function 

        setMember
        push 1, NULL, r:this, '19{O'
        getMember
        push 'prototype'
        getMember
        push 3, r:_global, 'ASSetPropFlags'
        callMethod
        pop
      end // of function 

      push 'call'
      callMethod
      pop
    end // of function 

    setMember
    push 1, NULL, '_global'
    getVariable
    push ';7pIo'
    getMember
    push 'prototype'
    getMember
    push 3, 'ASSetPropFlags'
    callFunction
  end // of initMovieClip 94

  frame 1
    push 'loader', 'this'
    getVariable
    push 1, '90D*'
    getVariable
    push 'Loader'
    newMethod
    setVariable
  end // of frame 1

  frame 2
    push 0, 'loader'
    getVariable
    push '0D 6'
    callMethod
    pop
  end // of frame 2

  frame 3
    push '', 4
    getProperty
    push 1
    subtract
    gotoAndPlay
  end // of frame 3
end
