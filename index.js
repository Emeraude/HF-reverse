#!/usr/bin/env node

const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config.json');

const app = express();

app.set('view engine', 'pug');
app.set('x-powered-by', false);
app.set('trust proxy', 'loopback');

app.listen(config.port);
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static('./public'));

app.get('/', function(req, res) {
  const pugParams = {mode: req.query.mode || "solo",
		     optionsString: req.query.options ? req.query.options.join(',') : "",
                     lives: req.query.lives,
                     bombs: req.query.bombs,
                     _debug: req.query.debug ? "1" : "0",
                     startLevel: req.query.startLevel,
                     baseUrl: config.baseUrl || req.protocol + "://" + req.get('host') + '/'} 
  if (req.query.adventure) {
    try {
      let advPath = require.resolve('./public/adventures/' + req.query.adventure + '.json')
      let adventure = require(advPath);
      pugParams.levels = adventure.levels;
      if (adventure.links)
        pugParams.links = adventure.links;
      if (adventure.levels_dim1)
        pugParams.levels_dim1 = adventure.levels_dim1;
      if (adventure.levels_dim2)
        pugParams.levels_dim2 = adventure.levels_dim2;
      if (adventure.levels_dim3)
        pugParams.levels_dim3 = adventure.levels_dim3;
      if (adventure.levels_dim4)
        pugParams.levels_dim4 = adventure.levels_dim4;
      if (adventure.skins)
        pugParams.skins = adventure.skins.join(', ');
      delete require.cache[advPath];
    } catch (e) {
      console.error(e);
      res.status(404)
        .send('Adventure ' + req.query.adventure + ' not found.');
      return;
    }
  }
  res.render('home', pugParams);
});

app.get('/play.html/manage', function(req, res) {
  res.send('gid=20653097&key=8571340b8d5711249d84dc1e1e8bce2c&families=0,1,2,3,4,10,11,12,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014');
});
